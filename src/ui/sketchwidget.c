/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <ui/interface.h>
#include <config.h>
#include <hildon/hildon-helper.h>

#include <stdlib.h>

#include "sketchwidget.h"

/*
 * SketchWidget's "Privates"
 */
void calc_rect(GdkRectangle *res, gint x1, gint y1, gint x2, gint y2, gint bs);
void sketchwidget_init_backpixmaps(SketchWidget * sk);
void sketch_draw_brush(GtkWidget * widget, gdouble x, gdouble y, gdouble pressure, SketchWidget * sk);
guint sketch_calc_brush_size_from_pressure(gdouble pressure, SketchWidget * sk);
gboolean sketch_teststupid(GtkWidget * widget, GdkEventExpose * event, SketchWidget * sk);
gboolean sketch_button_release(GtkWidget * widget, GdkEventButton * event, SketchWidget * sk);
gboolean sketch_button_press(GtkWidget * widget, GdkEventButton * event, SketchWidget * sk);
gboolean sketch_motion_notify(GtkWidget * widget, GdkEventMotion * event, SketchWidget * sk);
gboolean sketch_configure(GtkWidget * widget, GdkEventConfigure * event, SketchWidget * sk);
gboolean sketch_expose(GtkWidget * widget, GdkEventExpose * event, SketchWidget * sk);
void sketchwidget_invoke_undocallback(SketchWidget * sk, gboolean st);
void sketchwidget_invoke_redocallback(SketchWidget * sk, gboolean st);
void sketch_calc_pressurecolors(SketchWidget *sk);

void sketch_calc_pressurecolors(SketchWidget *sk)
{
guint pc = PRESSURE_COLORS + 1;
gdouble step = (PRESSURE_COLOR_MAX - PRESSURE_MIN) / PRESSURE_COLORS;
guint maxr = 32767;
guint maxg = 32767;
guint maxb = 32767;
/*
if (maxr<0) maxr=0;
if (maxg<0) maxg=0;
if (maxb<0) maxb=0;
*/
/*if (maxr==0 && maxg==0 && maxb==0) maxr=maxg=maxb=32768;*/
if (maxr<sk->brush_color.red || 
maxg<sk->brush_color.green || 
maxb<sk->brush_color.blue)
	{
	maxr=65535;
	maxg=65535;
	maxb=65535;
	}
	
guint stepr = (maxr - sk->brush_color.red) / pc;
guint stepg = (maxg - sk->brush_color.green) / pc;
guint stepb = (maxb - sk->brush_color.blue) / pc;

#ifdef PRESSURECOLORS_DEBUG
	sketchwidget_debug("step=%f stepr=%d stepg=%d stepb=%d", step, stepr, stepg, stepb);
#endif

GdkColormap *colormap = gdk_window_get_colormap(sk->drawingarea->window);

int i;
for(i=0;i<PRESSURE_COLORS;i++)
	{
	sk->brush_colors[i].red=sk->brush_color.red + ((i+1)*stepr);
	sk->brush_colors[i].green=sk->brush_color.green + ((i+1)*stepg);
	sk->brush_colors[i].blue=sk->brush_color.blue + ((i+1)*stepb);
	sk->brush_colors_pressure_index[i]=PRESSURE_MIN + ((PRESSURE_COLORS-i-1)*step);
	if (sk->brush_colors_pressure_index[i]>PRESSURE_MAX) sk->brush_colors_pressure_index[i]=PRESSURE_MAX;
	gdk_color_alloc(colormap, &sk->brush_colors[i]);

#ifdef PRESSURECOLORS_DEBUG
	sketchwidget_debug("i=%d - p=%f r=%d g=%d b=%d", i, sk->brush_colors_pressure_index[i], sk->brush_colors[i].red, sk->brush_colors[i].green, sk->brush_colors[i].blue);
#endif
	}
}

GdkColor sketch_calc_brush_color_from_pressure(gdouble pressure, SketchWidget * sk)
{
GdkColor ret=sk->brush_color;
int i;			

for(i=PRESSURE_COLORS-1;i>=0;i--)
	{
	if (pressure<=sk->brush_colors_pressure_index[i])
		{
		gdk_gc_set_foreground(sk->gc, &sk->brush_colors[i]);
		ret=sk->brush_colors[i];
#ifdef PRESSURECOLORS_DEBUG
		sketchwidget_debug("pressureindex is %d (pressure=%f)", i, pressure);
#endif
		break;
		}
	}

return(ret);
}

void sketchwidget_invoke_undocallback(SketchWidget * sk, gboolean st)
{
	if (sk->callback_undotoggle != NULL)
		(sk->callback_undotoggle) (sk, st, sk->callback_undotoggle_data);
}

void sketchwidget_invoke_redocallback(SketchWidget * sk, gboolean st)
{
	if (sk->callback_redotoggle != NULL)
		(sk->callback_redotoggle) (sk, st, sk->callback_redotoggle_data);
}

/*
 * these two functions are tested and they work
 */
void tile_getcoords(guint tileid, guint * x, guint * y)
{
	guint t = tileid % TILES_ROWSTRIDE;

	*y = (tileid - t) / TILES_ROWSTRIDE * TILE_SIZE;
	*x = t * TILE_SIZE;
}

guint tile_getid(guint x, guint y)
{
	guint tx = x % TILE_SIZE;
	guint xbase = (x - tx) / TILE_SIZE;

	guint ty = y % TILE_SIZE;
	guint ybase = (y - ty) / TILE_SIZE;

	return (ybase * TILES_ROWSTRIDE + xbase);
}

/*
 * internal
 */
void sketchundo_free_list(GList * ulist)
{
	GList *li = ulist;

	while(li != NULL)
		{
			SketchWidgetUndo *un = li->data;

			if (un != NULL)
				{
					GSList *tl = un->tileList;

					while(tl != NULL)
						{
							if (tl->data != NULL)
								{
								SketchWidgetTile *ti = (SketchWidgetTile *) tl->data;
	
								if (ti)
									{
									if (ti->tiledata) { gdk_pixmap_unref(ti->tiledata); ti->tiledata=NULL; }
									if (ti->tiledata_new) { gdk_pixmap_unref(ti->tiledata_new); ti->tiledata_new=NULL; }
									}
								}

							tl = g_slist_next(tl);
						}
					g_slist_free(un->tileList);
				}
			li = g_list_next(li);
		}
/*fixme: this one eventually causes a crash (in g_list_length) instead we keep the list for now*/
#if 0
	g_list_free(ulist);
#endif
}

/*
 * call everytime mouse is pressed
 */
void sketchundo_new(SketchWidget * sk)
{
	if (sk->undocurbuffer != NULL) return; /*assert?*/

	SketchWidgetUndo *su = g_new0(SketchWidgetUndo, 1);

	su->tileList = NULL;					/*unnecessary */
	sk->undocurbuffer = su;
}

/*
 * call everytime mouse is released
 */
void sketchundo_commit(SketchWidget * sk)
{
	sketchwidget_set_edited(sk, TRUE);

        /**
         * sometimes happens (esp on gregale) with scrolledwindow
         * expose stupid hack enabled, should be fixed though
         **/
	if (sk->undocurbuffer==NULL) {
            sketchwidget_warning("undocurbuffer is NULL in sketchundo_commit");
            return;
        }

/*	g_assert(sk->undocurbuffer != NULL);*/

	/*
	 * free structs ahead of undocurrent here
	 */
	if (sk->undocurrent > 0)
		{
#if 1
			GList *startsnip = sk->undolist;
			GList *endsnip = g_list_nth(sk->undolist, sk->undocurrent);

			if (endsnip != NULL)
				{
					sk->undolist=endsnip;
					if (endsnip->prev!=NULL) (endsnip->prev)->next=NULL;
				}
			else
				{
					sk->undolist = NULL;
				}
/*
fixme:crash--see comment in func. probably due to bad "snipping" somewhere above
*/
			sketchundo_free_list(startsnip);
#endif
			sk->undocurrent = 0;
		}

	/*
	 * check for UNDO_LEVELS and free structs here
	 */
	guint llen = g_list_length(sk->undolist);

	if (llen >= (UNDO_LEVELS - 1))
		{														/*we will add (erm, prepend is the right word) one more so we check for one less */
			GList *startsnip = g_list_nth(sk->undolist, UNDO_LEVELS - 1);

			if (startsnip != NULL && startsnip->prev != NULL)
				{
					/* cut the portion off and free it */
					(startsnip->prev)->next = NULL;
					sketchundo_free_list(startsnip);
				}
		}
		
	
	GSList *tl=sk->undocurbuffer->tileList;
	while(tl!=NULL)
		{
		SketchWidgetTile *ti = (SketchWidgetTile *) tl->data;
		if (ti!=NULL)
			{
				guint sx, sy;
				tile_getcoords(ti->tileid, &sx, &sy);
				ti->tiledata_new = gdk_pixmap_new(sk->drawingarea->window, TILE_SIZE, TILE_SIZE, -1);
				gdk_draw_drawable(ti->tiledata_new, sk->drawingarea->style->white_gc, sk->pixmap, sx, sy, 0, 0, TILE_SIZE, TILE_SIZE);
			}
		tl=g_slist_next(tl);
		}

	sk->undolist = g_list_prepend(sk->undolist, sk->undocurbuffer);
	sk->undocurbuffer = NULL;
	sk->undocurrent = 0;					/*reset our position */

	sketchwidget_invoke_undocallback(sk, TRUE);
	sketchwidget_invoke_redocallback(sk, FALSE);
}

/*
 * internal
 */
void sketchundo_savetile(SketchWidget * sk, guint tileid)
{
	if (tileid > TILES_X * TILES_Y)
		return;											/*FIXME:ugly, to keep drawing off the canvas */

	if (sk->undocurbuffer->saved_tiles[tileid] == 1) /*dupe-check*/
		return;

	guint sx, sy;

	tile_getcoords(tileid, &sx, &sy);

	SketchWidgetTile *t = g_new0(SketchWidgetTile, 1);

	t->tileid = tileid;
	t->tiledata = gdk_pixmap_new(sk->drawingarea->window, TILE_SIZE, TILE_SIZE, -1);
	t->tiledata_new = NULL;

	gdk_draw_drawable(t->tiledata, sk->drawingarea->style->white_gc, sk->pixmap, sx, sy, 0, 0, TILE_SIZE, TILE_SIZE);

	sk->undocurbuffer->tileList = g_slist_prepend(sk->undocurbuffer->tileList, t);

	sk->undocurbuffer->saved_tiles[tileid] = 1;
}

/*
 * call for each pixel before it's drawn
 */
void sketchundo_drawpoint(SketchWidget * sk, guint px, guint py)
{
	g_assert(sk->undocurbuffer != NULL);

	guint tileid = tile_getid(px, py);

	sketchundo_savetile(sk, tileid);
}

/*
 * call for each rect before it's drawn
 */
void sketchundo_drawrect(SketchWidget * sk, guint rx, guint ry, guint w, guint h)
{
	g_assert(sk->undocurbuffer != NULL);

	int i, j;

#if 1
	for(j = ry; j <= ry + h + TILE_SIZE; j += TILE_SIZE)
		{
			guint x_tile_start = tile_getid(rx, j);
			guint x_tile_end = tile_getid(rx + w /*+TILE_SIZE */ , j);

			for(i = x_tile_start; i <= x_tile_end; i++)
				{
					sketchundo_savetile(sk, i);
				}
		}
#else
		for(i=0;i<h;i++)
			for(j=0;j<w;j++)
			{
			guint tileid=tile_getid(rx+j, ry+i);
			sketchundo_savetile(sk, tileid);
			}
#endif
}

void sketchundo_drawtiles(SketchWidget * sk, SketchWidgetUndo * un, gboolean isredo)
{
/*
	GdkGC *tmpgc = gdk_gc_new(sk->drawingarea->window);
	gdk_gc_copy(tmpgc, sk->drawingarea->style->white_gc);
	gdk_gc_set_function(tmpgc, GDK_AND_REVERSE);
*/
	GSList *tl = un->tileList;

	while(tl != NULL)
		{
			SketchWidgetTile *t = (SketchWidgetTile *) (tl->data);
			GdkPixmap *ourpix;
			if (isredo==TRUE) ourpix=t->tiledata_new;
									else ourpix=t->tiledata;

			if (ourpix)
				{
					guint x, y;

					tile_getcoords(t->tileid, &x, &y);

					gdk_draw_drawable(sk->pixmap, /*tmpgc*/sk->drawingarea->style->white_gc,
														ourpix, 0, 0, x, y, TILE_SIZE, TILE_SIZE);
				}
			else
				{
					sketchwidget_warning("No tile data for tile ID: %d", t->tileid);
				}
			tl = g_slist_next(tl);
		}
/*
	g_object_unref(tmpgc);
*/
}

gboolean sketchwidget_undo(SketchWidget * sk)
{
	g_assert(sk != NULL);

	guint llen = g_list_length(sk->undolist);
	if (llen <= (sk->undocurrent))
		{
			sketchwidget_invoke_undocallback(sk, FALSE);
			return (FALSE);
		}

	SketchWidgetUndo *un = g_list_nth_data(sk->undolist, sk->undocurrent);

	if (un == NULL)
		return (FALSE);

	sk->undocurrent++;

	sketchundo_drawtiles(sk, un, FALSE);
	gtk_widget_queue_draw(sk->drawingarea);
	if (llen == sk->undocurrent)
		sketchwidget_invoke_undocallback(sk, FALSE);
	sketchwidget_invoke_redocallback(sk, TRUE);

	return (TRUE);
}

gboolean sketchwidget_redo(SketchWidget * sk)
{
	g_assert(sk != NULL);

	if (sk->undocurrent < 1)
		{
			sketchwidget_invoke_redocallback(sk, FALSE);
			return (FALSE);
		}

	SketchWidgetUndo *un = g_list_nth_data(sk->undolist, sk->undocurrent - 1);

	if (un == NULL) {
            sketchwidget_warning("Cannot redo: un == NULL");
            return FALSE;
        }

	sk->undocurrent--;

	sketchundo_drawtiles(sk, un, TRUE);
	gtk_widget_queue_draw(sk->drawingarea);
	if (sk->undocurrent < 1)
		sketchwidget_invoke_redocallback(sk, FALSE);
	sketchwidget_invoke_undocallback(sk, TRUE);

	return (TRUE);
}

void sketchwidget_wipe_undo(SketchWidget * sk)
{
	if (sk->undocurbuffer)
		sketchundo_commit(sk);			/*get rid of undocurbuffer */
	sk->undocurrent = 0;

	sketchundo_free_list(sk->undolist);
	sk->undolist = NULL;

	sketchwidget_invoke_undocallback(sk, FALSE);
	sketchwidget_invoke_redocallback(sk, FALSE);
}

void sketchwidget_set_fingercallback(SketchWidget * sk, void *func, gpointer data)
{
	g_assert(sk != NULL);
	sk->callback_finger = func;
	sk->callback_finger_data = data;
}

void sketchwidget_set_undocallback(SketchWidget * sk, void *func, gpointer data)
{
	g_assert(sk != NULL);
	sk->callback_undotoggle = func;
	sk->callback_undotoggle_data = data;
}

void sketchwidget_set_redocallback(SketchWidget * sk, void *func, gpointer data)
{
	g_assert(sk != NULL);
	sk->callback_redotoggle = func;
	sk->callback_redotoggle_data = data;
}

void sketchwidget_clear_real(SketchWidget * sk)
{
	gdk_draw_rectangle(sk->pixmap, sk->drawingarea->style->white_gc, TRUE, 0, 0, sk->drawingarea->allocation.width, sk->drawingarea->allocation.height);
}

GtkWidget *sketchwidget_get_drawingarea(SketchWidget * sk)
{
	g_assert(sk != NULL);
	return (sk->drawingarea);
}

GtkWidget *sketchwidget_get_mainwidget(SketchWidget * sk)
{
	g_assert(sk != NULL);
	return (sk->scrolledwindow);
}

GdkPixbuf *sketchwidget_trim_image(GdkPixbuf * pixbuf, guint totalw, guint totalh, double *w, double *h, gboolean doTopLeft)
{
	guint i, j, startx = 0, starty = 0, endx, endy;

	endx = totalw;
	endy = totalh;

	guint newendy = endy, newendx = endx;
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	int nchan = gdk_pixbuf_get_n_channels(pixbuf);
	guchar *pixels, *p;

	pixels = gdk_pixbuf_get_pixels(pixbuf);

	gboolean dobr = FALSE;
	if (doTopLeft==TRUE)
		{
		for(i = 0; i < endy; i++)
			{
				int yoff = i * rowstride;
	
				for(j = 0; j < endx; j++)
					{
						p = pixels + yoff + (j * nchan);
						if (p[0] != 255 || p[1] != 255 || p[2] != 255)
							{
								starty = i;
								dobr = TRUE;
								break;
							}
					}
				if (dobr == TRUE)
					break;
			}
		dobr = FALSE;
		}
	for(i = endy - 1; i >= starty; i--)
		{
			int yoff = i * rowstride;

			for(j = 0; j < endx; j++)
				{
					p = pixels + yoff + (j * nchan);
					if (p[0] != 255 || p[1] != 255 || p[2] != 255)
						{
							newendy = i + 1;
							dobr = TRUE;
							break;
						}
				}
			if (dobr == TRUE)
				break;
		}
	dobr = FALSE;
	if (doTopLeft==TRUE)
		{
		for(i = 0; i < endx; i++)
			{
				int ni = i * nchan;
	
				for(j = starty; j < endy; j++)
					{
						p = pixels + (j * rowstride) + ni;
						if (p[0] != 255 || p[1] != 255 || p[2] != 255)
							{
	
								startx = i;
								dobr = TRUE;
								break;
							}
					}
				if (dobr == TRUE)
					break;
			}
		dobr = FALSE;
		}
	for(i = endx - 1; i >= startx; i--)
		{
			int ni = i * nchan;

			for(j = starty; j < endy; j++)
				{
					p = pixels + (j * rowstride) + ni;
					if (p[0] != 255 || p[1] != 255 || p[2] != 255)
						{

							newendx = i + 1;
							dobr = TRUE;
							break;
						}
				}
			if (dobr == TRUE)
				break;
		}

	*w = newendx - startx;
	*h = newendy - starty;
	if (w==0 && h==0)
		{
		gdk_pixbuf_unref(pixbuf);
		return(NULL);
		}
	
	GdkPixbuf *pixbuf2 = gdk_pixbuf_new_subpixbuf(pixbuf, startx, starty, *w, *h);
	return (pixbuf2);
}

void sketchwidget_clear(SketchWidget * sk)
{
	sketchundo_new(sk);
	sketchundo_drawrect(sk, 0, 0, sk->drawingarea->allocation.width, sk->drawingarea->allocation.height);
	sketchwidget_clear_real(sk);
	sketchundo_commit(sk);
	gtk_widget_queue_draw(sk->drawingarea);
}

gboolean sketchwidget_cut(SketchWidget * sk, GtkClipboard * clippy)
{
	gboolean ret = sketchwidget_copy(sk, clippy);

	if (ret == FALSE)
		return (ret);
	sketchwidget_clear(sk);
	return (TRUE);
}

gboolean sketchwidget_copy(SketchWidget * sk, GtkClipboard * clippy)
{
	g_assert(sk != NULL);

	gboolean ret = FALSE;

	GdkPixbuf *pixbuf = gdk_pixbuf_get_from_drawable(NULL, GDK_DRAWABLE(sk->pixmap), NULL,
																									 0, 0,
																									 0, 0,
																									 sk->drawingarea->allocation.width,
																									 sk->drawingarea->allocation.height);

	if (pixbuf != NULL && GDK_IS_PIXBUF(pixbuf))
		{
			double w, h;
			GdkPixbuf *pixbuf2 = sketchwidget_trim_image(pixbuf,
																									 sk->drawingarea->allocation.width,
																									 sk->drawingarea->allocation.height, &w,
																									 &h, TRUE);

			if (pixbuf2 && GDK_IS_PIXBUF(pixbuf2))
				{
					if (w > 0 && h > 0)
						{
							GdkPixbuf *pixbuf3 = gdk_pixbuf_copy(pixbuf2);
							gtk_clipboard_set_image(clippy, pixbuf3);
							g_object_unref(pixbuf3);
							ret = TRUE;
						}
					g_object_unref(pixbuf2);
				}
		}
	return (ret);
}

gboolean sketchwidget_paste(SketchWidget * sk, GtkClipboard * clippy)
{
	g_assert(sk != NULL);

	gboolean ret = FALSE;

	GdkPixbuf *pixbuf = gtk_clipboard_wait_for_image(clippy);

	if (pixbuf != NULL && GDK_IS_PIXBUF(pixbuf))
		{
			int w = gdk_pixbuf_get_width(pixbuf);
			int h = gdk_pixbuf_get_height(pixbuf);

			if (w > sk->drawingarea->allocation.width)
				w = sk->drawingarea->allocation.width;
			if (h > sk->drawingarea->allocation.height)
				h = sk->drawingarea->allocation.height;
			if (w > 0 && h > 0)
				{
					sketchundo_new(sk);
					sketchundo_drawrect(sk, 0, 0, w, h);
					gdk_draw_pixbuf(GDK_DRAWABLE(sk->pixmap), NULL, pixbuf, 0, 0, 0, 0, w, h, GDK_RGB_DITHER_NONE, 0, 0);
					sketchundo_commit(sk);
					gtk_widget_queue_draw(sk->drawingarea);
					ret = TRUE;
				}
			g_object_unref(pixbuf);
		}
	return (ret);
}

GdkPixmap *sketchwidget_get_Pixmap(SketchWidget * sk)
{
	g_assert(sk != NULL);
	g_object_ref(sk->pixmap);
	return (sk->pixmap);
}

gboolean sketchwidget_get_edited(SketchWidget * sk)
{
	g_assert(sk != NULL);
	return (sk->is_edited);
}

void sketchwidget_set_edited(SketchWidget * sk, gboolean status)
{
	g_assert(sk != NULL);
	sk->is_edited = status;
}

/*
 * Creates and initialises a main_view 
 */
SketchWidget *sketchwidget_new(gint sizex, gint sizey, gboolean border)
{

	/*
	 * Zero memory with g_new0 
	 */
	SketchWidget *result = g_new0(SketchWidget, 1);

	sketchwidget_set_edited(result, FALSE);
	sketchwidget_set_brushsize(result, 0);

	result->backstyle_currentimage = SKETCHBACK_UNSET;
	result->backstyle = SKETCHBACK_NONE;
	
	result->shape = SKETCHSHAPE_FREEHAND;
	result->pressed = FALSE;
	result->fillmode = FALSE;
	
	result->border = border;
	result->scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
#ifdef MAEMOPADPLUS_FINGER_FRIENDLY
        hildon_helper_set_thumb_scrollbar(result->scrolledwindow, TRUE);
#endif
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(result->scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	result->drawingarea = gtk_drawing_area_new();
	gtk_widget_set_size_request(result->drawingarea, sizex, sizey);

	result->sizex=sizex;
	result->sizey=sizey;

	result->brush_color.red = 0;
	result->brush_color.green = 0;
	result->brush_color.blue = 0;

	result->pressuresensitivity = FALSE;
	
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(result->scrolledwindow), result->drawingarea);

	g_signal_connect(G_OBJECT(result->drawingarea), "expose_event", G_CALLBACK(sketch_expose), result);
	g_signal_connect(G_OBJECT(result->drawingarea), "configure_event", G_CALLBACK(sketch_configure), result);
	g_signal_connect(G_OBJECT(result->drawingarea), "motion_notify_event", G_CALLBACK(sketch_motion_notify), result);
	g_signal_connect(G_OBJECT(result->drawingarea), "button_press_event", G_CALLBACK(sketch_button_press), result);
	g_signal_connect(G_OBJECT(result->drawingarea), "button_release_event", G_CALLBACK(sketch_button_release), result);

	gtk_widget_set_events(result->drawingarea, GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK/* | GDK_ALL_EVENTS_MASK*/);

	g_signal_connect(G_OBJECT(result->scrolledwindow), "expose_event", G_CALLBACK(sketch_teststupid), result);
  gtk_widget_set_extension_events(result->drawingarea, GDK_EXTENSION_EVENTS_CURSOR); /* root of all evil */

	gtk_widget_show(result->drawingarea);
	gtk_widget_hide(result->scrolledwindow);

	result->undocurrent = 0;
	result->undolist = NULL;

	return result;
}

/*
 * clean up the allocated memory 
 */
void sketchwidget_destroy(SketchWidget * sk)
{
	g_assert(sk != NULL);

	int i;

	if (sk->undolist)
		g_list_free(sk->undolist);
	if (sk->pixmap)
		g_object_unref(sk->pixmap);
	if (sk->backpixmap)
		g_object_unref(sk->backpixmap);
	for(i = 0; i < SKETCHBACK_COUNT; i++)
		if (sk->backpixmaps[i])
			g_object_unref(sk->backpixmaps[i]);

	if (sk->gc)
		gdk_gc_unref(sk->gc);

	/*
	 * FIXME, segfault at GTK_IS_WIDGET
	 * if (sk->scrolledwindow && GTK_IS_WIDGET(sk->scrolledwindow)) gtk_widget_destroy(sk->scrolledwindow);
	 */
	g_free(sk);
}

void sketchwidget_set_shape(SketchWidget * sk, sketchShape sp)
{
	g_assert(sk != NULL);

	sk->shape = sp;
}

void sketchwidget_set_fillmode(SketchWidget * sk, gboolean filled)
{
	g_assert(sk != NULL);

	sk->fillmode = filled;
}

void sketchwidget_set_shift(SketchWidget * sk, gboolean shift)
{
	g_assert(sk != NULL);

	sk->shiftmode = shift;
}

gboolean sketchwidget_get_shift(SketchWidget * sk)
{
	g_assert(sk != NULL);

	return(sk->shiftmode);
}


void sketchwidget_set_brushcolor(SketchWidget * sk, GdkColor col)
{
	g_assert(sk != NULL);

	if (sk->drawingarea != NULL)
		{
			GdkColormap *colormap = gdk_window_get_colormap(sk->drawingarea->window);

			gdk_color_alloc(colormap, &col);
		}
	sk->brush_color = col;
	sketch_calc_pressurecolors(sk);
	
	if (sk->gc != NULL)
		gdk_gc_set_foreground(sk->gc, &col);
}

GdkColor sketchwidget_get_brushcolor(SketchWidget * sk)
{
	g_assert(sk != NULL);

	return (sk->brush_color);
}

void sketchwidget_set_backstyle(SketchWidget * sk, sketchBack style)
{
	g_assert(sk != NULL);

	sk->backstyle = style;
	gtk_widget_queue_draw(sk->drawingarea);

#if 0
	if (sk->backpixmap == NULL)
		return;											/*our widget not configured yet, configure will call this again */

	if (sk->backstyle == sk->backstyle_currentimage)
		return;

	sk->backstyle_currentimage = sk->backstyle;

	gdk_draw_rectangle(sk->backpixmap, sk->drawingarea->style->white_gc, TRUE, 0, 0, sk->drawingarea->allocation.width, sk->drawingarea->allocation.height);
	if (sk->backgc)
		gdk_gc_unref(sk->backgc);
	sk->backgc = gdk_gc_new(GDK_DRAWABLE(sk->backpixmap));

	GdkColormap *colormap = gdk_window_get_colormap(sk->drawingarea->window);

	if (style == SKETCHBACK_LINES)
		{
			int i;
			GdkColor tmpcol;

			tmpcol.red = 0x40 * 256;
			tmpcol.green = 0xA0 * 256;
			tmpcol.blue = 0xFF * 256;
			gdk_color_alloc(colormap, &tmpcol);
			gdk_gc_set_foreground(sk->backgc, &tmpcol);
			gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_LINE_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);

			for(i = BACKGRAPH_LINE_HEIGHT; i < sk->drawingarea->allocation.height; i += BACKGRAPH_LINE_HEIGHT)
				{
					gdk_draw_line(GDK_DRAWABLE(sk->backpixmap), sk->backgc, 0, i, sk->drawingarea->allocation.width, i);
				}

			tmpcol.red = 0xFF * 256;
			tmpcol.green = 0x00 * 256;
			tmpcol.blue = 0x80 * 256;
			gdk_color_alloc(colormap, &tmpcol);
			gdk_gc_set_foreground(sk->backgc, &tmpcol);
			gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_LINE_MARGIN_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);
			gdk_draw_line(GDK_DRAWABLE(sk->backpixmap), sk->backgc, BACKGRAPH_LINE_MARGIN, 0, BACKGRAPH_LINE_MARGIN, sk->drawingarea->allocation.height);
		}
	else if (style == SKETCHBACK_GRAPH)
		{
			int i;
			GdkColor tmpcol;

			tmpcol.red = 0x7F * 256;
			tmpcol.green = 0xB0 * 256;
			tmpcol.blue = 0xFF * 256;
			gdk_color_alloc(colormap, &tmpcol);
			gdk_gc_set_foreground(sk->backgc, &tmpcol);
			gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_GRAPH_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);

			for(i = BACKGRAPH_GRAPH_WIDTH; i < sk->drawingarea->allocation.height; i += BACKGRAPH_GRAPH_WIDTH)
				{
					gdk_draw_line(GDK_DRAWABLE(sk->backpixmap), sk->backgc, 0, i, sk->drawingarea->allocation.width, i);
				}
			for(i = BACKGRAPH_GRAPH_WIDTH; i < sk->drawingarea->allocation.width; i += BACKGRAPH_GRAPH_WIDTH)
				{
					gdk_draw_line(GDK_DRAWABLE(sk->backpixmap), sk->backgc, i, 0, i, sk->drawingarea->allocation.height);
				}
		}
	else if (style == SKETCHBACK_NONE)
		{
			int i;
			GdkColor tmpcol;

			tmpcol.red = 0x7F * 256;
			tmpcol.green = 0xB0 * 256;
			tmpcol.blue = 0xFF * 256;
			gdk_color_alloc(colormap, &tmpcol);
			gdk_gc_set_foreground(sk->backgc, &tmpcol);
			gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_GRAPH_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);

			for(i = 0; i < sk->drawingarea->allocation.height; i += TILE_SIZE)
				{
					gdk_draw_line(GDK_DRAWABLE(sk->backpixmap), sk->backgc, 0, i, sk->drawingarea->allocation.width, i);
				}
			for(i = 0; i < sk->drawingarea->allocation.width; i += TILE_SIZE)
				{
					gdk_draw_line(GDK_DRAWABLE(sk->backpixmap), sk->backgc, i, 0, i, sk->drawingarea->allocation.height);
				}
		}

	if (sk->border == TRUE)
		{
			GdkColor tmpcol;

			tmpcol.red = 192 * 256;
			tmpcol.green = 192 * 256;
			tmpcol.blue = 192 * 256;
			gdk_color_alloc(colormap, &tmpcol);
			gdk_gc_set_foreground(sk->backgc, &tmpcol);
			gdk_gc_set_line_attributes(sk->backgc, 2, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);

			gdk_draw_rectangle(sk->backpixmap, sk->backgc, FALSE, 0, 0, sk->drawingarea->allocation.width, sk->drawingarea->allocation.height);
		}

	gtk_widget_queue_draw(sk->drawingarea);
#endif
}

void sketchwidget_init_backpixmaps(SketchWidget * sk)
{
	g_assert(sk != NULL);

	int i;

	sk->backpixmapheights[0] = 0; /*or TILE_SIZE*/
	sk->backpixmapheights[1] = BACKGRAPH_LINE_HEIGHT + 1;
	sk->backpixmapheights[2] = BACKGRAPH_GRAPH_WIDTH;

	if (sk->backgc)
		gdk_gc_unref(sk->backgc);

	for(i = 0; i < SKETCHBACK_COUNT; i++)
		{
			if (sk->backpixmapheights[i] == 0)
				continue;

			if (sk->backpixmaps[i] != NULL)
				g_object_unref(sk->backpixmaps[i]);

			sk->backpixmaps[i] = gdk_pixmap_new(sk->drawingarea->window, sk->drawingarea->allocation.width, sk->backpixmapheights[i], -1);
			gdk_draw_rectangle(sk->backpixmaps[i], sk->drawingarea->style->white_gc, TRUE, 0, 0, sk->drawingarea->allocation.width, sk->backpixmapheights[i]);

			if (sk->backgc == NULL)
				sk->backgc = gdk_gc_new(GDK_DRAWABLE(sk->backpixmaps[i]));
		}

	GdkColormap *colormap = gdk_window_get_colormap(sk->drawingarea->window);

/*lines*/
	GdkColor tmpcol;

	tmpcol.red = 0x40 * 256;
	tmpcol.green = 0xA0 * 256;
	tmpcol.blue = 0xFF * 256;
	gdk_color_alloc(colormap, &tmpcol);
	gdk_gc_set_foreground(sk->backgc, &tmpcol);
	gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_LINE_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);

	for(i = BACKGRAPH_LINE_HEIGHT; i <= sk->backpixmapheights[SKETCHBACK_LINES]; i += BACKGRAPH_LINE_HEIGHT)
		{
			gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[SKETCHBACK_LINES]), sk->backgc, 0, i, sk->drawingarea->allocation.width, i);
		}

	tmpcol.red = 0xFF * 256;
	tmpcol.green = 0x00 * 256;
	tmpcol.blue = 0x80 * 256;
	gdk_color_alloc(colormap, &tmpcol);
	gdk_gc_set_foreground(sk->backgc, &tmpcol);
	gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_LINE_MARGIN_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);
	gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[SKETCHBACK_LINES]), sk->backgc, BACKGRAPH_LINE_MARGIN, 0, BACKGRAPH_LINE_MARGIN, sk->backpixmapheights[SKETCHBACK_LINES]);

/*graph*/
	tmpcol.red = 0x7F * 256;
	tmpcol.green = 0xB0 * 256;
	tmpcol.blue = 0xFF * 256;
	gdk_color_alloc(colormap, &tmpcol);
	gdk_gc_set_foreground(sk->backgc, &tmpcol);
	gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_GRAPH_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);

/*	for(i = BACKGRAPH_GRAPH_WIDTH; i <= sk->backpixmapheights[SKETCHBACK_GRAPH]; i += BACKGRAPH_GRAPH_WIDTH)*/
	for(i = 0; i < sk->backpixmapheights[SKETCHBACK_GRAPH]; i += BACKGRAPH_GRAPH_WIDTH)
		{
			gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[SKETCHBACK_GRAPH]), sk->backgc, 0, i, sk->drawingarea->allocation.width, i);
		}
/*	for(i = BACKGRAPH_GRAPH_WIDTH; i < sk->drawingarea->allocation.width; i += BACKGRAPH_GRAPH_WIDTH)*/
	for(i = 0; i < sk->drawingarea->allocation.width; i += BACKGRAPH_GRAPH_WIDTH)
		{
			gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[SKETCHBACK_GRAPH]), sk->backgc, i, 0, i, sk->backpixmapheights[SKETCHBACK_GRAPH]);
		}


/*none*/
/*
#define BACKGRAPH_DOT_WIDTH 32
	tmpcol.red = 0xC0 * 256;
	tmpcol.green = 0xC0 * 256;
	tmpcol.blue = 0xC0 * 256;
	gdk_color_alloc(colormap, &tmpcol);
	gdk_gc_set_foreground(sk->backgc, &tmpcol);

	for(i = 0; i < sk->backpixmapheights[SKETCHBACK_NONE]; i += BACKGRAPH_DOT_WIDTH)
		for(j = 0; j < sk->drawingarea->allocation.width; j += BACKGRAPH_DOT_WIDTH)
			{
				gdk_draw_point(GDK_DRAWABLE(sk->backpixmaps[SKETCHBACK_NONE]), sk->backgc, i, j);
			}
*/
/*
	tmpcol.red = 0x7F * 256;
	tmpcol.green = 0xB0 * 256;
	tmpcol.blue = 0xFF * 256;
	gdk_color_alloc(colormap, &tmpcol);
	gdk_gc_set_foreground(sk->backgc, &tmpcol);
	gdk_gc_set_line_attributes(sk->backgc, BACKGRAPH_GRAPH_SIZE, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);

	for(i = 0; i < sk->backpixmapheights[SKETCHBACK_NONE]; i += TILE_SIZE)
		{
			gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[SKETCHBACK_NONE]), sk->backgc, 0, i, sk->drawingarea->allocation.width, i);
		}
	for(i = 0; i < sk->drawingarea->allocation.width; i += TILE_SIZE)
		{
			gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[SKETCHBACK_NONE]), sk->backgc, i, 0, i, sk->backpixmapheights[SKETCHBACK_NONE]);
		}
*/

#if 0
/*important:sk->backgc should be configured for later use in expose()*/
	if (sk->border==TRUE)
		{
		GdkColor bordercol;
		bordercol.red = 192 * 256;
		bordercol.green = 192 * 256;
		bordercol.blue = 192 * 256;
		gdk_color_alloc(colormap, &bordercol);

		gdk_gc_set_foreground(sk->backgc, &bordercol);
		gdk_gc_set_line_attributes(sk->backgc, 2, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_BEVEL);
/* don't.
		for(i = 0; i < SKETCHBACK_COUNT; i++)
			{
				if (sk->backpixmapheights[i] == 0)
					continue;

				gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[i]), sk->backgc, 0, 0, 0, sk->backpixmapheights[i]);
				gdk_draw_line(GDK_DRAWABLE(sk->backpixmaps[i]), sk->backgc, sk->drawingarea->allocation.width, 0, sk->drawingarea->allocation.width, sk->backpixmapheights[i]);
			}
*/
		}
#endif
}

void sketchwidget_set_brushsize(SketchWidget * sk, guint bsize)
{
	g_assert(sk != NULL);

	if (bsize < 0)
		bsize = 0;
	if (bsize == 0)
		{
			sk->brush_radius = 0;
			sk->brush_size = 1;
		}
	else
		{
			sk->brush_radius = bsize / 2;
			if (sk->brush_radius < 1)
				sk->brush_radius = 1;

			sk->brush_size = sk->brush_radius * 2;
		}

	if (sk->gc)
		gdk_gc_set_line_attributes(sk->gc, sk->brush_size, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
}

guint sketchwidget_get_brushsize(SketchWidget * sk)
{
	g_assert(sk != NULL);

	return (sk->brush_size);
}

void calc_rect(GdkRectangle *res, gint x1, gint y1, gint x2, gint y2, gint bs)
{
gint x, y, w, h;

if (x1 < x2)
	{
		w = x2 - x1;
		x = x1;
	}
else
	{
		w = x1 - x2;
		x = x2;
	}

if (y1 < y2)
	{
		h = y2 - y1;
		y = y1;
	}
else
	{
		h = y1 - y2;
		y = y2;
	}

res->x=x;
res->y=y;
res->width=w;
res->height=h;
}

void sketch_draw_brush(GtkWidget * widget, gdouble x, gdouble y, gdouble pressure, SketchWidget * sk)
{
	GdkRectangle update_rect;

	GdkGC *gc;
	int br, bs, oldbs = 0;
	GdkColor oldcol;
	
	gc = sk->gc;
	br = sk->brush_radius;
/*	bs = sk->brush_size;*/

	if (pressure!=1)
		{
		bs=sketch_calc_brush_size_from_pressure(pressure, sk);
		oldbs=sk->brush_size;
		oldcol=sk->brush_color;
		if (sk->gc)
			{
			gdk_gc_set_line_attributes(sk->gc, bs, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
			if (sk->pressuresensitivity)
				{
				sk->brush_color=sketch_calc_brush_color_from_pressure(pressure, sk);
				gdk_gc_set_foreground(sk->gc, &sk->brush_color);
				}
			}
		}
	else
		{
		bs=sk->brush_size;
		}
		
	if (bs == 1)
		{
			update_rect.x = x;
			update_rect.y = y;
			update_rect.width = 1;
			update_rect.height = 1;

			sketchundo_drawrect(sk, update_rect.x, update_rect.y, update_rect.width, update_rect.height);

			gdk_draw_rectangle(sk->pixmap, gc, TRUE, update_rect.x, update_rect.y, update_rect.width, update_rect.height);
		}
	else
		{

			update_rect.x = x - br;
			update_rect.y = y - br;
			update_rect.width = bs;
			update_rect.height = bs;

			sketchundo_drawrect(sk, update_rect.x, update_rect.y, update_rect.width, update_rect.height);

			gdk_draw_arc(sk->pixmap, gc, TRUE, x - br, y - br, bs, bs, 0, 64 * 360);
		}

	if (pressure!=1)
		{
		gdk_gc_set_line_attributes(sk->gc, oldbs, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
		if (sk->pressuresensitivity)
			{
			sk->brush_color=oldcol;
			gdk_gc_set_foreground(sk->gc, &oldcol);
			}
		}

	gtk_widget_queue_draw_area(widget, update_rect.x, update_rect.y, update_rect.width, update_rect.height);
}

gboolean sketch_teststupid(GtkWidget * widget, GdkEventExpose * event, SketchWidget * sk)
{

if (event->area.height>=350)
	{
#ifdef NOTIFY_STUPID
	sketchwidget_warning("Stupid...");
#endif
	sk->stupidflag=1;
	}
	
return FALSE;
}

gboolean sketch_button_release(GtkWidget * widget, GdkEventButton * event, SketchWidget * sk)
{
if (sk->pressed==FALSE) return TRUE;

if (event->button == 1)
	{
	if (sk->shape != SKETCHSHAPE_FREEHAND)
		{
		sketchundo_new(sk);

		GdkRectangle update_rect;
		GdkGC *gc;
		int bs;
	
		gc = sk->gc;
		bs = sk->brush_size + 3;

		calc_rect(&update_rect, sk->start_x, sk->start_y, sk->cur_x, sk->cur_y, bs);

		if (sk->shiftmode)
			{
			if (sk->shape == SKETCHSHAPE_LINE)
				{
				int xd=abs(sk->start_x - sk->cur_x);
				int yd=abs(sk->start_y - sk->cur_y);
				if (xd>(yd*1.8)) sk->cur_y=sk->start_y;
				else if (yd>(xd*1.8)) sk->cur_x=sk->start_x;
				else
					{
						if (sk->cur_x<sk->start_x && sk->cur_y>sk->start_y)
							sk->cur_x=sk->start_x - abs(sk->cur_y - sk->start_y);
						else if (sk->cur_x>sk->start_x && sk->cur_y<sk->start_y)
							sk->cur_x=sk->start_x + abs(sk->cur_y - sk->start_y);
						else								
							sk->cur_y=sk->start_y + (sk->cur_x - sk->start_x);
					}
				calc_rect(&update_rect, sk->start_x, sk->start_y, sk->cur_x, sk->cur_y, bs);
				}
			else
				{
				update_rect.width=update_rect.height=sqrt((update_rect.height*update_rect.height)+(update_rect.width*update_rect.width));
				}
			}

		sketchundo_drawrect(sk, (update_rect.x>bs)?(update_rect.x-bs):0, (update_rect.y>bs)?(update_rect.y-bs):0, update_rect.width+(bs*2), update_rect.height+(bs*2));
		if (sk->shape == SKETCHSHAPE_RECT) gdk_draw_rectangle(sk->pixmap, gc, sk->fillmode, update_rect.x, update_rect.y, update_rect.width, update_rect.height);
		else if (sk->shape == SKETCHSHAPE_ELLIPSE) gdk_draw_arc(sk->pixmap, gc, sk->fillmode, update_rect.x, update_rect.y, update_rect.width, update_rect.height, 0, 360*64);
		else if (sk->shape == SKETCHSHAPE_LINE) gdk_draw_line(sk->pixmap, gc, sk->start_x, sk->start_y, sk->cur_x, sk->cur_y);

/*				gtk_widget_queue_draw_area(widget, update_rect.x, update_rect.y, update_rect.width+(bs*2), update_rect.height+(bs*2));
*/
			gtk_widget_queue_draw(widget);
		}

	sketchundo_commit(sk);

	sk->pressed=FALSE;

	sk->lastevent_x = 0;
	sk->lastevent_y = 0;

	sk->start_x = 0;
	sk->start_y = 0;

	sk->cur_x = 0;
	sk->cur_y = 0;

	sk->coords_valid = FALSE;

	}
return TRUE;
}

gboolean sketch_button_press(GtkWidget * widget, GdkEventButton * event, SketchWidget * sk)
{

if (sk->stupidflag==2)
	{
#ifdef NOTIFY_STUPID
	sketchwidget_warning("Stupid... (2)");
#endif
	sk->pressed=TRUE; /*first action after the stupid call wouldn't register otherwise*/
	sk->stupidflag=0;

	sk->coords_valid = FALSE;
	
	return TRUE;
	}

/*
FIXME: not compatible with maemo2
if (hildon_button_event_is_finger(event))
	{
#ifdef NOTIFY_COORDS
			fprintf(stderr, "GOT FINGERPRESS (%d,%d)\n", (int)event->x, (int)event->y);
#endif

	if (sk->callback_finger != NULL)
		{
		gdouble pressure;
    if (!gdk_event_get_axis ((GdkEvent *)event, GDK_AXIS_PRESSURE, &pressure)) pressure=1;
			
		(sk->callback_finger) (sk, (int)event->x, (int)event->y, pressure, sk->callback_finger_data);
		}
	return TRUE;
	}
*/

gdouble pressure;
if (!gdk_event_get_axis ((GdkEvent *)event, GDK_AXIS_PRESSURE, &pressure)) pressure=1;

if (hildon_helper_event_button_is_finger(event))
	{
#ifdef NOTIFY_COORDS
            sketchwidget_message("Got fingerpress at (%d,%d)", (int)event->x, (int)event->y);
#endif
	if (sk->callback_finger != NULL)
		(sk->callback_finger) (sk, (int)event->x, (int)event->y, pressure, sk->callback_finger_data);
    return TRUE;
	}


if (event->button == 1 && sk->pixmap != NULL)
	{

	if (event->x<0 || event->y<0 || event->x>=sk->sizex || event->y>=sk->sizey) return TRUE;

#ifdef NOTIFY_COORDS
            sketchwidget_message("Got button press at (%d,%d)", (int)event->x, (int)event->y);
#endif

		if (sk->shape == SKETCHSHAPE_FREEHAND)
			{
			sketchundo_new(sk);
			sketch_draw_brush(widget, event->x, event->y, pressure, sk);

			sk->lastevent_x = event->x;
			sk->lastevent_y = event->y;
			}
		else
			{
			sk->start_x = event->x;
			sk->start_y = event->y;
			sk->coords_valid = TRUE;
			}

		sk->pressed = TRUE;
	}
return TRUE;
}

guint sketch_calc_brush_size_from_pressure(gdouble pressure, SketchWidget * sk)
{
if (pressure<PRESSURE_MIN) pressure=PRESSURE_MIN;
else if (pressure==1 || pressure>PRESSURE_MAX) pressure=PRESSURE_MAX;

gdouble rp=pressure/PRESSURE_MAX;

guint bs=ceil(sk->brush_size*rp);

return(bs);
}

gboolean sketch_motion_notify(GtkWidget * widget, GdkEventMotion * event, SketchWidget * sk)
{
double x, y, pressure;
GdkModifierType state;

if (sk->stupidflag==1)
	{
#ifdef NOTIFY_STUPID
	sketchwidget_warning("Stupid...");
#endif
	sk->stupidflag++;
	return TRUE;
	}

if (sk->shape == SKETCHSHAPE_FREEHAND && sk->pressed == FALSE)
	{
#ifdef NOTIFY_COORDS
        sketchwidget_warning("Ignoring...");
#endif
	return TRUE;
	}

if (event->is_hint)
	{
/*		gdk_window_get_pointer(event->window, &x, &y, &state);*/

  gdk_device_get_state (event->device, event->window, NULL, &state);
  gdk_event_get_axis ((GdkEvent *)event, GDK_AXIS_X, &x);
  gdk_event_get_axis ((GdkEvent *)event, GDK_AXIS_Y, &y);
  if (!gdk_event_get_axis ((GdkEvent *)event, GDK_AXIS_PRESSURE, &pressure)) pressure=1;

#ifdef NOTIFY_COORDS
       sketchwidget_debug("Got hint at (%d,%d)", (int)x, (int)y);
#endif
	}
else
	{
		x = event->x;
		y = event->y;
		state = event->state;
		if (!gdk_event_get_axis ((GdkEvent *)event, GDK_AXIS_PRESSURE, &pressure)) pressure=1;

#ifdef NOTIFY_COORDS
       sketchwidget_debug("Got motion notify at (%d,%d)", (int)x, (int)y);
#endif
	}

	if (x<0 || y<0 || x>=sk->sizex || y>=sk->sizey) return TRUE;

if (sk->shape != SKETCHSHAPE_FREEHAND)
	{
	if (sk->coords_valid==FALSE)
		{
		sk->start_x = x;
		sk->start_y = y;
		sk->coords_valid=TRUE;
		return TRUE;
		}
	
	sk->cur_x = x;
	sk->cur_y = y;
/*
	GdkRectangle update_rect;
	int bs;
	bs = sk->brush_size + 3;
	calc_rect(&update_rect, sk->start_x, sk->start_y, sk->cur_x, sk->cur_y, bs);
	if (sk->shiftmode) update_rect.width=update_rect.height;

	gtk_widget_queue_draw_area(widget, update_rect.x, update_rect.y, update_rect.width+(bs*2), update_rect.height+(bs*2));
*/
	gtk_widget_queue_draw(widget);

	return TRUE;
	}

int xb = x, yb = y;

if (state & GDK_BUTTON1_MASK && sk->pixmap != NULL)
	{
		sketchundo_new(sk);

		if (sk->lastevent_x == 0 && sk->lastevent_y == 0)
			{
				sketch_draw_brush(widget, x, y, pressure, sk);
			}
		else
			{
				gint bs2, oldbs = 0;
				GdkColor oldcol;
				
				if (pressure!=1)
					{
					bs2=sketch_calc_brush_size_from_pressure(pressure, sk);
					oldbs=sk->brush_size;
					oldcol=sk->brush_color;
					if (sk->gc)
						{
						gdk_gc_set_line_attributes(sk->gc, bs2, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
						if (sk->pressuresensitivity)
							{
							sk->brush_color=sketch_calc_brush_color_from_pressure(pressure, sk);
							gdk_gc_set_foreground(sk->gc, &sk->brush_color);
							}
						}
					}
				else
					{
					bs2=sk->brush_size;
					}
				
				gint x1, y1, w, h;
				gint bs = bs2 + 3;

				if (x < sk->lastevent_x)
					{
						w = sk->lastevent_x - x;
						x1 = x;
					}
				else
					{
						w = x - sk->lastevent_x;
						x1 = sk->lastevent_x;
					}

				if (y < sk->lastevent_y)
					{
						h = sk->lastevent_y - y;
						y1 = y;
					}
				else
					{
						h = y - sk->lastevent_y;
						y1 = sk->lastevent_y;
					}

				if (x1 >= bs)
					x1 -= bs;
				else
					x1 = 0;

				if (y1 >= bs)
					y1 -= bs;
				else
					y1 = 0;

				/*
				 nice debug code
				 GdkColor col=sketchwidget_get_brushcolor(sk);
				 GdkColor col2, col3;
				 col2.red=col2.green=0;
				 col2.blue=255*255;
				 sketchwidget_set_brushcolor(sk, col2);
				 
				 col3.red=col2.green=col3.blue=255*255;
				 GdkColormap  *colormap = gdk_window_get_colormap(sk->drawingarea->window);
				 gdk_color_alloc(colormap, &col3);
				 gdk_gc_set_background(sk->gc, &col3);
				 
				 gdk_gc_set_line_attributes(sk->gc, 1, GDK_LINE_DOUBLE_DASH, GDK_CAP_BUTT, GDK_JOIN_BEVEL);
				 gdk_draw_rectangle(sk->pixmap, sk->gc, FALSE, x1, y1, w+(bs*2), h+(bs*2));
				 
				 sketchwidget_set_brushcolor(sk, col);
				 gdk_gc_set_line_attributes(sk->gc, sk->brush_size, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
				 gtk_widget_queue_draw(widget);
				 */
				sketchundo_drawrect(sk, x1, y1, w + (bs * 2), h + (bs * 2));

				gdk_draw_line(sk->pixmap, sk->gc, sk->lastevent_x, sk->lastevent_y, x, y);

				gtk_widget_queue_draw_area(widget, x1, y1, w + (bs * 2), h + (bs * 2));

				if (pressure!=1 && sk->gc)
					{
					gdk_gc_set_line_attributes(sk->gc, oldbs, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
					if (sk->pressuresensitivity)
						{
						sk->brush_color=oldcol;
						gdk_gc_set_foreground(sk->gc, &sk->brush_color);
						}
					}
			}

		sk->lastevent_x = xb;
		sk->lastevent_y = yb;
	}

return TRUE;
}

gboolean sketch_configure(GtkWidget * widget, GdkEventConfigure * event, SketchWidget * sk)
{
	if (sk->backpixmap)
		g_object_unref(sk->backpixmap);

	sketchwidget_init_backpixmaps(sk);

	sk->backpixmap = gdk_pixmap_new(widget->window, widget->allocation.width, widget->allocation.height, -1);

	if (sk->pixmap)
		g_object_unref(sk->pixmap);

	sk->pixmap = gdk_pixmap_new(widget->window, widget->allocation.width, widget->allocation.height, -1);
	sketchwidget_clear_real(sk);

	if (sk->gc)
		gdk_gc_unref(sk->gc);

	sk->gc = gdk_gc_new(GDK_DRAWABLE(sk->pixmap));

	sketchwidget_set_backstyle(sk, sk->backstyle);

	sketchwidget_set_brushcolor(sk, sk->brush_color);

	gdk_gc_set_line_attributes(sk->gc, sk->brush_size, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);

	return TRUE;
}

/*
 * Redraw the screen from the backing pixmap 
 */
gboolean sketch_expose(GtkWidget * widget, GdkEventExpose * event, SketchWidget * sk)
{
	GdkGC *tmpgc = gdk_gc_new(widget->window);

	gdk_gc_copy(tmpgc, widget->style->fg_gc[GTK_WIDGET_STATE(widget)]);

	/*
	 gdk_draw_drawable(widget->window,
	 widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
	 sk->backpixmap,
	 event->area.x, event->area.y,
	 event->area.x, event->area.y,
	 event->area.width, event->area.height);
	 */

	int bh = sk->backpixmapheights[sk->backstyle];
	GdkPixmap *bp = sk->backpixmaps[sk->backstyle];

	if (bh > 0 && bp != NULL)
		{
			int starty = event->area.y % bh;

			gdk_draw_drawable(widget->window, widget->style->fg_gc[GTK_WIDGET_STATE(widget)], bp, event->area.x, starty, event->area.x, event->area.y, event->area.width, bh - starty);

			starty = event->area.y + bh - starty;
			int endy = event->area.y + event->area.height;

			int tmp = endy % bh;
			int midy = endy - tmp;

			/*
			 * midy=endy;
			 */

			int i;

			for(i = starty; i < midy; i += bh)
				{
					gdk_draw_drawable(widget->window, widget->style->fg_gc[GTK_WIDGET_STATE(widget)], bp, event->area.x, 0, event->area.x, i, event->area.width, bh);
				}

			/*
			 * fill the rest
			 */
			if (midy < endy)
				gdk_draw_drawable(widget->window, widget->style->fg_gc[GTK_WIDGET_STATE(widget)], bp, event->area.x, 0, event->area.x, midy, event->area.width, midy - endy);

			gdk_gc_set_function(tmpgc, GDK_AND);
		}

#if 0
/*
this code kinda works but it won't do what a decent gtkrc can so why bother
also it if the widget scrolls the borders scroll with it too :p
*/
	if (sk->border==TRUE)
		{
		/*take care of left and right borders*/
		if (event->area.x<2)
			gdk_draw_line(widget->window, sk->backgc, 0, event->area.y, 2, event->area.height);
		if (event->area.x>=(widget->allocation.width-2) || (event->area.x+event->area.width)>=(widget->allocation.width-2))
			gdk_draw_line(widget->window, sk->backgc, widget->allocation.width-2, event->area.y, 2, event->area.height);

		/*take care of top and bottom borders*/
		if (event->area.y<2)
			gdk_draw_line(widget->window, sk->backgc, event->area.x, 0, event->area.width, 2);
		if (event->area.y>=(widget->allocation.height-2) || (event->area.y+event->area.height)>=(widget->allocation.height-2))
			gdk_draw_line(widget->window, sk->backgc, event->area.x, widget->allocation.height-2, event->area.width, 2);
		}
#endif

	gdk_draw_drawable(widget->window, tmpgc, sk->pixmap, event->area.x, event->area.y, event->area.x, event->area.y, event->area.width, event->area.height);

	g_object_unref(tmpgc);

	if (sk->shape != SKETCHSHAPE_FREEHAND && sk->pressed)
		{
		GdkRectangle update_rect;

		int bs;
		bs = sk->brush_size + 3;

		calc_rect(&update_rect, sk->start_x, sk->start_y, sk->cur_x, sk->cur_y, bs);
		if (sk->shiftmode)
			{
			if (sk->shape == SKETCHSHAPE_LINE)
				{
				int xd=abs(sk->start_x - sk->cur_x);
				int yd=abs(sk->start_y - sk->cur_y);
				if (xd>(yd*1.8)) sk->cur_y=sk->start_y;
				else if (yd>(xd*1.8)) sk->cur_x=sk->start_x;
				else
					{
						if (sk->cur_x<sk->start_x && sk->cur_y>sk->start_y)
							sk->cur_x=sk->start_x - abs(sk->cur_y - sk->start_y);
						else if (sk->cur_x>sk->start_x && sk->cur_y<sk->start_y)
							sk->cur_x=sk->start_x + abs(sk->cur_y - sk->start_y);
						else								
							sk->cur_y=sk->start_y + (sk->cur_x - sk->start_x);
					}
				calc_rect(&update_rect, sk->start_x, sk->start_y, sk->cur_x, sk->cur_y, bs);
				}
			else
				{
				update_rect.width=update_rect.height=sqrt((update_rect.height*update_rect.height)+(update_rect.width*update_rect.width));
				}
			}

		if (sk->shape == SKETCHSHAPE_RECT)
			gdk_draw_rectangle(widget->window, sk->gc, sk->fillmode, update_rect.x, update_rect.y, update_rect.width, update_rect.height);
		else if (sk->shape == SKETCHSHAPE_ELLIPSE)
			gdk_draw_arc(widget->window, sk->gc, sk->fillmode, update_rect.x, update_rect.y, update_rect.width, update_rect.height, 0, 360*64);
		else if (sk->shape == SKETCHSHAPE_LINE)
			gdk_draw_line(widget->window, sk->gc, sk->start_x, sk->start_y, sk->cur_x, sk->cur_y);
		}


	return FALSE;
}
