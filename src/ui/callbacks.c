/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <ui/callbacks.h>
#include <ui/interface.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <libintl.h>

#include <glib.h>

/*
 * strlen needed from string.h 
 */
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <config.h>
#include <time.h>

#include <hildon/hildon.h>
#include <hildon/hildon-banner.h>
#include <hildon/hildon-note.h>
#include <hildon/hildon-font-selection-dialog.h>
#include <tablet-browser-interface.h>

#include <libgnomevfs/gnome-vfs.h>

#include <sharingdialog/sharing-dialog.h>

#include "sketchwidget.h"

#include "../he/he-about-dialog.h"
#include "../he/he-simple-color-dialog.h"

/*
 * "Busy" status handling
 *
 * Use "busy_reset" to reset the busy status (turn off)
 * Use "busy_enter" when starting time-consuming processing
 * Use "busy_leave" when processing has been finished
 */
typedef enum {
    BUSY_RESET = 0,
    BUSY_INCREMENT,
    BUSY_DECREMENT,
} SetBusyType;

/* Don't use this directly, but make use of the macros defined below */
void set_busy(MainView* mainview, SetBusyType update);

#define busy_reset(mainview) set_busy(mainview, BUSY_RESET)
#define busy_enter(mainview) set_busy(mainview, BUSY_INCREMENT)
#define busy_leave(mainview) set_busy(mainview, BUSY_DECREMENT)


/*
 * Privates: 
 */
void checklist_abort_longpress(MainView* mainview);
gboolean read_file_to_buffer(MainView * mainview);
void write_buffer_to_file(MainView * mainview);
void new_node_dialog(nodeType typ, MainView * mainview);
gboolean foreach_func_update_ord (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter, MainView *mainview);
void move_nodes_up(GtkTreeModel * model, GtkTreeRowReference * topnode, GtkTreeRowReference * newtop);
GtkTreeRowReference *iter2ref(GtkTreeModel * model, GtkTreeIter * iter);
gboolean exec_command_on_db(MainView *mainview,char sql_string[]);
int get_node_id_on_tmp_db(GtkTreeModel *model,GtkTreeIter *iter);
gint get_branch_node_index(GtkTreePath *path);
gboolean tree_model_iter_prev(GtkTreeModel *tree_model,GtkTreeIter *iter);
gboolean show_confirmation(MainView* mainview, gchar* question);



gboolean callback_node_view_window_state(GtkWidget* window,
        GdkEventWindowState* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    GtkWidget* sketch_scroll = NULL;
    nodeData *nd = getSelectedNode(mainview);

    if (mainview->toolbar == NULL || mainview->sk == NULL) {
        return FALSE;
    }

    sketch_scroll = GTK_WIDGET(sketchwidget_get_mainwidget(mainview->sk));

    if (event->changed_mask & GDK_WINDOW_STATE_FULLSCREEN) {
        if (event->new_window_state & GDK_WINDOW_STATE_FULLSCREEN) {
            gtk_widget_hide(mainview->toolbar);
            gtk_scrolled_window_set_policy(
                    GTK_SCROLLED_WINDOW(sketch_scroll),
                    GTK_POLICY_NEVER,
                    GTK_POLICY_NEVER);
        } else {
            gtk_widget_show(mainview->toolbar);
            gtk_scrolled_window_set_policy(
                    GTK_SCROLLED_WINDOW(sketch_scroll),
                    GTK_POLICY_AUTOMATIC,
                    GTK_POLICY_AUTOMATIC);
        }
    }

    return FALSE;
}

void set_busy(MainView* mainview, SetBusyType update)
{
    switch (update) {
        case BUSY_RESET:
            mainview->busyrefcount = 0;
            break;
        case BUSY_INCREMENT:
            mainview->busyrefcount++;
            break;
        case BUSY_DECREMENT:
            if (mainview->busyrefcount > 0) {
                mainview->busyrefcount--;
            }
            break;
        default:
            g_assert_not_reached();
            return;
    }

    hildon_gtk_window_set_progress_indicator(GTK_WINDOW(mainview->data->main_view), (mainview->busyrefcount > 0));
    hildon_gtk_window_set_progress_indicator(GTK_WINDOW(mainview->data->node_view), (mainview->busyrefcount > 0));
}

void prepareUIforNodeChange(MainView * mainview, nodeType typ)
{
	gtk_widget_set_sensitive(GTK_WIDGET(mainview->undo_tb), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(mainview->redo_tb), TRUE);

	if (typ == NODE_TEXT)
		{
			gtk_widget_show(GTK_WIDGET(mainview->font_tb));
			gtk_widget_show(GTK_WIDGET(mainview->bold_tb));
			gtk_widget_show(GTK_WIDGET(mainview->italic_tb));
			gtk_widget_show(GTK_WIDGET(mainview->underline_tb));
			gtk_widget_show(GTK_WIDGET(mainview->bullet_tb));
			gtk_widget_show(mainview->tools_font);
		}
	else
		{
			gtk_widget_hide(GTK_WIDGET(mainview->font_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->bold_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->italic_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->underline_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->bullet_tb));
			gtk_widget_hide(mainview->tools_font);
		}

	if (typ == NODE_SKETCH)
		{
/*			gtk_widget_show(GTK_WIDGET(mainview->colorbutton_tb));*/
			gtk_widget_show(GTK_WIDGET(mainview->eraser_tb));
			gtk_widget_show(GTK_WIDGET(mainview->brushsize_tb));
			gtk_widget_show(GTK_WIDGET(mainview->sketchlines_tb));
			gtk_widget_show(GTK_WIDGET(mainview->shape_tb));
			gtk_widget_show(GTK_WIDGET(mainview->sharing_tb));
			gtk_widget_show(mainview->tools_color);
			gtk_widget_show(mainview->tools_brushsize);
			gtk_widget_show(mainview->tools_pagestyle);
			gtk_widget_show(mainview->tools_shape);
			gtk_widget_show(mainview->tools_pressure);
		}
	else
		{
/*			gtk_widget_hide(GTK_WIDGET(mainview->colorbutton_tb));*/
			gtk_widget_hide(GTK_WIDGET(mainview->eraser_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->brushsize_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->sketchlines_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->shape_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->sharing_tb));
			gtk_widget_hide(mainview->tools_color);
			gtk_widget_hide(mainview->tools_brushsize);
			gtk_widget_hide(mainview->tools_pagestyle);
			gtk_widget_hide(mainview->tools_shape);
			gtk_widget_hide(mainview->tools_pressure);
		}

	if (typ == NODE_CHECKLIST)
		{
			gtk_widget_show(GTK_WIDGET(mainview->bold_tb));
			gtk_widget_show(GTK_WIDGET(mainview->strikethru_tb));
			gtk_widget_show(GTK_WIDGET(mainview->check_tb));
			gtk_widget_show(GTK_WIDGET(mainview->checkadd_tb));
			gtk_widget_show(GTK_WIDGET(mainview->checkedit_tb));
			gtk_widget_show(GTK_WIDGET(mainview->checkdel_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->undo_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->redo_tb));
		}
	else
		{
			gtk_widget_hide(GTK_WIDGET(mainview->strikethru_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->check_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->checkadd_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->checkedit_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->checkdel_tb));
			gtk_widget_show(GTK_WIDGET(mainview->undo_tb));
			gtk_widget_show(GTK_WIDGET(mainview->redo_tb));
		}

	if (typ == NODE_TEXT)
		{
			gtk_widget_hide(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_hide(mainview->checklist_content);
			gtk_widget_show(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_show(mainview->tools_item);
		}
	else if (typ == NODE_SKETCH)
		{
			gtk_widget_show(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_hide(mainview->checklist_content);
			gtk_widget_show(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_show(mainview->tools_item);
		}
	else if (typ == NODE_CHECKLIST)
		{
			gtk_widget_show(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_hide(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_hide(mainview->tools_item);
			gtk_widget_show(mainview->checklist_content);
		}
	else
		{
			gtk_widget_hide(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_hide(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_hide(mainview->tools_item);
			gtk_widget_hide(mainview->checklist_content);
		}
}

nodeData *getSelectedNode(MainView * mainview)
{
	GtkTreeIter iter;
	GtkTreeModel *model;

	nodeData *nd;

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

	if (!gtk_tree_selection_get_selected(selection, &model, &iter))
		return (NULL);

	gtk_tree_model_get(model, &iter, NODE_DATA, &nd, -1);

	return (nd);
}

void saveCurrentData(MainView * mainview)
{
	nodeData *selnode = getSelectedNode(mainview);
	saveDataToNode(mainview, selnode);
}

void saveDataToNode(MainView * mainview, nodeData *selnode)
{
	if (selnode == NULL)
		return;

	if (
	(selnode->typ == NODE_SKETCH && sketchwidget_get_edited(mainview->sk) == FALSE) ||
	(selnode->typ == NODE_TEXT && wp_text_buffer_is_modified(mainview->buffer)==FALSE) ||
	(selnode->typ == NODE_CHECKLIST && mainview->checklist_edited==FALSE)
	)
		{
			maepad_message("node not edited, not saving");
			return;
		}

	mainview->file_edited = TRUE;

	busy_enter(mainview);
        maepad_debug("saveDataToNode working");

	gboolean goterr = TRUE;
	gchar *textdata = NULL;
	GdkPixbuf *pixbuf = NULL;
	gchar *sketchdata = NULL;
	gsize datalen = 0;
	char tq[512];

	if (selnode->typ == NODE_TEXT)
		{
#if 0
			GtkTextIter start, end;
			gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(mainview->buffer), &start, &end);
			textdata = gtk_text_buffer_serialize_rich_text(GTK_TEXT_BUFFER(mainview->buffer), &start, &end, &datalen);
#endif
			
			GString *gstr=g_string_sized_new(4096);
			wp_text_buffer_save_document(mainview->buffer, (WPDocumentSaveCallback)(wp_savecallback), gstr);

			datalen=gstr->len;
			textdata=g_string_free(gstr, FALSE);

/*			g_snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, bodyblob=NULL, body=?, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);*/
			g_snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, bodyblob=?, body=NULL, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);
		}
	else if (selnode->typ == NODE_SKETCH)
		{
			GError *err = NULL;
			GdkPixmap *skpix = sketchwidget_get_Pixmap(mainview->sk);
			GtkWidget *skdr = sketchwidget_get_drawingarea(mainview->sk);

			pixbuf = gdk_pixbuf_get_from_drawable(NULL, GDK_DRAWABLE(skpix), NULL, 0, 0, 0, 0, skdr->allocation.width, skdr->allocation.height);
			if (pixbuf == NULL)
				{
					maepad_warning("error saving: pixbuf is null");
				}
			else
				{
				double w, h;
				GdkPixbuf *pixbuf2 = sketchwidget_trim_image(pixbuf, skdr->allocation.width, skdr->allocation.height, &w, &h, FALSE);

				if (pixbuf2!=NULL)
					{
					if (gdk_pixbuf_save_to_buffer(pixbuf2, &sketchdata, &datalen, "png", &err, NULL) == FALSE)
						{
							sketchdata = NULL;
							datalen = 0;
                                                        maepad_warning("Error saving sketch: %s", err->message);
							g_error_free(err);
						}
					gdk_pixbuf_unref(pixbuf2);
					}
				}
			g_snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, body=NULL, bodyblob=?, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);
			maepad_debug("storing sketch in db: %d bytes", datalen);
			if (skpix && G_IS_OBJECT(skpix)) g_object_unref(skpix);
		}
	else if (selnode->typ == NODE_CHECKLIST)
		{
		g_snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, body=NULL, bodyblob=NULL, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);
		}

	do
		{
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

			if (rc)
				{
					maepad_warning("Error updating: %s", sqlite3_errmsg(mainview->db));
					break;
				}
			if (selnode->typ == NODE_TEXT)
				sqlite3_bind_text(stmt, 1, textdata, datalen, /*strlen(textdata),*/ SQLITE_TRANSIENT);
			else if (selnode->typ == NODE_SKETCH)
				sqlite3_bind_blob(stmt, 1, sketchdata, datalen, SQLITE_TRANSIENT);

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
				}
			sqlite3_finalize(stmt);

			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					maepad_warning("Error saving node: %s", sqlite3_errmsg(mainview->db));
				}
			else
				{
					if (selnode->typ == NODE_TEXT)
						gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(mainview->buffer), FALSE);
					else if (selnode->typ == NODE_SKETCH)
						sketchwidget_set_edited(mainview->sk, FALSE);
					else if (selnode->typ == NODE_CHECKLIST)
                                                mainview->checklist_edited = FALSE;
					goterr = FALSE;
				}
		}while(FALSE);

	while(goterr==FALSE && selnode->typ == NODE_CHECKLIST)
		{
		GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));

		char tq[512];
		g_snprintf(tq, sizeof(tq), "DELETE FROM %s WHERE nodeid=%d", checklisttable_tmpname, selnode->sql3id);
		sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

		GtkTreeIter iter;
		if (gtk_tree_model_get_iter_first(model, &iter)==FALSE) break;
		
		do
			{
			gint styletoset_weight;
			gboolean styletoset_strike;
			gboolean ischecked;
			gchar *text;
			gchar *color;
			unsigned long col=0;

	  	gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, CHECKNODE_BOLD, &styletoset_weight, CHECKNODE_STRIKE, &styletoset_strike, CHECKNODE_CHECKED, &ischecked, CHECKNODE_TEXT, &text, CHECKNODE_COLOR, &color, -1);

			GdkColor tmpcol;
			if (color!=NULL && strcmp(color, "(null)")!=0 && gdk_color_parse(color, &tmpcol))
				{
				col=((tmpcol.red>>8)<<16)|((tmpcol.green>>8)<<8)|(tmpcol.blue>>8);
				}

			gint style=0;
			if (ischecked) style|=CHECKSTYLE_CHECKED;
			if (styletoset_weight==PANGO_WEIGHT_BOLD) style|=CHECKSTYLE_BOLD;
			if (styletoset_strike) style|=CHECKSTYLE_STRIKE;

			g_snprintf(tq, sizeof(tq), "INSERT INTO %s (nodeid, name, style, color, ord) VALUES(%d, ?, %d, %lu, 0)", checklisttable_tmpname, selnode->sql3id, style, col);
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

			if (rc)
				{
					goterr=TRUE;
                                        maepad_warning("Error while saving checklist: %s", sqlite3_errmsg(mainview->db));
					break;
				}
			sqlite3_bind_text(stmt, 1, text, strlen(text), SQLITE_TRANSIENT);

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
				}
			sqlite3_finalize(stmt);

			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					goterr=TRUE;
					maepad_warning("Error while saving checklist (2): %s", sqlite3_errmsg(mainview->db));
				}

			g_free(color);
			g_free(text);
						
			}while(gtk_tree_model_iter_next(model, &iter)==TRUE);
		
		break;
		}

	if (textdata)
		g_free(textdata);
	if (sketchdata)
		g_free(sketchdata);
	if (goterr == TRUE)
		{
			show_banner(mainview, _("Error saving memo"));
		}
        busy_leave(mainview);
}

gboolean callback_treeview_button_press(GtkTreeView* treeview, GdkEventButton* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    GtkTreePath* path;

    if (mainview->node_list_longpress_path != NULL) {
        /* Forget old, remembered tree path for longpress */
        gtk_tree_path_free(mainview->node_list_longpress_path);
        mainview->node_list_longpress_path = NULL;
    }

    if (gtk_tree_view_get_path_at_pos(treeview,
                event->x, event->y, &path, NULL, NULL, NULL)) {
        /**
         * Save this path in case we open the longpress menu to
         * set the cursor in the treeview to the correct row.
         *
         * See on_node_menu_show() on how this is further used.
         **/
        mainview->node_list_longpress_path = path;
    }

    mainview->can_show_node_view = TRUE;

    return FALSE;
}

void callback_treeview_celldatafunc(GtkTreeViewColumn * tree_column, GtkCellRenderer * cell, GtkTreeModel * tree_model, GtkTreeIter * iter, gpointer data)
{
	MainView *mainview = ( MainView * ) data;
	g_assert(mainview != NULL && mainview->data != NULL );
	
	nodeData *nd;

	gtk_tree_model_get(tree_model, iter, NODE_DATA, &nd, -1);
	if (nd == NULL)
		return;

	if (nd->namepix == NULL)
		{
			g_object_set(cell, "visible", FALSE, NULL);
		}
	else
		{
			g_object_set(cell, "visible", TRUE, NULL);
			g_object_set(cell, "width", SKETCHNODE_RX, NULL);
			g_object_set(cell, "height", SKETCHNODE_RY, NULL);
		}
}

void callback_treeview_change(GtkTreeSelection * selection, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

        gchar* nodeName = _("View memo");

        if (nd != NULL && nd->name != NULL) {
            nodeName = nd->name;
        }

        /* Show node view with selected node */
        gtk_window_set_title(GTK_WINDOW(mainview->data->node_view),
                             nodeName);

        if (mainview->can_show_node_view) {
            gtk_widget_show(GTK_WIDGET(mainview->data->node_view));
        }

        /* Make sure we don't accidentally collapse any nodes */
	gtk_tree_view_expand_all(GTK_TREE_VIEW(mainview->treeview));

	guint tm=time(NULL);
	if (mainview->cansel_time>0 && mainview->cansel_time+1.0<tm) mainview->cansel_node=NULL;

	if (nd==NULL)
		{
		if (mainview->cansel_node!=NULL)
			{
			maepad_debug("[SELLOGIC] saving %d to unselect all nodes", (mainview->cansel_node)->sql3id);
			saveDataToNode(mainview, (mainview->cansel_node));
			mainview->cansel_node=NULL;
			}
		return;
		}

	if (mainview->cansel_node!=NULL)
		{
		if (nd->sql3id == (mainview->cansel_node)->sql3id)
			{
			mainview->cansel_node=NULL;
			maepad_debug("[SELLOGIC] doubly selected %d, doing nothing", nd->sql3id);
                        prepareUIforNodeChange(mainview, nd->typ);
			return;
			}
		else
			{
                        maepad_debug("[SELLOGIC] saving %d to load new node", (mainview->cansel_node)->sql3id);
			saveDataToNode(mainview, (mainview->cansel_node));
			mainview->cansel_node=NULL;
			}
		}

	if (nd == NULL) return;

        busy_enter(mainview);

	gboolean goterr = TRUE;
	char *textdata = NULL;
	char *blob = NULL;
	int blobsize = 0, textsize = 0;

	char tq[512];

	g_snprintf(tq, sizeof(tq), "SELECT bodytype, body, bodyblob, flags FROM %s WHERE nodeid=%d", datatable_tmpname, nd->sql3id);
	sqlite3_stmt *stmt = NULL;
	const char *dum;
	int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

	if (rc)
		{
			maepad_warning("Error reading (1): %s", sqlite3_errmsg(mainview->db));
		}
	else
		{
			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
					else if (rc == SQLITE_ROW)
						{
							nd->typ = sqlite3_column_int(stmt, 0);
							nd->flags = sqlite3_column_int(stmt, 3);

							prepareUIforNodeChange(mainview, nd->typ);
							if (nd->typ == NODE_TEXT)
								{
									gboolean file_edited_backup = mainview->file_edited;

									blobsize = sqlite3_column_bytes(stmt, 2);
									blob = (char *)sqlite3_column_blob(stmt, 2);

									textdata = (char *)sqlite3_column_text(stmt, 1);
									textsize = sqlite3_column_bytes(stmt, 1);

/*									gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), "", -1);*/
									wp_text_buffer_reset_buffer(mainview->buffer, TRUE);									

									gboolean richtext=FALSE;

									if (blob != NULL)
										{
#if 0
										gboolean oldway=FALSE;
										if (blobsize>8)
											{
											char tst[8];
											strncpy(tst, blob, 8);
											tst[8]=0;
											if (strcmp(tst, "RICHTEXT")==0) oldway=TRUE;
											}
										if (oldway)
											{
											GError *err = NULL;
											GtkTextIter iter;
											gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(mainview->buffer), &iter);
											gtk_text_buffer_deserialize_rich_text(GTK_TEXT_BUFFER(mainview->buffer), &iter, blob, blobsize, TRUE, &err);
											if (err != NULL)
												{
													g_error_free(err);
												}
											else
												{
													richtext=TRUE;
												}
											}
#endif
	
										if (richtext==FALSE)
											{
											wp_text_buffer_load_document_begin(mainview->buffer, TRUE);
											wp_text_buffer_load_document_write(mainview->buffer, blob, blobsize);
											wp_text_buffer_load_document_end(mainview->buffer);
											richtext=TRUE;
											}
										}
									if (richtext==FALSE && !(textdata == NULL || g_utf8_validate(textdata, textsize, NULL) == FALSE))
										{
/*											gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), textdata, textsize);*/
										wp_text_buffer_load_document_begin(mainview->buffer, FALSE);
										wp_text_buffer_load_document_write(mainview->buffer, textdata, textsize);
										wp_text_buffer_load_document_end(mainview->buffer);
										}

									wp_text_buffer_enable_rich_text(mainview->buffer, TRUE);
                                                                        hildon_check_button_set_active(HILDON_CHECK_BUTTON(mainview->menu_button_wordwrap),
                                                                                (nd->flags & NODEFLAG_WORDWRAP)?(TRUE):(FALSE));

									gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(mainview->buffer), FALSE); /*we probably don't need this*/

									callback_undotoggle((gpointer)mainview->buffer, FALSE, mainview); /*we need these*/
									callback_redotoggle((gpointer)mainview->buffer, FALSE, mainview);
									
									if (file_edited_backup==FALSE) mainview->file_edited=FALSE; /*textview changed event toggles this?*/
									goterr = FALSE;
								}
							else if (nd->typ == NODE_SKETCH)
								{
									sketchwidget_wipe_undo(mainview->sk);

                                                                        /* Disable squared and filled mode when opening a sketch */
                                                                        hildon_check_button_set_active(HILDON_CHECK_BUTTON(mainview->menu_button_square), FALSE);
                                                                        hildon_check_button_set_active(HILDON_CHECK_BUTTON(mainview->menu_button_filled), FALSE);

									gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->shapemenuitems[1]), TRUE);
									gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->shapemenuitems[0]), TRUE);

									blobsize = sqlite3_column_bytes(stmt, 2);
									blob = (char *)sqlite3_column_blob(stmt, 2);
									gboolean clear = TRUE;

									if (blob == NULL)
										goterr = FALSE;
									if (blob != NULL)
										{
											maepad_debug("blob size: %d", blobsize);
											GdkPixbufLoader *pl = gdk_pixbuf_loader_new_with_type("png", NULL);
											GError *err = NULL;

											gdk_pixbuf_loader_write(pl, (guchar *) blob, blobsize, &err);
											if (err != NULL)
												{
													maepad_warning("Error loading sketch: %s", err->message);
													g_error_free(err);
													err = NULL;
												}
											gdk_pixbuf_loader_close(pl, NULL);
											GdkPixbuf *pixbuf = gdk_pixbuf_loader_get_pixbuf(pl);

											if (GDK_IS_PIXBUF(pixbuf))
												{
													GtkWidget *skdr = sketchwidget_get_drawingarea(mainview->sk);
													GtkPixmap *skpix = (GtkPixmap *) sketchwidget_get_Pixmap(mainview->sk);

													int w=gdk_pixbuf_get_width(pixbuf);
													int h=gdk_pixbuf_get_height(pixbuf);
													if (w!=skdr->allocation.width || h!=skdr->allocation.height)
														{
														if (w>skdr->allocation.width) w=skdr->allocation.width;
														if (h>skdr->allocation.height) h=skdr->allocation.height;
														sketchwidget_clear_real(mainview->sk);
														}
													gdk_draw_pixbuf(GDK_DRAWABLE(skpix), NULL, pixbuf, 0, 0, 0, 0, w, h, GDK_RGB_DITHER_NONE, 0, 0);

													clear = FALSE;
													goterr = FALSE;

													if (skpix && G_IS_OBJECT(skpix)) g_object_unref(skpix);
												}
											else
												{
													maepad_warning("Error loading pixbuf");
												}
											g_object_unref(pl);
										}
									if (clear == TRUE)
										{
											maepad_message("Clearing sketch widget");
											sketchwidget_clear_real(mainview->sk);
										}
									gtk_widget_queue_draw(sketchwidget_get_drawingarea(mainview->sk));
								}
							else if (nd->typ == NODE_CHECKLIST)
								{
                                                                mainview->checklist_edited = FALSE;
								GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
								g_object_ref(model);
							  gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->listview), NULL);
								gtk_list_store_clear(GTK_LIST_STORE(model));

								g_snprintf(tq, sizeof(tq), "SELECT name, style, color FROM %s WHERE nodeid=%d ORDER BY ord, idx", checklisttable_tmpname, nd->sql3id);
								sqlite3_stmt *stmt2 = NULL;
								const char *dum;
								int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt2, &dum);
								if (rc)
									{
										maepad_warning("Error reading checklist (1) %s", sqlite3_errmsg(mainview->db));
									}
								else
									{
										goterr=FALSE;
										rc = SQLITE_BUSY;
										while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
											{
												rc = sqlite3_step(stmt2);
												if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
													break;
												else if (rc == SQLITE_ROW)
													{
														char *textdata = (char *)sqlite3_column_text(stmt2, 0);
														int style = sqlite3_column_int(stmt2, 1);
														unsigned long col = sqlite3_column_int(stmt2, 2);

														GtkTreeIter    toplevel;
														gtk_list_store_append(GTK_LIST_STORE(model), &toplevel);
														gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_TEXT, textdata, CHECKNODE_CHECKED, FALSE, -1);
														if ((style & CHECKSTYLE_CHECKED)>0) {
                                                                                                                    gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_CHECKED, TRUE, -1);
                                                                                                                    gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_ICON_NAME, "widgets_tickmark_list", -1);
                                                                                                                }
														if ((style & CHECKSTYLE_BOLD)>0) gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_BOLD, PANGO_WEIGHT_BOLD, -1);
														if ((style & CHECKSTYLE_STRIKE)>0) gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_STRIKE, TRUE, -1);

														if (col>0)
															{
															char tmp[10];
															g_snprintf(tmp, sizeof(tmp), "#%02lx%02lx%02lx", ((col & 0xFF0000) >> 16), ((col & 0xFF00) >> 8), (col & 0xFF));
															gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_COLOR, tmp, -1);	
															}

													}
											}
										if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
											{
											maepad_warning("Error reading checklist (2): %s", sqlite3_errmsg(mainview->db));
											goterr=TRUE;
											}
							
										sqlite3_finalize(stmt2);
									}

							  gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->listview), model);
								g_object_unref(model);
								}

							if ((nd->flags & NODEFLAG_SKETCHLINES) > 0)
								gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->sketchlinesmenuitems[1]), TRUE);
							else if ((nd->flags & NODEFLAG_SKETCHGRAPH) > 0)
								gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->sketchlinesmenuitems[2]), TRUE);
							else
								{
									gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->sketchlinesmenuitems[0]), TRUE);
									callback_sketchlines(NULL, mainview->sketchlinesmenuitems[0]);	/*FIXME:ugly */
								}
							gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->tools_pressure), TRUE);

							break;
						}
				}
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE) {
                            maepad_warning("Error reading (2): %s", sqlite3_errmsg(mainview->db));
                        }

			sqlite3_finalize(stmt);
		}

        busy_leave(mainview);

    /* Show / hide the sketch-related menu widgets */
    if (nd->typ == NODE_SKETCH) {
        gtk_widget_show(mainview->menu_button_square);
        gtk_widget_show(mainview->menu_button_filled);
    } else {
        gtk_widget_hide(mainview->menu_button_square);
        gtk_widget_hide(mainview->menu_button_filled);
    }

    /* Show/hide the rich text-related menu widgets */
    if (nd->typ == NODE_TEXT) {
        gtk_widget_show(mainview->menu_button_wordwrap);
    } else {
        gtk_widget_hide(mainview->menu_button_wordwrap);
    }

    /* Show/hide the checklist-related menu widgets */
    if (nd->typ == NODE_CHECKLIST) {
        gtk_widget_show(mainview->menu_button_remove_checked);
    } else {
        gtk_widget_hide(mainview->menu_button_remove_checked);
    }

	if (goterr == TRUE)
		{
			show_banner(mainview, _("Error loading memo"));
		}
}

gboolean treeview_canselect(GtkTreeSelection * selection, GtkTreeModel * model, GtkTreePath * path, gboolean path_currently_selected, gpointer userdata)
{
	MainView *mainview = (MainView *) userdata;
	g_assert(mainview != NULL && mainview->data != NULL);

	if (path_currently_selected)
		{
		GtkTreeIter iter;
	  gtk_tree_model_get_iter(model, &iter, path);
	
	  gtk_tree_model_get(model, &iter, NODE_DATA, &(mainview->cansel_node), -1);
		mainview->cansel_time=time(NULL);
		}

	return (TRUE);
}

gboolean newnodedlg_key_press_cb(GtkWidget * widget, GdkEventKey * event, GtkWidget * dlg)
{
	SketchWidget *s = gtk_object_get_data(GTK_OBJECT(dlg), "sk");

	switch (event->keyval)
		{
		case GDK_F7:
			sketchwidget_redo(s);
			return TRUE;
		case GDK_F8:
			sketchwidget_undo(s);
			return TRUE;
		}
	return FALSE;
}


/* This struct will hold all our toggle buttons, so we can
 * only allow one to be active at a time (i.e. radio buttons) */
typedef struct _newNodeToggleButtons newNodeToggleButtons;
struct _newNodeToggleButtons
{
    GtkWidget *rbt; /* Text */
    GtkWidget *rbs; /* Sketch */
    GtkWidget *rbc; /* Checklist */
};

void show_sketch_widget(GtkWidget *widget, gpointer user_data)
{
    GtkWidget *dialog = (GtkWidget*)user_data;

    /* Show the sketch widget and hide the entry + draw button */
    gtk_widget_show(gtk_object_get_data(GTK_OBJECT(dialog), "al"));
    gtk_widget_hide(gtk_object_get_data(GTK_OBJECT(dialog), "draw_button"));
    gtk_widget_hide(gtk_object_get_user_data(GTK_OBJECT(dialog)));
}

void new_node_dialog(nodeType typ, MainView * mainview)
{
	GtkWidget *dialog, *entry, *but_ok, *vbox, *hbox, *al;
        GtkWidget *rb1, *rb2, *rb3;
        GtkWidget *hb;
        gchar datetime_str[200];
        time_t t_now;
        struct tm *tm_now;
        gboolean datetime_written = FALSE;

        t_now = time(NULL);
        tm_now = localtime(&t_now);
        
        if (tm_now != NULL) {
            if (strftime(datetime_str, sizeof(datetime_str), "%y-%m-%d %H:%M", tm_now) != 0) {
                datetime_written = TRUE;
            }
        }

        if (datetime_written == FALSE) {
            /* Was not able to determine a datetime string - use default */
            maepad_warning("Cannot determine current time");
            strncpy(datetime_str, _("New memo"), sizeof(datetime_str));
            datetime_str[sizeof(datetime_str)-1] = '\0';
        }
        
        newNodeToggleButtons *nntb = g_malloc(sizeof(newNodeToggleButtons));

	dialog = gtk_dialog_new();
        gtk_window_set_title(GTK_WINDOW(dialog), _("Add new memo"));
        gtk_window_set_transient_for(GTK_WINDOW(dialog), mainview_get_dialog_parent(mainview));
        gtk_dialog_set_has_separator(GTK_DIALOG(dialog), FALSE);

	g_signal_connect(G_OBJECT(dialog), "key_press_event", G_CALLBACK(newnodedlg_key_press_cb), dialog);

	vbox = gtk_vbox_new(FALSE, 0);

	hbox = gtk_hbox_new(TRUE, 0);

        /* Text note toggle button */
        rb1 = hildon_gtk_radio_button_new(HILDON_SIZE_FINGER_HEIGHT, NULL);
        gtk_button_set_label(GTK_BUTTON(rb1), _("Rich text"));
        gtk_button_set_image(GTK_BUTTON(rb1), gtk_image_new_from_file(PIXMAPDIR "/text.png"));
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(rb1), TRUE, TRUE, 0);
        nntb->rbt = rb1;

        /* Sketch toggle button */
        rb2 = hildon_gtk_radio_button_new_from_widget(HILDON_SIZE_FINGER_HEIGHT, GTK_RADIO_BUTTON(rb1));
        gtk_button_set_label(GTK_BUTTON(rb2), _("Sketch"));
        gtk_button_set_image(GTK_BUTTON(rb2), gtk_image_new_from_file(PIXMAPDIR "/sketch.png"));
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(rb2), TRUE, TRUE, 0);
        nntb->rbs = rb2;

        /* Checklist toggle button */
        rb3 = hildon_gtk_radio_button_new_from_widget(HILDON_SIZE_FINGER_HEIGHT, GTK_RADIO_BUTTON(rb1));
        gtk_button_set_label(GTK_BUTTON(rb3), _("Checklist"));
        gtk_button_set_image(GTK_BUTTON(rb3), gtk_image_new_from_file(PIXMAPDIR "/checklist.png"));
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(rb3), TRUE, TRUE, 0);
        nntb->rbc = rb3;

        /* Set mode to 0 to get correct styling */
        gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(rb1), FALSE);
        gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(rb2), FALSE);
        gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(rb3), FALSE);

        /* Remember "new note toggle buttons" list */
        gtk_object_set_data(GTK_OBJECT(dialog), "nntb", nntb);
        
	if (typ == NODE_TEXT) {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb1), TRUE);
        } else if (typ == NODE_SKETCH) {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb2), TRUE);
        } else {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb3), TRUE);
        }

        gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	but_ok = gtk_dialog_add_button(GTK_DIALOG(dialog), _("Add"), GTK_RESPONSE_OK);
        gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
	g_signal_connect(G_OBJECT(but_ok), "clicked", G_CALLBACK(callback_new_node_real), dialog);

	gtk_object_set_data(GTK_OBJECT(dialog), "m", mainview);

        hb = gtk_hbox_new(FALSE, 10);
        gtk_box_pack_start(GTK_BOX(hb), gtk_label_new(_("Name:")), FALSE, FALSE, 0);
	entry = hildon_entry_new(HILDON_SIZE_AUTO);
        gtk_entry_set_activates_default(GTK_ENTRY(entry), TRUE);
	gtk_object_set_user_data(GTK_OBJECT(dialog), entry);
	gtk_entry_set_text(GTK_ENTRY(entry), datetime_str);
        gtk_editable_select_region(GTK_EDITABLE(entry), 0, -1);
        gtk_box_pack_start(GTK_BOX(hb), entry, TRUE, TRUE, 0);
        
        /* Sketch widget, hidden by default */
	al = gtk_alignment_new(0.5, 0.5, 0, 0);
	SketchWidget *s = sketchwidget_new(SKETCHNODE_X, SKETCHNODE_Y, TRUE);
	gtk_object_set_data(GTK_OBJECT(dialog), "sk", s);
	gtk_object_set_data(GTK_OBJECT(dialog), "al", al);
	sketchwidget_set_brushsize(s, 2);
	sketchwidget_set_backstyle(s, SKETCHBACK_GRAPH);
	gtk_widget_set_size_request(sketchwidget_get_mainwidget(s), SKETCHNODE_X, SKETCHNODE_Y);
	gtk_container_add(GTK_CONTAINER(al), sketchwidget_get_mainwidget(s));
	gtk_box_pack_start(GTK_BOX(hb), al, FALSE, FALSE, 0);

        /*but_sketch = hildon_button_new_with_text(
			HILDON_SIZE_FINGER_HEIGHT | HILDON_SIZE_AUTO_WIDTH,
			HILDON_BUTTON_ARRANGEMENT_VERTICAL,
			_("Use sketch label"), NULL);

	gtk_object_set_data(GTK_OBJECT(dialog), "draw_button", but_sketch);
        gtk_signal_connect(GTK_OBJECT(but_sketch), "clicked", G_CALLBACK(show_sketch_widget), dialog);
        gtk_box_pack_start(GTK_BOX(hb), but_sketch, FALSE, TRUE, 0);*/
        gtk_box_pack_start(GTK_BOX(vbox), hb, TRUE, FALSE, 0);

        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), vbox);
        
	gtk_widget_grab_focus(entry);

	gtk_widget_show_all(dialog);

        /* Hide the sketch widget at first */
        gtk_widget_hide(al);
	gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
}

void add_new_node(nodeData * node, MainView * mainview, gboolean ischild)
{
	GtkTreeIter parentiter, newiter;
	GtkTreeIter parentiterFilter, newiterFilter;
	GtkTreeModel *model;
	GtkTreeIter *ptr = NULL;
	gboolean setPtr = FALSE;

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

	if (gtk_tree_selection_get_selected(selection, &model, &parentiterFilter)) {
		setPtr = TRUE;
	}

	model = GTK_TREE_MODEL(mainview->main_view_tree_store);
	gtk_tree_model_filter_convert_iter_to_child_iter(
			GTK_TREE_MODEL_FILTER(mainview->main_view_tree_filter),
			&parentiter,
			&parentiterFilter);

	if (setPtr) {
		ptr = &parentiter;
	}

	GtkTreePath *path = NULL;

	unsigned int parentnodeid = 0;

	if (ptr != NULL)
		{
			path = gtk_tree_model_get_path(model, &parentiter);

			if (ischild == FALSE)
				{
					gtk_tree_path_up(path);

					if (gtk_tree_path_get_depth(path) == 0)
						{
					    /* Selected node is a root node */
							ptr = NULL;   /* New node can not have a Parent node */
							gtk_tree_path_down(path);	/*restore path so expand() works */
						}
					else if (gtk_tree_path_get_depth(path) > 0)
						{
					    /* Selected node is a child node */
							if (gtk_tree_model_get_iter(model, &parentiter, path))
								ptr = &parentiter;
						}
				}
		}

	if (ptr != NULL)
		{
			nodeData *nd;

			gtk_tree_model_get(model, ptr, NODE_DATA, &nd, -1);
			if (nd)
				parentnodeid = nd->sql3id;
		}

	node->sql3id = 0;
	do
		{
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			char tq[512];

			/*
			 * FIXME: ord 
			 */
			g_snprintf(tq, sizeof(tq), "INSERT INTO %s (parent, bodytype, name, nameblob, ord) VALUES (%d, %d, ?, ?, 0);", datatable_tmpname, parentnodeid, node->typ);
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

			if (rc)
				{
					maepad_warning("Error inserting (1): %s", sqlite3_errmsg(mainview->db));
					break;
				}
			if (node->name != NULL)
				sqlite3_bind_text(stmt, 1, node->name, strlen(node->name), SQLITE_TRANSIENT);
			else
				sqlite3_bind_text(stmt, 1, NULL, 0, SQLITE_TRANSIENT);

			if (node->namepix != NULL)
				{
					gchar *namepixdata = NULL;
					gsize datalen = 0;

					GError *err = NULL;

					if (gdk_pixbuf_save_to_buffer(node->namepix, &namepixdata, &datalen, "png", &err, NULL) == FALSE)
						{
							namepixdata = NULL;
							datalen = 0;
							maepad_warning("Error saving name: %s", err->message);
							g_error_free(err);
						}
					sqlite3_bind_blob(stmt, 2, namepixdata, datalen, SQLITE_TRANSIENT);
				}
			else
				sqlite3_bind_blob(stmt, 2, NULL, 0, SQLITE_TRANSIENT);

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
				}
			sqlite3_finalize(stmt);
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					maepad_warning("Error inserting (2): %s", sqlite3_errmsg(mainview->db));
					break;
				}
			node->sql3id = sqlite3_last_insert_rowid(mainview->db);
		}
	while(FALSE);

	if (node->sql3id == 0)
		{
			if (node->name)
				g_free(node->name);
			if (node->namepix)
				g_object_unref(node->namepix);
			g_free(node);
			if (path)
				gtk_tree_path_free(path);
			show_banner(mainview, _("Error creating memo"));
			return;
		}

	gtk_tree_store_append(GTK_TREE_STORE(model), &newiter, ptr);

	gtk_tree_store_set(GTK_TREE_STORE(model), &newiter,
                NODE_NAME, format_overview_name(node, NULL),
                NODE_PIXBUF, node->namepix,
                NODE_DATA, node,
                -1);

	if (path)
		{
			mainview->loading=TRUE; /*only when we have a valid parent*/
			gtk_tree_view_expand_row(GTK_TREE_VIEW(mainview->treeview), path, FALSE);
			gtk_tree_path_free(path);
		}

	if (gtk_tree_model_filter_convert_child_iter_to_iter(
			GTK_TREE_MODEL_FILTER(mainview->main_view_tree_filter),
			&newiterFilter,
			&newiter)) {
		/* Only set the selection if the new item is visible -
		 * it could be invisible if the filter doesn't show it */
		gtk_tree_selection_select_iter(selection, &newiterFilter);
	}

	mainview->loading=FALSE;
}

void callback_new_node_real(GtkAction * action, gpointer data)
{
	MainView *mainview;

	GtkWidget *dialog = data;
	GtkWidget *entry = gtk_object_get_user_data(GTK_OBJECT(dialog));

	mainview = gtk_object_get_data(GTK_OBJECT(dialog), "m");
	SketchWidget *s = gtk_object_get_data(GTK_OBJECT(dialog), "sk");
        newNodeToggleButtons *nntb = gtk_object_get_data(GTK_OBJECT(dialog), "nntb");

	nodeType typ = NODE_TEXT;
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(nntb->rbs))) {
            typ = NODE_SKETCH;
        } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(nntb->rbc))) {
            typ = NODE_CHECKLIST;
        }
        g_free(nntb);

	gchar *txt = NULL;

	/*if (GTK_WIDGET_VISIBLE(entry))
		{*/
			txt = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));
			if (strcmp(txt, "") == 0)
				{
					g_free(txt);
					return;
				}/*
		}
	else
		{
			GtkWidget *sdr = sketchwidget_get_drawingarea(s);

			GdkPixmap *spix = sketchwidget_get_Pixmap(s);

			pixbuf = gdk_pixbuf_get_from_drawable(NULL, GDK_DRAWABLE(spix), NULL, 0, 0, 0, 0, sdr->allocation.width, sdr->allocation.height);
			g_object_unref(spix);
			double w, h;
			GdkPixbuf *pixbuf2 = sketchwidget_trim_image(pixbuf, SKETCHNODE_X, SKETCHNODE_Y, &w,
																									 &h, TRUE);
			if (pixbuf2==NULL) return;

			GdkPixbuf *pixbuf3 = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, SKETCHNODE_RX,
																					SKETCHNODE_RY);

			gdk_pixbuf_fill(pixbuf3, 0xffffffff);

			if (w <= SKETCHNODE_RX && h <= SKETCHNODE_RY)
				{
					gdk_pixbuf_copy_area(pixbuf2, 0, 0, w, h, pixbuf3, 0, (SKETCHNODE_RY - h) / 2);
				}
			else
				{
					double neww, newh;

					if (w > h)
						{
							neww = SKETCHNODE_RX;
							newh = (h / w) * SKETCHNODE_RX;
						}
					else
						{
							newh = SKETCHNODE_RY;
							neww = (w / h) * SKETCHNODE_RY;
						}
					if (newh > SKETCHNODE_RY)
						newh = SKETCHNODE_RY;

					GdkPixbuf *tmpbuf = gdk_pixbuf_scale_simple(pixbuf2, neww, newh,
																											GDK_INTERP_BILINEAR);

					gdk_pixbuf_copy_area(tmpbuf, 0, 0, neww, newh, pixbuf3, 0, (SKETCHNODE_RY - newh) / 2);

					gdk_pixbuf_unref(tmpbuf);
				}

			pixbuf = pixbuf3;
			gdk_pixbuf_unref(pixbuf2);
		}*/

	nodeData *node;

	node = g_malloc(sizeof(nodeData));
	node->typ = typ;
	node->name = txt;
	node->namepix = NULL;

	/*if (GTK_WIDGET_VISIBLE(entry))
		{
                    node->name = txt;
		}
	else
		{
			node->namepix = pixbuf;
		}*/

	node->lastMod = 0;
	node->flags = 0;
	node->sql3id = 0;

	mainview->newnodedialog_createchild = FALSE;
	add_new_node(node, mainview, FALSE);

	sketchwidget_destroy(s);
	gtk_widget_destroy(dialog);
	mainview->file_edited = TRUE;
}

/*
 * delete node 
 */
void callback_file_delete_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	if (getSelectedNode(mainview) == NULL) {
            show_banner(mainview, _("Select a memo first"));
            return;
        }

        if (show_confirmation(mainview, _("Delete selected memo?"))) {
            mainview->can_show_node_view = FALSE;
            callback_delete_node_real(mainview);
            gtk_widget_hide(GTK_WIDGET(mainview->data->node_view));
        }
}

/*
 * Callback for Rename Menuitem
 */
void callback_file_rename_node(GtkAction * action, gpointer data)
{
    MainView *mainview = (MainView*)data;
    g_assert(mainview != NULL && mainview->data != NULL);

    /* Get the selected node */
    nodeData *sel_node = getSelectedNode(mainview);
    if (sel_node == NULL) {
        /* Do nothing, if no node has been selected */
        show_banner(mainview, _("Select a memo first"));
        return;
    }

    if (sel_node->namepix != NULL) {
        /* the memo has a graphical label, cannot edit! */
        show_banner(mainview, _("Cannot rename memos with sketch name"));
        return;
    }

    gchar* new_name = show_line_edit_dialog(mainview, _("Rename memo"), _("New name:"), _("Rename"), sel_node->name);

    /* Only rename node when user accepted the new name */
    if (new_name != NULL) {
        callback_rename_node_real(mainview, new_name);
        g_free(new_name);
    }
}

void callback_rename_node_real(MainView* mainview, gchar* new_name)
{
    GtkTreeIter iter, filterIter;
    GtkTreeModel *model;
    nodeData *nd = NULL;

    /* Get the selected node */
    GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

    if (!gtk_tree_selection_get_selected(selection, &model, &filterIter)) {
        return;
    }

    /* Convert filter iter to (backend) model iter for rename */
    model = GTK_TREE_MODEL(mainview->main_view_tree_store);
    gtk_tree_model_filter_convert_iter_to_child_iter(
		    GTK_TREE_MODEL_FILTER(mainview->main_view_tree_filter),
		    &iter,
		    &filterIter);

    gtk_tree_model_get (model, &iter, NODE_DATA, &nd, -1);

    if (nd == NULL) {
        return;
    }

    /* Update the database */
    sqlite3_stmt *stmt = NULL;

    char* sql = sqlite3_mprintf("UPDATE %s SET name='%q' WHERE nodeid=%d", datatable_tmpname, new_name, nd->sql3id);

    int rc = sqlite3_prepare(mainview->db, sql, strlen(sql), &stmt, NULL);
    if (rc == SQLITE_OK) {
        rc = SQLITE_BUSY;
        while (rc == SQLITE_BUSY) {
            rc = sqlite3_step(stmt);
            if (rc == SQLITE_DONE) {
                /* Update in the database was successful - now update the rest */

                /* Update the noteData */
                g_free(nd->name);
                nd->name = g_strdup(new_name);

                /* Update the window title of node_view */
                gtk_window_set_title(GTK_WINDOW(mainview->data->node_view), nd->name);

                /* Update the value in the tree store */
                gtk_tree_store_set (GTK_TREE_STORE(model), &iter, NODE_NAME, format_overview_name(nd, new_name), -1);

                break;
            } else if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE) {
                maepad_warning("Error updating node: %s", sqlite3_errmsg(mainview->db));
                break;
            }
        }
        sqlite3_finalize(stmt);
    } else {
        maepad_warning("Error preparing DB update query: %s", sqlite3_errmsg(mainview->db));
    }

    sqlite3_free(sql);

    mainview->file_edited = TRUE;
}

void callback_file_export_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd=getSelectedNode(mainview);
	if (nd == NULL)
		{
			show_banner(mainview, _("Select a memo first"));
			return;
		}

	gchar *nodename=nd->name;
	if (nodename==NULL) nodename=_("saved memo");

	if (nd->typ == NODE_TEXT)
		{
/*
		GtkTextIter begin, end;
		gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER(mainview->buffer), &begin, &end);
		gchar *text = gtk_text_buffer_get_slice(GTK_TEXT_BUFFER(mainview->buffer), &begin, &end, TRUE);
*/
			GString *gstr=g_string_sized_new(4096);
			wp_text_buffer_save_document(mainview->buffer, (WPDocumentSaveCallback)(wp_savecallback), gstr);
			gint textlen=gstr->len;
			gchar *text=g_string_free(gstr, FALSE);

		if (text==NULL || !strcmp(text, ""))
			{
			show_banner(mainview, _("Memo is empty"));
			}
		else
			{
			gchar *fn = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, nodename, "html");
			if (fn!=NULL)
				{
				GnomeVFSResult vfs_result;
				GnomeVFSHandle *handle = NULL;
				GnomeVFSFileSize out_bytes;
  			vfs_result = gnome_vfs_create(&handle, fn, GNOME_VFS_OPEN_WRITE, 0, 0600);
		    if ( vfs_result != GNOME_VFS_OK ) {
					show_banner(mainview, _("Export failed"));
		    	}
    		else
    			{
    			gnome_vfs_write(handle, text, textlen, &out_bytes);
    			gnome_vfs_close(handle);
    			if (out_bytes==strlen(text)) show_banner(mainview, _("Exported"));
    															else show_banner(mainview, _("Export incomplete"));
    			}
				g_free(fn);
				}
			}
		g_free(text);
		}
	else if (nd->typ == NODE_SKETCH)
		{
		GdkPixmap *skpix = sketchwidget_get_Pixmap(mainview->sk);
		GtkWidget *skdr = sketchwidget_get_drawingarea(mainview->sk);
		GdkPixbuf *pixbuf = gdk_pixbuf_get_from_drawable(NULL, GDK_DRAWABLE(skpix), NULL, 0, 0, 0, 0, skdr->allocation.width, skdr->allocation.height);
		if (pixbuf==NULL)
			{
			show_banner(mainview, _("Memo is empty"));
			}		
		else
			{
			gchar *fn = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, nodename, "png");
			if (fn!=NULL)
				{
				if (gdk_pixbuf_save(pixbuf, fn, "png", NULL, NULL)==FALSE)
					{
					show_banner(mainview, _("Export failed"));
					}
				else
					{
					show_banner(mainview, _("Exported"));
					}
				g_free(fn);
				}
			}
		g_object_unref(skpix);
		}
	else if (nd->typ == NODE_CHECKLIST)
		{
					show_banner(mainview, _("Export of checklists not possible yet"));
		}
}

/*
 *	callback from menu item
 *	move selected node down (switch node with next sibling), don't change level of node
 */
void callback_move_down_node(GtkAction * action, gpointer data)
{
	GtkTreeIter iter, filterIter;
	GtkTreeModel *model;
	
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
	gtk_tree_selection_get_selected(selection, &model, &filterIter);

	model = GTK_TREE_MODEL(mainview->main_view_tree_store);
	gtk_tree_model_filter_convert_iter_to_child_iter(
			GTK_TREE_MODEL_FILTER(mainview->main_view_tree_filter),
			&iter,
			&filterIter);

	GtkTreeIter old_iter = iter;/*save pointer to old iter, we will need it during swap nodes*/

	if (gtk_tree_model_iter_next(model,&iter)==FALSE)/*get next node*/
		return;

	GtkTreeStore *treeStore = GTK_TREE_STORE(model);
	gtk_tree_store_swap(treeStore,&iter,&old_iter);

	mainview->file_edited = TRUE;/*we have made changes , if required show "save changes?" dialog in future*/
}

/*
 *	callback from menu item
 *	move selected node down (switch node with prev sibling), don't change level of node
 */
void callback_move_up_node(GtkAction * action, gpointer data)
{
	GtkTreeIter iter, filterIter;
	GtkTreeModel *model;

	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
	gtk_tree_selection_get_selected(selection, &model, &filterIter);

	model = GTK_TREE_MODEL(mainview->main_view_tree_store);
	gtk_tree_model_filter_convert_iter_to_child_iter(
			GTK_TREE_MODEL_FILTER(mainview->main_view_tree_filter),
			&iter,
			&filterIter);

	GtkTreeIter old_iter=iter;/*save pointer to old iter, we will need it during swap nodes*/

	if (tree_model_iter_prev(model,&iter)==FALSE)/*get previous node*/
		return;

	GtkTreeStore *treeStore = GTK_TREE_STORE(model);
	gtk_tree_store_swap(treeStore,&old_iter,&iter);/*do move*/

	mainview->file_edited = TRUE;/*we have made changes , if required show "save changes?" dialog in future*/
}


/*
 *	simple execute of sql command which is stored in sql_string[]
 */
gboolean exec_command_on_db(MainView *mainview,char sql_string[])
{
	sqlite3_stmt *stmt = NULL;
	const char* dum;
	gboolean db_query_result = FALSE;

	int rc = sqlite3_prepare (mainview->db, sql_string, strlen(sql_string), &stmt, &dum);
	
	if (rc) {
                maepad_warning("Error preparing DB update query: %s", sqlite3_errmsg(mainview->db));
		return FALSE;
	}

	rc = SQLITE_BUSY;
	while (rc == SQLITE_BUSY) {
		rc = sqlite3_step (stmt);
		if (rc == SQLITE_DONE) {
			db_query_result = TRUE;
			break;
		}
		else if(rc == SQLITE_ERROR || rc== SQLITE_MISUSE) {
                        maepad_warning("Error updating node: %s", sqlite3_errmsg(mainview->db));
			break;
		}
		sqlite3_finalize(stmt);
	}	
	return TRUE;
}

/*
 *	it is used in gtk_tree_model_foreach function to update ord value for all nodes (it's usefull for move up, move down node)
 */
gboolean foreach_func_update_ord (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter, MainView *mainview)
{
	nodeData *node;	
	gtk_tree_model_get(model, iter, NODE_DATA, &node, -1);
	/*we need index of node on actual level*/
	gint index=get_branch_node_index(path);

	/*prepare to execute update command,and exec it*/
	char sql_command[512];
	g_snprintf (sql_command, sizeof(sql_command), "UPDATE %s SET ord=\"%d\" WHERE nodeid=%d",datatable_tmpname, index, node->sql3id);
	exec_command_on_db(mainview,sql_command);

	/*we don't want break gtk_tree_model_foreach function,until we call this func on each node -  so we return always FALSE*/
	return FALSE; 
}

/*
 *	return id number of iter (id number which is used to identify in sql database of nodes)
 */
int get_node_id_on_tmp_db(GtkTreeModel *model,GtkTreeIter *iter)
{
	if (iter==NULL) 
		return 0;/*we got ROOT parent here*/

	nodeData *node;
	gtk_tree_model_get(model, iter, NODE_DATA, &node, -1);
	return node->sql3id;
}

/*
 *	get index of node in current branch
 */
gint get_branch_node_index(GtkTreePath *path)
{
	int depth=gtk_tree_path_get_depth(path);
	gint *indicies = gtk_tree_path_get_indices(path);
	
	return indicies[depth-1];
}

/*
 *	similiar with gtk_tree_model_iter_next (), but opposite
 */
gboolean tree_model_iter_prev(GtkTreeModel *tree_model,GtkTreeIter *iter)
{
	GtkTreePath *path = gtk_tree_model_get_path(tree_model, iter);
	
	if (path==NULL){
		fprintf(stderr,"Error: path is null\n");
		return FALSE;
	}

	if (gtk_tree_path_prev(path)==FALSE)
		return FALSE;
	
	gtk_tree_model_get_iter(tree_model, iter,path);

	return TRUE;
}

gboolean ref2iter(GtkTreeModel * model, GtkTreeRowReference * ref, GtkTreeIter * iter)
{
	gboolean res = FALSE;
	GtkTreePath *path = gtk_tree_row_reference_get_path(ref);

	if (gtk_tree_model_get_iter(model, iter, path))
		{
			res = TRUE;
		}
	gtk_tree_path_free(path);
	return (res);
}

GtkTreeRowReference *iter2ref(GtkTreeModel * model, GtkTreeIter * iter)
{
	GtkTreeRowReference *ref;

	GtkTreePath *path = gtk_tree_model_get_path(model, iter);

	ref = gtk_tree_row_reference_new(model, path);
	gtk_tree_path_free(path);
	return (ref);
}

void move_nodes_up(GtkTreeModel * model, GtkTreeRowReference * topnode, GtkTreeRowReference * newtop)
{
	GtkTreeIter topiter;

	if (ref2iter(model, topnode, &topiter) == FALSE)
		return;

	GtkTreeIter child;

	if (gtk_tree_model_iter_children(model, &child, &topiter))
		{
			GtkTreeRowReference *ref;
			GList *rr_list = NULL, *node;

			do
				{
					ref = iter2ref(model, &child);
					rr_list = g_list_append(rr_list, ref);
				}
			while(gtk_tree_model_iter_next(model, &child));

			/*
			 * got a reflist for all children
			 */

			for(node = rr_list; node; node = node->next)
				{
					ref = (GtkTreeRowReference *) (node->data);
					if (ref2iter(model, ref, &child))
						{
							GtkTreeIter newtopiter, newiter;
							GtkTreeIter *newtopiterptr;

							if (ref2iter(model, newtop, &newtopiter))
								newtopiterptr = &newtopiter;
							else
								newtopiterptr = NULL;

							nodeData *node;

							gtk_tree_model_get(model, &child, NODE_DATA, &node, -1);

							gtk_tree_store_append(GTK_TREE_STORE(model), &newiter, newtopiterptr);
							gtk_tree_store_set(GTK_TREE_STORE(model), &newiter, NODE_NAME, format_overview_name(node, NULL), NODE_PIXBUF, node->namepix, NODE_DATA, node, -1);

							GtkTreeRowReference *newref = iter2ref(model, &newiter);

							move_nodes_up(model, ref, newref);
							gtk_tree_row_reference_free(newref);

							gtk_tree_store_remove(GTK_TREE_STORE(model), &child);
						}
					gtk_tree_row_reference_free(ref);
				}

			g_list_free(rr_list);
		}

}

void callback_delete_node_real(MainView* mainview)
{
	GtkTreeIter iter, filterIter;
	GtkTreeModel *model;

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

	if (!gtk_tree_selection_get_selected(selection, &model, &filterIter))
		return;

	/* Convert filter iter to (backend) model iter */
	model = GTK_TREE_MODEL(mainview->main_view_tree_store);
	gtk_tree_model_filter_convert_iter_to_child_iter(
			GTK_TREE_MODEL_FILTER(mainview->main_view_tree_filter),
			&iter,
			&filterIter);

	nodeData *nd;

	gtk_tree_model_get(model, &iter, NODE_DATA, &nd, -1);
	if (!nd)
		return;

	mainview->file_edited = TRUE;

	unsigned int sql3id = nd->sql3id;

	if (nd->name)
		g_free(nd->name);

	/*
	 * g_free(nd->data);
	 * if (nd->pix) g_object_unref(nd->pix);
	 */
	g_free(nd);

	GtkTreeRowReference *upref = NULL, *ref = NULL;

	GtkTreePath *path = gtk_tree_model_get_path(model, &iter);

	ref = gtk_tree_row_reference_new(model, path);
	if (gtk_tree_path_up(path))
		upref = gtk_tree_row_reference_new(model, path);
	gtk_tree_path_free(path);

	g_object_ref(mainview->main_view_tree_filter);
	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), NULL);

	move_nodes_up(model, ref, upref);

	if (ref2iter(model, ref, &iter))
		{
			char tq[512];

			g_snprintf(tq, sizeof(tq), "SELECT parent FROM %s WHERE nodeid=%d", datatable_tmpname, sql3id);
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);
			unsigned int sql3parentid = 0;

			if (rc)
				{
					maepad_warning("Error finding children for delete: %s", sqlite3_errmsg(mainview->db));
				}
			else
				{
					rc = SQLITE_BUSY;
					while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
						{
							rc = sqlite3_step(stmt);
							if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
								break;
							else if (rc == SQLITE_ROW)
								{
									sql3parentid = sqlite3_column_int(stmt, 0);
									break;
								}
						}
					sqlite3_finalize(stmt);

					g_snprintf(tq, sizeof(tq), "UPDATE %s SET parent=%d WHERE parent=%d;", datatable_tmpname, sql3parentid, sql3id);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                            maepad_warning("Error moving nodes up one level");
                                        } else
						{
							g_snprintf(tq, sizeof(tq), "DELETE FROM %s WHERE nodeid=%d;", datatable_tmpname, sql3id);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                                            maepad_warning("Error deleting node");
                                                        }

                                                        /* Delete all checklist items that do not have
                                                         * a node anymore (= orphaned checklist items) */
							g_snprintf(tq, sizeof(tq), "DELETE FROM %s WHERE nodeid NOT IN (SELECT nodeid FROM %s);", checklisttable_tmpname, datatable_tmpname);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                                            maepad_warning("Error deleting orphaned checklist items");
                                                        }
						}
				}
			gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
		}

	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview),
			GTK_TREE_MODEL(mainview->main_view_tree_filter));
	g_object_unref(mainview->main_view_tree_filter);

	/* Carry out a refilter to show/hide remaining items */
	gtk_tree_model_filter_refilter(mainview->main_view_tree_filter);

	gtk_tree_row_reference_free(ref);
	gtk_tree_row_reference_free(upref);

	gtk_tree_view_expand_all(GTK_TREE_VIEW(mainview->treeview));

}

void callback_edit_clear(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);
	nodeData *nd = getSelectedNode(mainview);

        if (show_confirmation(mainview, _("Remove all contents of this memo?"))) {
            switch (nd->typ) {
                case NODE_TEXT:
                    gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), "", 0);
                    break;
                case NODE_SKETCH:
                    sketchwidget_clear(mainview->sk);
                    break;
                case NODE_CHECKLIST:
                    gtk_list_store_clear(GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview))));
                    break;
                default:
                    g_assert_not_reached();
            }
        }
}

/*
 * cut 
 */
void callback_edit_cut(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd->typ == NODE_TEXT)
		gtk_text_buffer_cut_clipboard(GTK_TEXT_BUFFER(mainview->buffer), mainview->clipboard, TRUE);
	else if (nd->typ == NODE_SKETCH)
		{
		if (sketchwidget_cut(mainview->sk, mainview->clipboard)==FALSE)
								show_banner(mainview, _("Error cutting"));
		}								
	else if (nd->typ == NODE_CHECKLIST)
					show_banner(mainview, _("Unimplemented"));

}

/*
 * copy 
 */
void callback_edit_copy(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd->typ == NODE_TEXT)
		gtk_text_buffer_copy_clipboard(GTK_TEXT_BUFFER(mainview->buffer), mainview->clipboard);
	else if (nd->typ == NODE_SKETCH)
		{
		if (sketchwidget_copy(mainview->sk, mainview->clipboard)==FALSE)
					show_banner(mainview, _("Error copying"));
		}
	else if (nd->typ == NODE_CHECKLIST)
        {
            /* Copy all selected entries as multiline text (1 line per entry) */
            GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
            GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview));
            
            gint selected_rows = gtk_tree_selection_count_selected_rows(selection);
            GList* l = gtk_tree_selection_get_selected_rows(selection, NULL);
            
            GtkTreeIter iter;
            gchar *str_data;
            
            gchar **entries = g_malloc0(sizeof(gchar*)*selected_rows+1);
            gint entries_idx = 0;
            
            GList* cur = l;
            while(cur) {
            	GtkTreePath *path = cur->data;
            
            	if (gtk_tree_model_get_iter(model, &iter, path)) {
                     gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, CHECKNODE_TEXT, &(entries[entries_idx++]), -1);
                }
            	gtk_tree_path_free(path);
                
            	cur = cur->next;
            }
            
            g_list_free(l);
            str_data = g_strjoinv("\n", entries);
            g_strfreev(entries);
            gtk_clipboard_set_text(mainview->clipboard, str_data, -1);
            g_free(str_data);
            
            str_data = g_strdup_printf(_("Copied %d entries"), selected_rows);
            show_banner(mainview, str_data);
            g_free(str_data);
            
        }
}

/*
 * paste 
 */
void callback_edit_paste(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd->typ == NODE_TEXT)
		gtk_text_buffer_paste_clipboard(GTK_TEXT_BUFFER(mainview->buffer), mainview->clipboard, NULL, TRUE);
	else if (nd->typ == NODE_SKETCH)
		{
		if (sketchwidget_paste(mainview->sk, mainview->clipboard)==FALSE)
							show_banner(mainview, _("Error pasting"));
		}
	else if (nd->typ == NODE_CHECKLIST) {
		/* Paste string from clipboard as new item */
		callback_checklist_paste(mainview);
	}

	mainview->file_edited = TRUE;
}

gint cb_popup(GtkWidget * widget, GdkEvent * event)
{
	GtkMenu *menu;
	GdkEventButton *event_button;

	/*
	 * The "widget" is the menu that was supplied when 
	 * * g_signal_connect_swapped() was called.
	 */
	menu = GTK_MENU(widget);
	event_button = (GdkEventButton *) event;
	if (event->type == GDK_BUTTON_PRESS && event_button->button == 3)
		{
			gtk_menu_popup(menu, NULL, NULL, NULL, NULL, event_button->button, event_button->time);
			return TRUE;
		}
	return FALSE;
}


/**
 * on_node_menu_show(GtkWidget*, gpointer)
 *
 * This is called when the longpress menu is shown in the main
 * window. In the Hildon UI mode that we are using, this does
 * not automatically select the touched node, so we need to set
 * the cursor here so that the functions in the menu operate on
 * the node that the user touched (i.e. the expected one).
 *
 * The value of mainview->node_list_longpress_path has been set
 * by callback_treeview_button_press on the buttn press event.
 **/
void
on_node_menu_show(GtkWidget* nodemenu, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;

    if (mainview->node_list_longpress_path != NULL) {
        /* Set the cursor, but don't open the node view */
        mainview->can_show_node_view = FALSE;
        gtk_tree_view_set_cursor(GTK_TREE_VIEW(mainview->treeview),
                mainview->node_list_longpress_path, NULL, FALSE);

        /* Dispose the path (we don't need it anymore) */
        gtk_tree_path_free(mainview->node_list_longpress_path);
        mainview->node_list_longpress_path = NULL;
    }
}

/*
 * close 
 */
gboolean closefile(MainView * mainview)
{
	saveCurrentData(mainview);

	if (mainview->file_edited)
		{
			HildonNote *hn = HILDON_NOTE(hildon_note_new_confirmation_add_buttons(GTK_WINDOW(mainview->data->main_view), _("Save changes?"), _("Yes"), CONFRESP_YES, _("No"), CONFRESP_NO, _("Cancel"), CONFRESP_CANCEL, NULL, NULL));
			gint answer = gtk_dialog_run(GTK_DIALOG(hn));
			gtk_widget_destroy(GTK_WIDGET(hn));

			if (answer == CONFRESP_CANCEL)
				return (FALSE);
			else if (answer == CONFRESP_YES)
				{
					if (mainview->file_name == NULL)
						{
							mainview->file_name = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, "maemopaddata", "db");
						}
					write_buffer_to_file(mainview);
				}
		}

	if (mainview->db)
		sqlite3_close(mainview->db);
	mainview->db = NULL;
	return (TRUE);
}

gboolean callback_file_close(GtkAction * action, gpointer data)
{

	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);
	if (closefile(mainview) == FALSE)
		return(FALSE);

	gtk_main_quit();
	return(TRUE);
}

void callback_file_new_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeType typ = NODE_SKETCH;

	nodeData *nd = getSelectedNode(mainview);

	if (nd != NULL)
		typ = nd->typ;

	new_node_dialog(typ, mainview);
}

/*
 * new 
 */
void callback_file_new(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	gchar *filename = NULL;

	filename = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, "memos", "db");
	if (filename == NULL) {
            return;
        }

	if (closefile(mainview) == FALSE) {
            return;
        }

	new_file(mainview);

	do
		{
                busy_enter(mainview);

		int rc;
	
		rc = sqlite3_open(filename, &mainview->db);
		if (rc)
			{
				show_banner(mainview, _("Cannot create database"));
				maepad_warning("Can't create database %s: %s", filename, sqlite3_errmsg(mainview->db));
				break;
			}
			
		sqlite3_exec(mainview->db, "PRAGMA synchronous = OFF;", NULL, NULL, NULL);

		char tq[512];
	
		g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", misctable_name, misctable);
		sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
	
		g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", datatable_name, datatable);
		if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
			{
				maepad_warning("Cannot create data table");
				show_banner(mainview, _("Error creating data table"));
				break;
			}

		g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", checklisttable_name, checklisttable);
		if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
			{
				maepad_warning("Cannot create checklist table");
				show_banner(mainview, _("Error creating checklist table"));
				break;
			}
	
		if (mainview->db)
			sqlite3_close(mainview->db);
		mainview->db = NULL;
	
		mainview->file_name = filename;
		mainview->file_edited = FALSE;
		read_file_to_buffer(mainview);

/*add a starter memo*/
		nodeData *node;
		node = g_malloc(sizeof(nodeData));
		node->typ = NODE_SKETCH;
		node->name = g_strdup(_("My first memo"));
		node->namepix = NULL;
		node->lastMod = 0;
		node->flags = 0;
		node->sql3id = 0;
		add_new_node(node, mainview, TRUE);
		/*gtk_paned_set_position(GTK_PANED(mainview->hpaned), 180);*/
		write_buffer_to_file(mainview);

	}while(FALSE);
        busy_reset(mainview);
}

gboolean reset_ctree(GtkTreeModel * model, GtkTreePath * path, GtkTreeIter * iter, gpointer data)
{
	nodeData *node;

	gtk_tree_model_get(model, iter, NODE_DATA, &node, -1);
	if (node)
		{
			if (node->name)
				g_free(node->name);
			if (node->namepix)
				g_object_unref(node->namepix);
			g_free(node);

			gtk_tree_store_set(GTK_TREE_STORE(model), iter, NODE_DATA, NULL, -1);
		}
	return (FALSE);
}

void new_file(MainView * mainview)
{
        busy_enter(mainview);
	/*
	 * clear buffer, filename and free buffer text 
	 */
	gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), "", -1);
	mainview->file_name = NULL;
	mainview->file_edited = FALSE;
	mainview->newnodedialog_createchild = TRUE;

	/* The filter is the "filtered down" list that's displayed in the
	 * treeview and the model is the underlying real data */
	GtkTreeModel *filter = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview));
	GtkTreeModel *model = GTK_TREE_MODEL(mainview->main_view_tree_store);

	g_object_ref(filter);
	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), NULL);

	gtk_tree_model_foreach(model, (GtkTreeModelForeachFunc) reset_ctree, (gpointer) mainview);

	/*
	 * crashing bastard
	 * gtk_tree_store_clear(GTK_TREE_STORE(model));
	 */
	GtkTreePath *path = gtk_tree_path_new_from_indices(0, -1);
	GtkTreeIter iter;

	if (gtk_tree_model_get_iter(model, &iter, path))
		{
			do
				{
					gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
				}
			while(gtk_tree_store_iter_is_valid(GTK_TREE_STORE(model), &iter));
		}
	gtk_tree_path_free(path);

	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), filter);
	g_object_unref(filter);

        busy_leave(mainview);
}

/*
 * open 
 */
void callback_file_open(GtkAction * action, gpointer data)
{
	gchar *filename = NULL;
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	filename = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_OPEN, NULL, NULL);
        if (filename == NULL) {
            return;
        }

	if (closefile(mainview) == FALSE) {
            return;
        }

	open_file(filename, mainview);
	g_free(filename);
}

gboolean open_file(gchar * filename, MainView * mainview)
{
	gboolean ret=FALSE;

        /* Don't open nodes when opening a file */
        mainview->can_show_node_view = FALSE;

        busy_enter(mainview);

	while(filename != NULL)
		{
			struct stat s;

			if (stat(filename, &s) == -1) break;

			mainview->file_name = g_strdup(filename);
			gboolean res = read_file_to_buffer(mainview);

			if (res == FALSE)
				{
					g_free(mainview->file_name);
					mainview->file_name = NULL;
					break;
				}
			mainview->file_edited = FALSE;
			ret=TRUE;
			break;
		}

        busy_leave(mainview);
	return(ret);
}

void callback_about_link(GtkAboutDialog *about, const gchar *link, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);
	osso_rpc_run_with_defaults(mainview->data->osso, "osso_browser", OSSO_BROWSER_OPEN_NEW_WINDOW_REQ, NULL,
			DBUS_TYPE_STRING, link, DBUS_TYPE_INVALID);
}

void callback_about(GtkAction * action, gpointer data)
{
	MainView* mainview = (MainView *)data;
        he_about_dialog_present(mainview_get_dialog_parent(mainview),
                                NULL /* auto-detect app name */,
                                "maepad",
                                VERSION,
                                _("A node-based memory pad for Maemo"),
                                _("(c) 2010 Thomas Perl"),
                                "http://thp.io/2010/maepad/",
                                "https://garage.maemo.org/tracker/?group_id=1291",
                                "http://thp.io/2010/maepad/donate");
}

gboolean callback_live_search_refilter(HildonLiveSearch *livesearch, gpointer user_data)
{
	MainView *mainview = (MainView*)user_data;
	/* Avoid showing the node view when filtering */
        mainview->can_show_node_view = FALSE;
	return FALSE;
}

/*
 * save 
 */
void callback_file_save(GtkAction * action, gpointer data)
{
	gchar *filename = NULL;
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	/*
	 * check is we had a new file 
	 */
	if (mainview->file_name != NULL)
		{
			write_buffer_to_file(mainview);
		}
	else
		{
			filename = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, "maemopaddata", "db");
			/*
			 * if we got a file name from chooser -> save file 
			 */
			if (filename != NULL)
				{
					mainview->file_name = filename;
					write_buffer_to_file(mainview);
					mainview->file_edited = FALSE;
				}
		}
}

void callback_shapemenu(GtkAction * action, GtkWidget * wid)
{
	gint style = GPOINTER_TO_INT(gtk_object_get_user_data(GTK_OBJECT(wid)));
	MainView* mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");
	g_assert(mainview != NULL);

        if (style >= 0 && style < SKETCHSHAPE_COUNT) {
            /* We use the sketch widget's enum for available styles */
            sketchwidget_set_shape(mainview->sk, style);

            /* Draw the correct indicator for the current shape */
            GtkWidget* pix = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(wid), "i"));
            g_assert(pix != NULL);
            gtk_widget_show(pix);
            gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(mainview->shape_tb), pix);
        } else {
            /* Fail. We shouldn't get here at all! */
            g_error("Invalid style ID from shape menu: %d", style);
        }
}



void callback_eraser(GtkAction * action, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mainview->eraser_tb))) {
            /* Eraser on: Set pen color to white */
            GdkColor white = { 0, 0xFFFF, 0xFFFF, 0xFFFF };
            sketchwidget_set_brushcolor(mainview->sk, white);
        } else {
            /* Eraser off: Set default color again (or black) */
            GdkColor black = {0, 0, 0, 0};
            if (mainview->current_color == NULL) {
                mainview->current_color = gdk_color_copy(&black);
            }
            sketchwidget_set_brushcolor(mainview->sk, *(mainview->current_color));
        }
}

void callback_menu(GtkAction * action, GtkWidget * menu)
{
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 0, GDK_CURRENT_TIME);
}

void callback_brushsizetb(GtkAction * action, MainView *mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);
        callback_menu(NULL, mainview->brushsizemenu);
}

void callback_brushsize(GtkAction * action, GtkWidget * wid)
{
	int bsize = (int)gtk_object_get_user_data(GTK_OBJECT(wid));
	MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");

	g_assert(mainview != NULL && mainview->data != NULL);

	sketchwidget_set_brushsize(mainview->sk, bsize);

	GtkWidget *pix = gtk_object_get_data(GTK_OBJECT(wid), "i");

	gtk_widget_show(pix);
	gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(mainview->brushsize_tb), pix);
}

void callback_sketchlines(GtkAction * action, GtkWidget * wid)
{
	int style = (int)gtk_object_get_user_data(GTK_OBJECT(wid));
	MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");

	g_assert(mainview != NULL);

	nodeData *nd = getSelectedNode(mainview);
	gboolean doit = FALSE;

	if (nd != NULL && nd->typ == NODE_SKETCH)
		{
			nd->flags &= ~NODEFLAG_SKETCHLINES;
			nd->flags &= ~NODEFLAG_SKETCHGRAPH;
/*			sketchwidget_set_edited(mainview->sk, TRUE);*/ /*we call this on openfile, so this messes things up*/
			doit = TRUE;
		}

	if (style == 0)
		{
			sketchwidget_set_backstyle(mainview->sk, SKETCHBACK_NONE);
		}
	else if (style == 1)
		{
			sketchwidget_set_backstyle(mainview->sk, SKETCHBACK_LINES);
			if (doit == TRUE)
				nd->flags |= NODEFLAG_SKETCHLINES;
		}
	else if (style == 2)
		{
			sketchwidget_set_backstyle(mainview->sk, SKETCHBACK_GRAPH);
			if (doit == TRUE)
				nd->flags |= NODEFLAG_SKETCHGRAPH;
		}

	GtkWidget *pix = gtk_object_get_data(GTK_OBJECT(wid), "i");

	gtk_widget_show(pix);
	gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(mainview->sketchlines_tb), pix);
}

void callback_color(GtkAction* action, MainView* mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);
	if (nd == NULL) return;

        HeSimpleColorDialog* dialog = HE_SIMPLE_COLOR_DIALOG(he_simple_color_dialog_new());
        gtk_window_set_transient_for(GTK_WINDOW(dialog), mainview_get_dialog_parent(mainview));

        if (mainview->current_color) {
            he_simple_color_dialog_set_color(dialog, mainview->current_color);
        }

        if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK) {
            gtk_widget_destroy(GTK_WIDGET(dialog));
            return;
        }

        gdk_color_free(mainview->current_color);
        mainview->current_color = he_simple_color_dialog_get_color(dialog);

        gtk_widget_destroy(GTK_WIDGET(dialog));

        switch (nd->typ) {
            case NODE_SKETCH:
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->eraser_tb), FALSE);
		sketchwidget_set_brushcolor(mainview->sk, *(mainview->current_color));
                break;
            case NODE_CHECKLIST:
                { /* Put in a separate block to allow new local variables */
                    GtkTreeModel* model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
                    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview));
                    GList* selected = gtk_tree_selection_get_selected_rows(selection, NULL);

                    gchar* color_string = g_strdup_printf("#%02x%02x%02x",
                            mainview->current_color->red >> 8,
                            mainview->current_color->green >> 8,
                            mainview->current_color->blue >> 8);

                    GList* cur = selected;
                    while (cur != NULL) {
                        GtkTreePath* path = cur->data;
                        GtkTreeIter iter;
                        if (gtk_tree_model_get_iter(model, &iter, path)) {
                            gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_COLOR, color_string, -1);
                        }
                        gtk_tree_path_free(path);
                        cur = cur->next;
                    }

                    g_list_free(selected);
                    g_free(color_string);
                }
                break;
            default:
                g_assert_not_reached();
        }
}

void callback_color_invoke(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);
	gtk_button_clicked(GTK_BUTTON(mainview->colorbutton_tb));
}



void callback_pressure(GtkAction * action, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

nodeData *nd = getSelectedNode(mainview);

if (nd == NULL)
	return;
if (nd->typ != NODE_SKETCH)
	return;
	
/* pressure sensitivity disabled for now...
mainview->sk->pressuresensitivity=gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(mainview->tools_pressure));*/
}


void callback_wordwrap(GtkWidget* widget, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    nodeData* nd = getSelectedNode(mainview);
    GtkWrapMode wrap_mode;

    if (nd == NULL || nd->typ != NODE_TEXT) {
        return;
    }

    if (hildon_check_button_get_active(HILDON_CHECK_BUTTON(mainview->menu_button_wordwrap))) {
        nd->flags |= NODEFLAG_WORDWRAP;
        wrap_mode = GTK_WRAP_WORD_CHAR;
    } else {
        nd->flags &= ~NODEFLAG_WORDWRAP;
        wrap_mode = GTK_WRAP_NONE;
    }

    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(mainview->textview), wrap_mode);
}


void callback_font(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd == NULL)
		return;
	if (nd->typ != NODE_TEXT)
		return;

HildonFontSelectionDialog *dialog = HILDON_FONT_SELECTION_DIALOG(hildon_font_selection_dialog_new(NULL, NULL));

gboolean gotsel=wp_text_buffer_has_selection(mainview->buffer);
/*gotsel=FALSE;*/

WPTextBufferFormat fmt;
wp_text_buffer_get_attributes(mainview->buffer, &fmt, gotsel);

gint ri=0;
if (fmt.text_position==TEXT_POSITION_SUPERSCRIPT) ri=1;
else if (fmt.text_position==TEXT_POSITION_SUBSCRIPT) ri=-1;

g_object_set(G_OBJECT(dialog),
	"family-set", fmt.cs.font,
	"family", wp_get_font_name(fmt.font),
	"size-set", fmt.cs.font_size,
	"size", wp_font_size[fmt.font_size],
	"color-set", fmt.cs.color,
	"color", &fmt.color,
	"bold-set", fmt.cs.bold,
	"bold", fmt.bold,
	"italic-set", fmt.cs.italic,
	"italic", fmt.italic,
	"underline-set", fmt.cs.underline,
	"underline", fmt.underline,
	"strikethrough-set", fmt.cs.strikethrough,
	"strikethrough", fmt.strikethrough,
	"position-set", fmt.cs.text_position,
	"position", ri,
	NULL);

gtk_widget_show_all(GTK_WIDGET(dialog));
if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
	{
	gboolean bold, italic, underline, strikethrough;
	gchar *family = NULL;
	gint size, position;
	GdkColor *color=NULL;
	gboolean set_family, set_size, set_bold, set_italic, set_underline, set_strikethrough, set_color, set_position;

	g_object_get(G_OBJECT(dialog), "family", &family, "size", &size, "bold", &bold, "italic", &italic,
	"underline", &underline, "strikethrough", &strikethrough,
	"family-set", &set_family, "size-set", &set_size, "bold-set", &set_bold, "italic-set", &set_italic,
	"underline-set", &set_underline, "strikethrough-set", &set_strikethrough,
	"color", &color, "color-set", &set_color, "position", &position, "position-set", &set_position,
	NULL);

	wp_text_buffer_get_attributes(mainview->buffer, &fmt, FALSE);
	fmt.cs.font=fmt.cs.font_size=fmt.cs.strikethrough=fmt.cs.color=fmt.cs.bold=fmt.cs.italic=fmt.cs.underline=fmt.cs.text_position=0;

	if (set_family) { fmt.font=wp_get_font_index(family, 1); fmt.cs.font=1; }
	if (set_size) { fmt.font_size=wp_get_font_size_index(size, 16); fmt.cs.font_size=1; }

	if (set_strikethrough)
		{
		fmt.cs.strikethrough=1;
		fmt.strikethrough=strikethrough;
		}

	if (set_color)
		{
/*
GLIB WARNING ** GLib-GObject - IA__g_object_set_valist: object class `GtkTextTag' has no property named `'
*/
		fmt.cs.color=1;
		fmt.color.pixel=color->pixel;
		fmt.color.red=color->red;
		fmt.color.green=color->green;
		fmt.color.blue=color->blue;
		}

	if (set_position)
		{
		if (position==1) ri=TEXT_POSITION_SUPERSCRIPT;
		else if (position==-1) ri=TEXT_POSITION_SUBSCRIPT;
		else ri=TEXT_POSITION_NORMAL;

		fmt.cs.text_position=1;
		fmt.text_position=ri;
		}

	if (set_bold)
		{
		fmt.cs.bold=1;
		fmt.bold=bold;
		}
	if (set_italic)
		{
		fmt.cs.italic=1;
		fmt.italic=italic;
		}
	if (set_underline)
		{
		fmt.cs.underline=1;
		fmt.underline=underline;
		}

	wp_text_buffer_set_format(mainview->buffer, &fmt);
	}
		
gtk_widget_destroy(GTK_WIDGET(dialog));
}

void callback_fontstyle(GtkAction * action, GtkWidget * wid)
{
MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");
g_assert(mainview != NULL && mainview->data != NULL);

nodeData *nd = getSelectedNode(mainview);

if (nd == NULL)
	return;
if (nd->typ == NODE_TEXT)
	{
	gboolean act=gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(wid));
	
	gint style = (gint)gtk_object_get_data(GTK_OBJECT(wid), "s");
	wp_text_buffer_set_attribute(mainview->buffer, style, (gpointer)act);
	}
else if (nd->typ == NODE_CHECKLIST)
	{
	gboolean act=gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(wid));
	gint style = (gint)gtk_object_get_data(GTK_OBJECT(wid), "s");
	if (style!=WPT_BOLD && style!=WPT_STRIKE && style!=WPT_LEFT) return;

	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
	GList* l=gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview)), NULL);

	gint styletoset_weight=PANGO_WEIGHT_NORMAL;
	gboolean styletoset_strike=FALSE;
	gboolean checkit=FALSE;

	if (style==WPT_BOLD && act==TRUE) styletoset_weight=PANGO_WEIGHT_BOLD;
	else if (style==WPT_STRIKE && act==TRUE) styletoset_strike=TRUE;
	else if (style==WPT_LEFT && act==TRUE) checkit=TRUE;

 	GList* cur=l;
 	while(cur)
 		{
 		GtkTreePath *path=cur->data;

		GtkTreeIter iter;
		if (gtk_tree_model_get_iter(model, &iter, path))
			{
		  if (style==WPT_BOLD) gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_BOLD, styletoset_weight, -1);
		  else if (style==WPT_STRIKE) gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_STRIKE, styletoset_strike, -1);
		  else if (style==WPT_LEFT) {
                      gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_CHECKED, checkit, -1);
                      if (checkit) {
                          gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_ICON_NAME, "widgets_tickmark_list", -1);
                      } else {
                          gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_ICON_NAME, NULL, -1);
                      }
                  }
			}
 		gtk_tree_path_free(path);
 		cur=cur->next;
 		}

	g_list_free(l);
        mainview->checklist_edited = TRUE;
	}
}

void
callback_sharing(GtkWidget* widget, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    nodeData* nd = getSelectedNode(mainview);
    gchar* filename = g_strdup("/tmp/untitled.png");
    gchar* uri = NULL;

    if (nd == NULL || nd->typ != NODE_SKETCH) {
        show_banner(mainview, _("Only sketches can be shared"));
        return;
    }

    if (nd->name != NULL) {
        g_free(filename);
        filename = g_strdup_printf("/tmp/%s.png", nd->name);
    }

    busy_enter(mainview);
    GdkPixmap* skpix = sketchwidget_get_Pixmap(mainview->sk);
    GtkWidget* skdr = sketchwidget_get_drawingarea(mainview->sk);
    GdkPixbuf* pixbuf = gdk_pixbuf_get_from_drawable(NULL,
            GDK_DRAWABLE(skpix), NULL, 0, 0, 0, 0,
            skdr->allocation.width, skdr->allocation.height);

    if (pixbuf == NULL) {
        show_banner(mainview, _("Memo is empty"));
        goto cleanup;
    }

    if (!gdk_pixbuf_save(pixbuf, filename, "png", NULL, NULL)) {
        show_banner(mainview, _("File export failed"));
        goto cleanup;
    }

    uri = g_strdup_printf("file://%s", filename);

    sharing_dialog_with_file(mainview->data->osso,
            mainview_get_dialog_parent(mainview),
            uri);

cleanup:
    g_object_unref(skpix);
    g_free(filename);
    g_free(uri);
    busy_leave(mainview);
}

void
callback_remove_checked(GtkWidget* widget, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    nodeData* nd = getSelectedNode(mainview);
    GtkTreeIter iter;
    GtkTreeModel* model = NULL;
    GList* checked_items = NULL;
    gchar* question = NULL;
    gint count = 0;

    if (nd == NULL || nd->typ != NODE_CHECKLIST) {
        return;
    }

    /* Get a list of checked items */
    model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
    if (!gtk_tree_model_get_iter_first(model, &iter)) {
        return;
    }

    busy_enter(mainview);
    do {
        gboolean checked = FALSE;
        gtk_tree_model_get(model, &iter, CHECKNODE_CHECKED, &checked, -1);

        if (checked) {
            GtkTreePath* path = gtk_tree_model_get_path(model, &iter);

            checked_items = g_list_append(checked_items,
                    gtk_tree_row_reference_new(model, path));
            count++;

            gtk_tree_path_free(path);
        }
    } while (gtk_tree_model_iter_next(model, &iter));
    busy_leave(mainview);

    if (checked_items == NULL) {
        show_banner(mainview, _("No checked items in checklist"));
        return;
    }

    question = g_strdup_printf(N_("Remove %d checked item?", "Remove %d checked items?", count), count);
    if (show_confirmation(mainview, question)) {
        /* Remove the checklist items from the list */
        checklist_remove_rowrefs(mainview, checked_items, TRUE);
    } else {
        /* Free the allocated row references + list */
        GList* cur = checked_items;
        while (cur != NULL) {
            GtkTreeRowReference* rowref = cur->data;
            gtk_tree_row_reference_free(rowref);
            cur = cur->next;
        }
        g_list_free(checked_items);
    }
    g_free(question);
}

void callback_textbuffer_move(WPTextBuffer *textbuffer, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

/*
gboolean gotsel=wp_text_buffer_has_selection(mainview->buffer);

_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), gotsel);
_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->italic_tb), gotsel);
_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->underline_tb), gotsel);
_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->bullet_tb), gotsel);
*/
WPTextBufferFormat fmt;
wp_text_buffer_get_attributes(mainview->buffer, &fmt, FALSE/*gotsel*/);

g_signal_handlers_block_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_block_by_func(mainview->italic_tb, callback_fontstyle, mainview->italic_tb);
g_signal_handlers_block_by_func(mainview->underline_tb, callback_fontstyle, mainview->underline_tb);
g_signal_handlers_block_by_func(mainview->bullet_tb, callback_fontstyle, mainview->bullet_tb);

gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), fmt.bold);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->italic_tb), fmt.italic);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->underline_tb), fmt.underline);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bullet_tb), fmt.bullet);

g_signal_handlers_unblock_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_unblock_by_func(mainview->italic_tb, callback_fontstyle, mainview->italic_tb);
g_signal_handlers_unblock_by_func(mainview->underline_tb, callback_fontstyle, mainview->underline_tb);
g_signal_handlers_unblock_by_func(mainview->bullet_tb, callback_fontstyle, mainview->bullet_tb);
}

gint wp_savecallback(const gchar *buffer, GString * gstr)
{
gstr=g_string_append(gstr, buffer);
return(0);
}

void callback_undo(GtkAction * action, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd == NULL) return;
	
	if (nd->typ == NODE_SKETCH) sketchwidget_undo(mainview->sk);
	else if (nd->typ == NODE_TEXT) wp_text_buffer_undo(mainview->buffer);
}

void callback_redo(GtkAction * action, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd == NULL) return;
	
	if (nd->typ == NODE_SKETCH) sketchwidget_redo(mainview->sk);
	else if (nd->typ == NODE_TEXT) wp_text_buffer_redo(mainview->buffer);
}

void callback_undotoggle(gpointer widget, gboolean st, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	gtk_widget_set_sensitive(GTK_WIDGET(mainview->undo_tb), st);
}

void callback_redotoggle(gpointer widget, gboolean st, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	gtk_widget_set_sensitive(GTK_WIDGET(mainview->redo_tb), st);
}

gboolean close_cb(GtkWidget * widget, GdkEventAny * event, MainView * mainview)
{
	callback_file_close(NULL, mainview);
	return (TRUE);
}

void
mainview_click_on_current_node(MainView* mainview)
{
    GtkTreeSelection* selection = NULL;
    GtkTreeModel* model = NULL;
    GtkTreeIter iter;

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
    if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
        GtkTreePath* path = gtk_tree_model_get_path(model, &iter);

        mainview->can_show_node_view = TRUE;
        gtk_tree_view_set_cursor(GTK_TREE_VIEW(mainview->treeview),
                path, NULL, FALSE);

        gtk_tree_path_free(path);
    }
}

gboolean
on_main_view_key_press(GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    gboolean result = FALSE;
    gboolean visible = FALSE;

    g_object_get(G_OBJECT(mainview->main_view_live_search),
		    "visible", &visible,
		    NULL);

    if (visible) {
        /* Live search is active - only handle tree navigation */
        switch (event->keyval) {
            case GDK_KP_Enter:
            case GDK_Return:
                /* Open selected memo in node view */
                mainview_click_on_current_node(mainview);
                result = TRUE;
                break;
            case GDK_Down:
                /* Goto (=select) next memo */
                mainview->can_show_node_view = FALSE;
                nodelist_select(mainview, TREEVIEW_SELECT_NEXT);
                result = TRUE;
                break;
            case GDK_Up:
                /* Goto (=select) previous memo */
                mainview->can_show_node_view = FALSE;
                nodelist_select(mainview, TREEVIEW_SELECT_PREVIOUS);
                result = TRUE;
                break;
            default:
                break;
        }

        /* Return early, so we don't handle normal input */
        return result;
    }

    switch (event->keyval) {
        case GDK_KP_Enter:
        case GDK_Return:
        case GDK_l:
        case GDK_L:
            /* Open selected memo in node view */
            mainview_click_on_current_node(mainview);
            result = TRUE;
            break;
        case GDK_w:
        case GDK_W:
            /* Save changes to file ("write buffer") */
            callback_file_save(NULL, mainview);
            result = TRUE;
            break;
        case GDK_BackSpace:
            /* Delete selected node (with confirmation) */
            callback_file_delete_node(NULL, mainview);
            result = TRUE;
            break;
        case GDK_o:
        case GDK_O:
            /* Add new memo to list */
            callback_file_new_node(NULL, mainview);
            result = TRUE;
            break;
        case GDK_r:
        case GDK_R:
            /* Replace (edit) name of selected memo */
            callback_file_rename_node(NULL, mainview);
            result = TRUE;
            break;
        case GDK_g:
            if (mainview->main_view_prev_keyval == GDK_g) {
                /* Goto (=select) first memo */
                mainview->can_show_node_view = FALSE;
                nodelist_select(mainview, TREEVIEW_SELECT_FIRST);

                /* Don't remember this key for the next keypress */
                mainview->main_view_prev_keyval_reset = TRUE;

                result = TRUE;
            }
            break;
        case GDK_G:
            /* Goto (=select) last memo */
            mainview->can_show_node_view = FALSE;
            nodelist_select(mainview, TREEVIEW_SELECT_LAST);
            result = TRUE;
            break;
        case GDK_j:
        case GDK_J:
        case GDK_Down:
            /* Goto (=select) next memo */
            mainview->can_show_node_view = FALSE;
            nodelist_select(mainview, TREEVIEW_SELECT_NEXT);
            result = TRUE;
            break;
        case GDK_k:
        case GDK_K:
        case GDK_Up:
            /* Goto (=select) previous memo */
            mainview->can_show_node_view = FALSE;
            nodelist_select(mainview, TREEVIEW_SELECT_PREVIOUS);
            result = TRUE;
            break;
        case GDK_slash:
            /* Start live search */
            gtk_widget_show(GTK_WIDGET(mainview->main_view_live_search));
            /**
             * TODO: It would be nice if we would focus the entry here
             * already, but the entry is private to HildonLiveSearch.
             *
             * See Maemo bug 11458
             **/
            result = TRUE;
            break;
        default:
            /* Ignore keypress (consume it!) */
            result = TRUE;
            break;
    }

    return result;
}

gboolean
on_node_view_key_press(GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    nodeData* nd = getSelectedNode(mainview);
    gboolean result = FALSE;

    if (nd == NULL) {
        return FALSE;
    }

    switch (event->keyval) {
        case GDK_w:
        case GDK_W:
            if (nd->typ != NODE_TEXT) {
                /* Save changes to file ("write buffer") */
                callback_file_save(NULL, mainview);
                result = TRUE;
            }
            break;
        case GDK_BackSpace:
            if (event->state & GDK_SHIFT_MASK) {
                /* Remove current memo (with confirmation) */
                callback_file_delete_node(NULL, mainview);
                result = TRUE;
            } else if (nd->typ == NODE_CHECKLIST) {
                /* Remove current item (with confirmation) */
                callback_checklist_delete(NULL, mainview);
                result = TRUE;
            } else if (nd->typ == NODE_SKETCH) {
                /* Undo last sketch action */
                callback_undo(NULL, mainview);
                result = TRUE;
            }
            break;
        case GDK_space:
            if (nd->typ == NODE_CHECKLIST) {
                /* Toggle check of current item */
                maepad_toggle_gtk_toggle_tool_button(mainview->check_tb);
                result = TRUE;
            } else if (nd->typ == NODE_SKETCH) {
                /* Toggle eraser tool */
                maepad_toggle_gtk_toggle_tool_button(mainview->eraser_tb);
                result = TRUE;
            }
            break;
        case GDK_f:
        case GDK_F:
            if (nd->typ != NODE_TEXT) {
                /* Toggle fullscreen mode */
                callback_fullscreen(NULL, mainview);
                result = TRUE;
            }
            break;
        case GDK_h:
        case GDK_H:
            if (nd->typ != NODE_TEXT) {
                /* Hide node view, go to main view ("left") */
                gtk_widget_hide(GTK_WIDGET(mainview->data->node_view));
                result = TRUE;
            }
            break;
        case GDK_o:
        case GDK_O:
            if (nd->typ == NODE_CHECKLIST) {
                /* Prepend if using uppercase "o" */
                mainview->checklist_prepend = ((event->keyval) == GDK_O);
                /* Insert new checklist item */
                callback_checklist_add(NULL, mainview);
                result = TRUE;
            }
            break;
        case GDK_d:
            if (nd->typ == NODE_CHECKLIST) {
                if (mainview->node_view_prev_keyval == GDK_d) {
                    /* Yank selected items, then remove them silently */
                    callback_edit_copy(NULL, mainview);
                    callback_checklist_delete_real(mainview);
                    show_banner(mainview, _("Item yanked to clipboard"));

                    /* Don't remember this key for the next keypress */
                    mainview->node_view_prev_keyval_reset = TRUE;

                    result = TRUE;
                }
            }
            break;
        case GDK_y:
            if (nd->typ == NODE_CHECKLIST) {
                if (mainview->node_view_prev_keyval == GDK_y) {
                    /* Non-destructive yanking of items */
                    callback_edit_copy(NULL, mainview);
                    show_banner(mainview, _("Item yanked to clipboard"));

                    /* Don't remember this key for the next keypress */
                    mainview->node_view_prev_keyval_reset = TRUE;

                    result = TRUE;
                }
            }
            break;
        case GDK_p:
        case GDK_P:
            if (nd->typ == NODE_CHECKLIST) {
                /* Prepend if using uppercase "P" */
                mainview->checklist_prepend = ((event->keyval) == GDK_P);
                /* Paste text in clipboard as items */
                callback_edit_paste(NULL, mainview);
                result = TRUE;
            }
            break;
        case GDK_g:
            if (nd->typ == NODE_CHECKLIST) {
                if (mainview->node_view_prev_keyval == GDK_g) {
                    /* Goto (=select) first item */
                    checklist_select(mainview, TREEVIEW_SELECT_FIRST);

                    /* Don't remember this key for the next keypress */
                    mainview->node_view_prev_keyval_reset = TRUE;

                    result = TRUE;
                }
            }
            break;
        case GDK_G:
            if (nd->typ == NODE_CHECKLIST) {
                /* Goto (=select) last item */
                checklist_select(mainview, TREEVIEW_SELECT_LAST);
                result = TRUE;
            }
            break;
        case GDK_j:
        case GDK_J:
        case GDK_Down:
            if (nd->typ == NODE_CHECKLIST) {
                /* Goto (=select) next item */
                checklist_select(mainview, TREEVIEW_SELECT_NEXT);
                result = TRUE;
            } else if (nd->typ == NODE_SKETCH) {
                /* Undo last sketch operation */
                callback_undo(NULL, mainview);
                result = TRUE;
            }
            break;
        case GDK_k:
        case GDK_K:
        case GDK_Up:
            if (nd->typ == NODE_CHECKLIST) {
                /* Goto (=select) previous item */
                checklist_select(mainview, TREEVIEW_SELECT_PREVIOUS);
                result = TRUE;
            } else if (nd->typ == NODE_SKETCH) {
                /* Redo last sketch operation */
                callback_redo(NULL, mainview);
                result = TRUE;
            }
            break;
        case GDK_a:
        case GDK_A:
            if (nd->typ == NODE_CHECKLIST) {
                /* Append text to current node */
                checklist_edit_selected(mainview, LINE_EDIT_MODE_APPEND);
                result = TRUE;
            }
            break;
        case GDK_r:
        case GDK_R:
        case GDK_KP_Enter:
        case GDK_Return:
            if (nd->typ == NODE_CHECKLIST) {
                /* Replace (edit) text of current node */
                checklist_edit_selected(mainview, LINE_EDIT_MODE_DEFAULT);
                result = TRUE;
            }
            break;
        case GDK_i:
        case GDK_I:
            if (nd->typ == NODE_CHECKLIST) {
                /* Prepend (insert) text to current node */
                checklist_edit_selected(mainview, LINE_EDIT_MODE_PREPEND);
                result = TRUE;
            }
            break;
        default:
            /* do nothing */
            break;
    }

    return result;
}

gboolean
on_main_view_key_release(GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;

    if (mainview->main_view_prev_keyval_reset) {
        /* Forget last-pressed key value */
        mainview->main_view_prev_keyval = GDK_VoidSymbol;
        mainview->main_view_prev_keyval_reset = FALSE;
    } else {
        /* Remember last keyval for double-press bindings */
        mainview->main_view_prev_keyval = event->keyval;
    }

    return FALSE;
}

gboolean
on_node_view_key_release(GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;

    if (mainview->node_view_prev_keyval_reset) {
        /* Forget last-pressed key value */
        mainview->node_view_prev_keyval = GDK_VoidSymbol;
        mainview->node_view_prev_keyval_reset = FALSE;
    } else {
        /* Remember last keyval for double-press bindings */
        mainview->node_view_prev_keyval = event->keyval;
    }

    return FALSE;
}

gboolean
on_checklist_longpress_timeout(gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    callback_checklist_edit(NULL, mainview);
    mainview->checklist_longpress_source_id = 0;
    return FALSE;
}

gboolean
on_checklist_button_press(GtkWidget* widget, GdkEventButton* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    GtkTreePath* path;
    GtkTreeViewColumn* column;
    GtkTreeSelection* selection;

    if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(widget),
                event->x,
                event->y,
                &path,
                &column,
                NULL,
                NULL)) {
        selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(widget));

        if (gtk_tree_selection_path_is_selected(selection, path)) {
            if (column == mainview->checklist_column_check) {
                /* Check column touched - toggle checklist item */
                maepad_toggle_gtk_toggle_tool_button(mainview->check_tb);
            } else if (column == mainview->checklist_column_text) {
                mainview->checklist_longpress_source_id = g_timeout_add(
                        CHECKLIST_LONGPRESS_EDIT_DELAY,
                        on_checklist_longpress_timeout,
                        mainview);
            }
            return TRUE;
        }

        gtk_tree_path_free(path);
    }

    return FALSE;
}

void
checklist_abort_longpress(MainView* mainview)
{
    /* Abort the longpress action on the checklist */
    if (mainview->checklist_longpress_source_id != 0) {
        g_source_remove(mainview->checklist_longpress_source_id);
        mainview->checklist_longpress_source_id = 0;
    }
}

gboolean
on_checklist_button_release(GtkWidget* widget, GdkEventButton* event, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    checklist_abort_longpress(mainview);
    return FALSE;
}

gboolean
on_checklist_start_panning(HildonPannableArea* area, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    checklist_abort_longpress(mainview);
    return FALSE;
}

void callback_fullscreen(GtkToolButton* tool_button, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    gtk_window_fullscreen(GTK_WINDOW(mainview->data->node_view));
}

void
callback_sketch_button_toggled(HildonCheckButton* button, gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    gboolean active = hildon_check_button_get_active(button);

    if (GTK_WIDGET(button) == GTK_WIDGET(mainview->menu_button_square)) {
        sketchwidget_set_shift(mainview->sk, active);
    } else if (GTK_WIDGET(button) == GTK_WIDGET(mainview->menu_button_filled)) {
        sketchwidget_set_fillmode(mainview->sk, active);
    }
}

void callback_buffer_modified(GtkAction * action, gpointer data)
{
MainView *mainview = (MainView *) data;
g_assert(mainview != NULL && mainview->data != NULL);

mainview->file_edited = TRUE;
}

GtkTreeRowReference *read_sqlite3_data(MainView * mainview, unsigned int parentid, GtkTreeRowReference * parenttree, unsigned int selected, GtkTreeStore * model)
{
	GtkTreeRowReference *resref = NULL;

	char q[256];

	g_snprintf(q, sizeof(q), "SELECT nodeid, bodytype, name, nameblob, lastmodified, flags FROM %s WHERE parent=%d ORDER BY ord", datatable_tmpname, parentid);

	sqlite3_stmt *stmt = NULL;
	const char *dum;
	int rc = sqlite3_prepare(mainview->db, q, strlen(q), &stmt, &dum);

	if (rc)
		{
			maepad_warning("Error reading SQLite3 data: %s", sqlite3_errmsg(mainview->db));
			return (NULL);
		}

	rc = SQLITE_BUSY;
	while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
		{
			rc = sqlite3_step(stmt);
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
				break;
			else if (rc == SQLITE_ROW)
				{
					int nodeid = sqlite3_column_int(stmt, 0);
					int typ = sqlite3_column_int(stmt, 1);
					const unsigned char *name = sqlite3_column_text(stmt, 2);
					const unsigned char *nameblob = sqlite3_column_text(stmt, 3);
					int lastmod = sqlite3_column_int(stmt, 4);
					int flags = sqlite3_column_int(stmt, 5);

					if ((typ != NODE_TEXT && typ != NODE_SKETCH && typ != NODE_CHECKLIST) || (name == NULL && nameblob == NULL)) {
                                            maepad_warning("Unknown node type in database: %d (skipping)", typ);
                                            continue;
                                        }

					nodeData *node = g_malloc(sizeof(nodeData));

					node->sql3id = nodeid;
					node->typ = typ;
					node->flags = flags;
					node->name = NULL;
					node->namepix = NULL;
					if (name != NULL) {
						node->name = g_strdup((char *)name);
                                        } else {
                                                node->name = g_strdup(_("Unnamed memo"));
                                        }
					/*if (nameblob != NULL)
						{
							int blobsize = sqlite3_column_bytes(stmt, 3);

							GdkPixbufLoader *pl = gdk_pixbuf_loader_new_with_type("png", NULL);
							GError *err = NULL;

							gdk_pixbuf_loader_write(pl, (guchar *) nameblob, blobsize, &err);
							if (err != NULL)
								{
									fprintf(stderr, "Error loading nodename! %s\n", err->message);
									g_error_free(err);
									err = NULL;
								}
							gdk_pixbuf_loader_close(pl, NULL);
							GdkPixbuf *pixbuf = gdk_pixbuf_loader_get_pixbuf(pl);

							if (GDK_IS_PIXBUF(pixbuf))
								node->namepix = pixbuf;
						}*/
					node->lastMod = lastmod;

					GtkTreeIter parentiter, newiter;
					void *par = NULL;

					if (parenttree != NULL)
						{
							GtkTreePath *pa = gtk_tree_row_reference_get_path(parenttree);

							gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &parentiter, pa);
							gtk_tree_path_free(pa);
							par = &parentiter;
						}

					gtk_tree_store_append(model, &newiter, par);
					gtk_tree_store_set(model, &newiter, NODE_NAME, format_overview_name(node, NULL), NODE_PIXBUF, node->namepix, NODE_DATA, node, -1);

					GtkTreePath *pa = gtk_tree_model_get_path(GTK_TREE_MODEL(model), &newiter);

					GtkTreeRowReference *newref = gtk_tree_row_reference_new(GTK_TREE_MODEL(model), pa);

					if (selected == nodeid)
						resref = newref;

					gtk_tree_path_free(pa);
					GtkTreeRowReference *r = read_sqlite3_data(mainview, nodeid, newref, selected,
																										 model);

					if (resref != newref)
						gtk_tree_row_reference_free(newref);

					if (r != NULL)
						{
							if (resref == NULL)
								resref = r;
							else
								gtk_tree_row_reference_free(r);	/*safeguard */
						}
				}
		}

	if (stmt)
		sqlite3_finalize(stmt);

	return (resref);							/*ref to supposed-to-be-selected treeitem */
}

/*
 * read file 
 */
gboolean read_file_to_buffer(MainView * mainview)
{
	char tq[512];

	g_assert(mainview != NULL);
	gboolean res = FALSE;

	gchar *filename = mainview->file_name;

	new_file(mainview);
	mainview->file_name = filename;
	mainview->loading=TRUE;

        maepad_message("Reading database file: %s", filename);

	int rc;
	sqlite3_stmt *stmt = NULL;

	rc = sqlite3_open(filename, &mainview->db);
	do
		{
			if (rc)
				{
					maepad_warning("Cannot open database %s: %s", filename, sqlite3_errmsg(mainview->db));
					break;
				}
				
			sqlite3_exec(mainview->db, "PRAGMA synchronous = OFF;", NULL, NULL, NULL);

			char *q = "SELECT skey, sval FROM settings";
			const char *dum;

			rc = sqlite3_prepare(mainview->db, q, strlen(q), &stmt, &dum);
			if (rc)
				{
					maepad_warning("Error reading settings: %s", sqlite3_errmsg(mainview->db));
					break;
				}

			unsigned int selectedCard = 0;
			unsigned int curDataVersion = 0;
			unsigned int curChecklistVersion = 0;

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
					else if (rc == SQLITE_ROW)
						{
							const gchar* col_key = (const gchar*)sqlite3_column_text(stmt, 0);
							const gchar* col_val = (const gchar*)sqlite3_column_text(stmt, 1);
							if (!strcmp(col_key, "selectedNode"))
								{
									gint tmp = atoi((char *)col_val);

									if (tmp > 0)
										selectedCard = tmp;
								}
							if (!strcmp(col_key, "dataVersion"))
								{
									gint tmp = atoi((char *)col_val);

									if (tmp > 0)
										curDataVersion = tmp;
								}
							if (!strcmp(col_key, "checklistVersion"))
								{
									gint tmp = atoi((char *)col_val);

									if (tmp > 0)
										curChecklistVersion = tmp;
								}
							if (!strcmp(col_key, "newNodeDlgCreateChild"))
								{
									gint tmp = atoi((char *)col_val);

									mainview->newnodedialog_createchild = TRUE;
									if (tmp == 0)
										mainview->newnodedialog_createchild = FALSE;
								}
							if (!strcmp(col_key, "brushSize"))
								{
									gint tmp = atoi((char *)col_val);
									if (tmp>0) sk_set_brushsize(mainview, tmp);
								}
							if (!strcmp(col_key, "brushColor"))
								{
									unsigned long tmp = atol((char *)col_val);
									GdkColor c2;
						
									c2.red = ((tmp & 0xFF0000) >> 16) << 8;
									c2.green = ((tmp & 0xFF00) >> 8) << 8;
									c2.blue = (tmp & 0xFF) << 8;
									sketchwidget_set_brushcolor(mainview->sk, c2);

                                                                        if (mainview->current_color != NULL) {
                                                                            gdk_color_free(mainview->current_color);
                                                                        }
                                                                        mainview->current_color = gdk_color_copy(&c2);
								}

						}
				}
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					maepad_warning("Error reading data: %s", sqlite3_errmsg(mainview->db));
					break;
				}

			if (stmt) {
				sqlite3_finalize(stmt);
				stmt = NULL;
			}

			gboolean resback = FALSE;

			while(curDataVersion < datatableversion)
				{
					if (curDataVersion == 0)
						{
							g_snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

							g_snprintf(tq, sizeof(tq), "ALTER TABLE %s RENAME TO %s", datatable_name, datatable_backupname);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                                            maepad_warning("Error backing up table %s to %s", datatable_name, datatable_backupname);
                                                            break;
                                                        }
							resback = TRUE;

							g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", datatable_name, datatable);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                                            maepad_warning("Error creating table: %s", datatable_name);
                                                            break;
                                                        }
							g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT nodeid, parent, bodytype, name, body, nameblob, bodyblob, lastmodified, ord, 0 FROM %s", datatable_name, datatable_backupname);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                                            maepad_warning("Error copying data from %s to %s", datatable_name, datatable_backupname);
                                                            break;
                                                        }

							g_snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

							curDataVersion = datatableversion;
						}
				break;
				}

			if (curDataVersion != datatableversion)
				{
                                        maepad_warning("Data table version mismatch: %d <=> %d", curDataVersion, datatableversion);

					if (resback == TRUE)
						{
							g_snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_name);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
							g_snprintf(tq, sizeof(tq), "ALTER TABLE %s RENAME TO %s", datatable_backupname, datatable_name);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
						}

					break;
				}


			while(curChecklistVersion < checklisttableversion)
				{
					if (curChecklistVersion == 0) /*no checklisttable at all*/
						{
							g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", checklisttable_name, checklisttable);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                                            maepad_warning("Error creating checklist table during schema upgrade");
                                                            break;
                                                        }
							curChecklistVersion = checklisttableversion;
						}
				break;
				}


			GtkTreeStore *filter = GTK_TREE_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview)));

			g_object_ref(filter);
			gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), NULL);

			do
				{
					char tq[512];

					g_snprintf(tq, sizeof(tq), "CREATE%s TABLE %s%s", TEMPTABLE_KEYWORD, datatable_tmpname, datatable);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                            maepad_warning("Error creating temp table: %s", datatable_tmpname);
                                            break;
                                        }
					g_snprintf(tq, sizeof(tq), "CREATE INDEX %s_index ON %s %s", datatable_tmpname, datatable_tmpname, dataindex);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                            maepad_warning("Error creating temp index for %s", datatable_tmpname);
                                            break;
                                        }
					g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_tmpname, datatable_name);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                            maepad_warning("Error copying data from %s to %s", datatable_name, datatable_tmpname);
                                            break;
                                        }

					g_snprintf(tq, sizeof(tq), "CREATE%s TABLE %s%s", TEMPTABLE_KEYWORD, checklisttable_tmpname, checklisttable);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                            maepad_warning("Error creating temp table: %s", checklisttable_tmpname);
                                            break;
                                        }
					g_snprintf(tq, sizeof(tq), "CREATE INDEX %s_index ON %s %s", checklisttable_tmpname, checklisttable_tmpname, checklistindex);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                            maepad_warning("Error creating temp index for %s", checklisttable_tmpname);
                                            break;
                                        }
					g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_tmpname, checklisttable_name);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                                            maepad_warning("Error copying data from %s to %s", checklisttable_name, checklisttable_tmpname);
                                            break;
                                        }
				}
			while(FALSE);

                        read_sqlite3_data(mainview, 0, NULL, selectedCard, mainview->main_view_tree_store);

			gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), GTK_TREE_MODEL(filter));
			g_object_unref(filter);
			gtk_tree_view_expand_all(GTK_TREE_VIEW(mainview->treeview));

			res = TRUE;
		}
	while(FALSE);

	if (stmt) {
		sqlite3_finalize(stmt);
	}


	mainview->loading=FALSE;
	
	return (res);
}

/*
 * write to file 
 */
void write_buffer_to_file(MainView * mainview)
{
        maepad_message("Writing database to file: %s", mainview->file_name);
	saveCurrentData(mainview);

	/*update ord value in database for all nodes*/
	gtk_tree_model_foreach(GTK_TREE_MODEL(mainview->main_view_tree_store),
			(GtkTreeModelForeachFunc)foreach_func_update_ord, mainview);

        busy_enter(mainview);

	char tq[512];

	g_snprintf(tq, sizeof(tq), "DROP TABLE %s", misctable_name);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", misctable_name, misctable);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	gint nndcc = 1;

	if (mainview->newnodedialog_createchild == FALSE)
		nndcc = 0;
	g_snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('newNodeDlgCreateChild', '%d');", misctable_name, nndcc);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	nodeData *node = getSelectedNode(mainview);

	if (node)
		{
			g_snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('selectedNode', '%d');", misctable_name, node->sql3id);
			sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
		}

	guint bsize = sketchwidget_get_brushsize(mainview->sk);
	g_snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('brushSize', '%d');", misctable_name, bsize);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

        if (mainview->current_color == NULL) {
            GdkColor color = {0, 0, 0, 0};
            mainview->current_color = gdk_color_copy(&color);
        }
	unsigned long bcol = ((mainview->current_color->red >> 8) << 16) |
                             ((mainview->current_color->green >> 8) << 8) |
                             ((mainview->current_color->blue) >> 8);

	g_snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('brushColor', '%lu');", misctable_name, bcol);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	g_snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('dataVersion', '%d');", misctable_name, datatableversion);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	g_snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('checklistVersion', '%d');", misctable_name, checklisttableversion);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	g_snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
	g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", datatable_backupname, datatable);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
                        maepad_warning("Error creating backup table: %s", datatable_backupname);
			show_banner(mainview, _("Error creating backup table"));

                        busy_leave(mainview);
			return;
		}
	g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_backupname, datatable_name);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			maepad_warning("Error backing up table %s to %s", datatable_name, datatable_backupname);
			show_banner(mainview, _("Error creating backup table"));

                        busy_leave(mainview);
			return;
		}
	g_snprintf(tq, sizeof(tq), "DELETE FROM %s", datatable_name);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_name, datatable_tmpname);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
                        maepad_warning("Error saving table %s to %s", datatable_tmpname, datatable_name);
			show_banner(mainview, _("Error saving table"));

			g_snprintf(tq, sizeof(tq), "DELETE FROM %s", datatable_name);
			sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

			g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_name, datatable_backupname);
			if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                            maepad_warning("Error restoring backup. Data lost :(");
                        }

                        busy_leave(mainview);
			return;
		}

	g_snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

/*checklist*/
	g_snprintf(tq, sizeof(tq), "DROP TABLE %s", checklisttable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
	g_snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", checklisttable_backupname, checklisttable);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
                        maepad_warning("Error creating backup table: %s", checklisttable_backupname);
			show_banner(mainview, _("Error creating checklist backup table"));

                        busy_leave(mainview);
			return;
		}

	g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_backupname, checklisttable_name);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			maepad_warning("Error backing up table %s to %s", checklisttable_name, checklisttable_backupname);
			show_banner(mainview, _("Error creating checklist backup table"));

                        busy_leave(mainview);
			return;
		}
	g_snprintf(tq, sizeof(tq), "DELETE FROM %s", checklisttable_name);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_name, checklisttable_tmpname);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
                        maepad_warning("Error saving table %s to %s", checklisttable_tmpname, checklisttable_name);
			show_banner(mainview, _("Error saving checklist table"));

			g_snprintf(tq, sizeof(tq), "DELETE FROM %s", checklisttable_name);
			sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

			g_snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_name, checklisttable_backupname);
			if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0) {
                            maepad_warning("Error restoring backup. Data lost :(");
                        }
                        busy_leave(mainview);
			return;
		}

	g_snprintf(tq, sizeof(tq), "DROP TABLE %s", checklisttable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	mainview->file_edited = FALSE;
        busy_leave(mainview);
	show_banner(mainview, _("Changes saved"));
}

void callback_checklist_change(GtkTreeSelection *selection, MainView *mainview)
{
    GtkTreeIter iter;

g_assert(mainview != NULL && mainview->data != NULL);

g_signal_handlers_block_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_block_by_func(mainview->strikethru_tb, callback_fontstyle, mainview->strikethru_tb);
g_signal_handlers_block_by_func(mainview->check_tb, callback_fontstyle, mainview->check_tb);

gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), FALSE);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->strikethru_tb), FALSE);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->check_tb), FALSE);

GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));

    if (gtk_tree_model_get_iter_first(model, &iter)) {
        gtk_widget_show(mainview->listscroll);
        gtk_widget_hide(mainview->checklist_empty_label);
    } else {
        gtk_widget_hide(mainview->listscroll);
        gtk_widget_show(mainview->checklist_empty_label);
    }

GList* l=gtk_tree_selection_get_selected_rows(selection, NULL);

gboolean gotit=FALSE;
 	
GList* cur=l;
while(cur)
	{
	GtkTreePath *path=cur->data;

	if (!gotit)
		{
		GtkTreeIter iter;
		if (gtk_tree_model_get_iter(model, &iter, path))
			{
			gint styletoset_weight;
			gboolean styletoset_strike;
			gboolean ischecked;

	  	gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, CHECKNODE_BOLD, &styletoset_weight, CHECKNODE_STRIKE, &styletoset_strike, CHECKNODE_CHECKED, &ischecked, -1);
	  	if (styletoset_weight==PANGO_WEIGHT_BOLD) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), TRUE);
	  	if (styletoset_strike==TRUE) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->strikethru_tb), TRUE);
	  	if (ischecked==TRUE) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->check_tb), TRUE);
			gotit=TRUE;
			}
		}
	gtk_tree_path_free(path);
	cur=cur->next;
	}

g_list_free(l);

g_signal_handlers_unblock_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_unblock_by_func(mainview->strikethru_tb, callback_fontstyle, mainview->strikethru_tb);
g_signal_handlers_unblock_by_func(mainview->check_tb, callback_fontstyle, mainview->check_tb);
}

void callback_checklist_paste(MainView *mainview)
{
    g_assert(mainview != NULL && mainview->data != NULL);
    gchar **entries;
    gboolean append = FALSE;
    guint i;

    gchar *pasted_text = gtk_clipboard_wait_for_text(mainview->clipboard);
    entries = g_strsplit(pasted_text, "\n", 0);

    GtkTreeSelection *selection;
    GtkTreeModel *model;
    GtkTreeIter current, insert;

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview));
    if (gtk_tree_selection_get_selected(selection, &model, &current)) {
        /* Insert after current (selected) item */
        append = FALSE;
    } else {
        /* Insert at end */
        model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
        append = TRUE;
    }

    for (i=0; i<g_strv_length(entries); i++) {
        if (append) {
            gtk_list_store_append(GTK_LIST_STORE(model),
                    &insert);
        } else {
            if (i == 0 && mainview->checklist_prepend) {
                gtk_list_store_insert_before(GTK_LIST_STORE(model),
                        &insert,
                        &current);
            } else {
                gtk_list_store_insert_after(GTK_LIST_STORE(model),
                        &insert,
                        &current);
            }
            current = insert;
        }

        gtk_list_store_set(GTK_LIST_STORE(model),
                &insert,
                CHECKNODE_CHECKED, FALSE,
                CHECKNODE_TEXT, entries[i],
                -1);
    }

    mainview->checklist_edited = TRUE;

    g_strfreev(entries);
    g_free(pasted_text);
}

void callback_checklist_add(GtkAction *action, MainView *mainview)
{
    g_assert(mainview != NULL && mainview->data != NULL);

    if (action != NULL) {
        /* If coming from the tool button, always append */
        mainview->checklist_prepend = FALSE;
    }

    gchar* text = show_line_edit_dialog(mainview, _("Add new checklist item"), _("Name:"), _("Add"), "");

    if (text != NULL) {
        GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview));
        GtkTreeModel *model;
        GtkTreeIter current, insert;

        if (gtk_tree_selection_get_selected(selection, &model, &current)) {
            /* Something is selected */
            if (mainview->checklist_prepend) {
                /* User wants to insert before current item */
                gtk_list_store_insert_before(GTK_LIST_STORE(model),
                        &insert,
                        &current);
            } else {
                /* User wants to insert after current item */
                gtk_list_store_insert_after(GTK_LIST_STORE(model),
                        &insert,
                        &current);
            }
        } else {
            /* Nothing is selected - just append at the end */
            model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
            gtk_list_store_append(GTK_LIST_STORE(model), &insert);
        }

        gtk_list_store_set(GTK_LIST_STORE(model),
                           &insert,
                           CHECKNODE_CHECKED, FALSE,
                           CHECKNODE_TEXT, text,
                           -1);

        GtkTreePath *path = gtk_tree_model_get_path(model, &insert);
        if (path) {
            gtk_tree_view_set_cursor(GTK_TREE_VIEW(mainview->listview), path, mainview->checklist_column_text, FALSE);
            gtk_tree_path_free(path);
        }

        mainview->checklist_edited = TRUE;
    }
}

void
checklist_edit_selected(MainView* mainview, LineEditMode mode)
{
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview));

    if (gtk_tree_selection_count_selected_rows(selection) == 0) {
	show_banner(mainview, _("Select items first"));
	return;
    }

    GtkTreeModel* model;
    GtkTreeIter iter;

    if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
        gchar* old_text = NULL;
        gtk_tree_model_get(model, &iter, CHECKNODE_TEXT, &old_text, -1);

        gchar* new_text = show_line_edit_dialog_full(mainview, _("Edit checklist item"), _("New name:"), _("Save"), old_text, mode);

        if (new_text != NULL) {
            gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_TEXT, new_text, -1);
            mainview->checklist_edited = TRUE;
            g_free(new_text);
        }

        g_free(old_text);
    }
}

void callback_checklist_edit(GtkAction *action, MainView *mainview)
{
    g_assert(mainview != NULL && mainview->data != NULL);
    checklist_edit_selected(mainview, LINE_EDIT_MODE_DEFAULT);
}

void callback_checklist_delete(GtkAction *action, MainView *mainview)
{
    g_assert(mainview != NULL && mainview->data != NULL);

    if (gtk_tree_selection_count_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview)))==0) {
	show_banner(mainview, _("Select items first"));
	return;
    }

    if (show_confirmation(mainview, _("Delete selected checklist item?"))) {
        callback_checklist_delete_real(mainview);
    }
}

void callback_checklist_delete_real(MainView* mainview)
{
GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
GList* l=gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview)), NULL);

    /* Select the item after the selection */
    checklist_select(mainview, TREEVIEW_SELECT_NEXT);

GList* rowrefs=NULL;
GList* cur=l;
while(cur)
	{
	GtkTreePath *path=cur->data;

	GtkTreeIter iter;
	if (gtk_tree_model_get_iter(model, &iter, path))
		{
  	GtkTreeRowReference  *rowref = gtk_tree_row_reference_new(model, path);
		rowrefs=g_list_append(rowrefs, rowref);
		}
	gtk_tree_path_free(path);
	cur=cur->next;
	}
        g_list_free(l);
        checklist_remove_rowrefs(mainview, rowrefs, FALSE);
}

void
checklist_remove_rowrefs(MainView* mainview, GList* rowrefs, gboolean show_result)
{
    GtkTreeModel* model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
    GList* cur = rowrefs;
    gint count = 0;

    busy_enter(mainview);

    while (cur) {
        GtkTreeRowReference* rowref = cur->data;
        GtkTreePath* path = gtk_tree_row_reference_get_path(rowref);
        if (path != NULL) {
            GtkTreeIter iter;
            if (gtk_tree_model_get_iter(model, &iter, path)) {
                gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
                count++;
            }
            gtk_tree_path_free(path);
        }
        gtk_tree_row_reference_free(rowref);
        cur = cur->next;
    }
    g_list_free(rowrefs);

    busy_leave(mainview);

    if (count && show_result) {
        gchar* message = g_strdup_printf(N_("%d item removed", "%d items removed", count), count);
        show_banner(mainview, message);
        g_free(message);
    }

    mainview->checklist_edited = TRUE;
}

/* Private callback for show_confirmation() - handle keyboard input */
gboolean
on_confirmation_key_press_event(GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
    GtkDialog* dialog = GTK_DIALOG(widget);

    switch (event->keyval) {
        /* Positive response - Enter, Return or y */
        case GDK_KP_Enter:
        case GDK_Return:
        case GDK_y:
        case GDK_Y:
            gtk_dialog_response(dialog, GTK_RESPONSE_OK);
            return TRUE;

        /* Negative response - Backspace, Escape or n */
        case GDK_BackSpace:
        case GDK_Escape:
        case GDK_n:
        case GDK_N:
            gtk_dialog_response(dialog, GTK_RESPONSE_CANCEL);
            return TRUE;

        /* Don't handle the rest of the keys */
        default:
            return FALSE;
    }
}


/* Ask the user for confirmation of a specific action */
gboolean
show_confirmation(MainView* mainview, gchar* question)
{
    GtkDialog* dialog = GTK_DIALOG(hildon_note_new_confirmation(
                GTK_WINDOW(mainview->data->main_view), question));
    gtk_window_set_transient_for(GTK_WINDOW(dialog), mainview_get_dialog_parent(mainview));
    g_signal_connect(G_OBJECT(dialog), "key-press-event",
            G_CALLBACK(on_confirmation_key_press_event), NULL);

    gint response = gtk_dialog_run(dialog);
    gtk_widget_destroy(GTK_WIDGET(dialog));

    return (response == GTK_RESPONSE_OK);
}

/* Show a information banner to the user (non-modal) */
void
show_banner(MainView* mainview, const gchar* text)
{
    hildon_banner_show_information(GTK_WIDGET(mainview_get_dialog_parent(mainview)), NULL, text);
}

typedef struct {
    GtkWidget* dialog;
    gboolean can_close_window;
} LineEditDialogData;

/* Helper function to close line edit dialog on Backspace */
gboolean
line_edit_dialog_key_press(GtkWidget* w, GdkEventKey* event, gpointer user_data)
{
    LineEditDialogData* dialogdata = (LineEditDialogData*)user_data;
    GtkEntry* entry = GTK_ENTRY(w);

    if (event->keyval == GDK_BackSpace &&
            dialogdata->can_close_window &&
            strcmp(gtk_entry_get_text(entry), "") == 0) {
        gtk_dialog_response(GTK_DIALOG(dialogdata->dialog), GTK_RESPONSE_CANCEL);
        return TRUE;
    }

    return FALSE;
}

/* Let the user enter or edit a line of text */
gchar*
show_line_edit_dialog_full(MainView* mainview, const gchar* title,
        const gchar* label_text, const gchar* action, const gchar* text,
        LineEditMode mode)
{
    GtkWidget* edit_dialog;
    GtkWidget* label;
    GtkWidget* entry;
    GtkWidget* hbox;
    gchar* result = NULL;
    LineEditDialogData dialogdata;

    edit_dialog = GTK_WIDGET(gtk_dialog_new());
    gtk_window_set_title(GTK_WINDOW(edit_dialog), title);
    gtk_window_set_transient_for(GTK_WINDOW(edit_dialog), mainview_get_dialog_parent(mainview));

    label = GTK_WIDGET(gtk_label_new(label_text));

    entry = hildon_entry_new(HILDON_SIZE_AUTO);
    gtk_entry_set_text(GTK_ENTRY(entry), text);
    gtk_entry_set_activates_default(GTK_ENTRY(entry), TRUE);
    switch (mode) {
        case LINE_EDIT_MODE_DEFAULT:
            gtk_editable_select_region(GTK_EDITABLE(entry), 0, -1);
            break;
        case LINE_EDIT_MODE_APPEND:
            gtk_editable_select_region(GTK_EDITABLE(entry), -1, -1);
            break;
        case LINE_EDIT_MODE_PREPEND:
            gtk_editable_select_region(GTK_EDITABLE(entry), 0, 0);
            break;
        default:
            break;
    }

    gtk_dialog_add_button(GTK_DIALOG(edit_dialog), action, GTK_RESPONSE_OK);
    gtk_dialog_set_default_response(GTK_DIALOG(edit_dialog), GTK_RESPONSE_OK);

    dialogdata.dialog = edit_dialog;
    dialogdata.can_close_window = (strcmp(text, "") == 0);
    g_signal_connect(G_OBJECT(entry), "key-press-event", G_CALLBACK(line_edit_dialog_key_press), &dialogdata);

    hbox = GTK_WIDGET(gtk_hbox_new(FALSE, 10));

    gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(edit_dialog))), GTK_WIDGET(hbox));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);

    gtk_widget_show_all(GTK_WIDGET(hbox));

    while (TRUE) {
        if (gtk_dialog_run(GTK_DIALOG(edit_dialog)) == GTK_RESPONSE_OK) {
            result = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));
            if (strcmp(result, "") != 0) {
                break;
            } else {
                show_banner(mainview, _("Please enter a non-empty text"));
                g_free(result);
                result = NULL;
            }
        } else {
            result = NULL;
            break;
        }
    }

    gtk_widget_destroy(GTK_WIDGET(edit_dialog));
    return result;
}

void
treeview_scroll_to_selection(MainView* mainview, TreeViewType type)
{
    GtkTreeView* treeview = NULL;
    HildonPannableArea* pannable_area = NULL;
    GtkTreeModel* model = NULL;
    GtkTreeSelection* selection = NULL;
    GtkTreePath* path = NULL;
    GtkTreeIter iter;
    GdkRectangle rect;
    gint y;

    switch (type) {
        case TREEVIEW_MAINVIEW:
            treeview = GTK_TREE_VIEW(mainview->treeview);
            pannable_area = HILDON_PANNABLE_AREA(mainview->scrolledtree);
            break;
        case TREEVIEW_CHECKLIST:
            treeview = GTK_TREE_VIEW(mainview->listview);
            pannable_area = HILDON_PANNABLE_AREA(mainview->listscroll);
            break;
        default:
            g_assert_not_reached();
            break;
    }

    /* Let the view scroll so that the selected item is visible */
    selection = gtk_tree_view_get_selection(treeview);
    if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
        path = gtk_tree_model_get_path(model, &iter);

        gtk_tree_view_get_background_area(treeview, path, NULL, &rect);
        gtk_tree_view_convert_bin_window_to_tree_coords(treeview,
                0, rect.y, NULL, &y);
        hildon_pannable_area_scroll_to(pannable_area, -1, y);

        gtk_tree_path_free(path);
    }
}

gboolean
checklist_scroll_to_selection(gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    treeview_scroll_to_selection(mainview, TREEVIEW_CHECKLIST);
    return FALSE;
}

gboolean
nodelist_scroll_to_selection(gpointer user_data)
{
    MainView* mainview = (MainView*)user_data;
    treeview_scroll_to_selection(mainview, TREEVIEW_MAINVIEW);
    return FALSE;
}

void
checklist_select(MainView* mainview, TreeviewSelectType type)
{
    GtkTreeModel* model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview));
    GtkTreeIter iter;
    GtkTreeIter last;
    GtkTreePath* path;

    switch (type) {
        case TREEVIEW_SELECT_FIRST:
            if (gtk_tree_model_get_iter_first(model, &iter)) {
                path = gtk_tree_model_get_path(model, &iter);
                gtk_tree_selection_select_path(selection, path);
                gtk_tree_path_free(path);
            }
            break;
        case TREEVIEW_SELECT_LAST:
            if (gtk_tree_model_get_iter_first(model, &iter)) {
                do {
                    last = iter;
                } while (gtk_tree_model_iter_next(model, &iter));
                path = gtk_tree_model_get_path(model, &last);
                gtk_tree_selection_select_path(selection, path);
                gtk_tree_path_free(path);
            }
            break;
        case TREEVIEW_SELECT_PREVIOUS:
            if (gtk_tree_selection_get_selected(selection, NULL, &iter)) {
                path = gtk_tree_model_get_path(model, &iter);
                if (gtk_tree_path_prev(path)) {
                    gtk_tree_selection_select_path(selection, path);
                }
                gtk_tree_path_free(path);
            }
            break;
        case TREEVIEW_SELECT_NEXT:
            if (gtk_tree_selection_get_selected(selection, NULL, &iter)) {
                path = gtk_tree_model_get_path(model, &iter);
                gtk_tree_path_next(path);
                gtk_tree_selection_select_path(selection, path);
                gtk_tree_path_free(path);
	    }
            break;
        default:
            g_assert_not_reached();
            break;
    }

    checklist_scroll_to_selection(mainview);

    /**
     * This is a workaround: The HildonPannableArea does not seem to
     * scroll to the right position (especially when going to the first
     * or last item in the list), so we simply scroll again after 500ms.
     **/
    g_timeout_add(500, checklist_scroll_to_selection, mainview);
}

void
nodelist_select(MainView* mainview, TreeviewSelectType type)
{
    GtkTreeView* treeview = GTK_TREE_VIEW(mainview->treeview);
    GtkTreeModel* model = gtk_tree_view_get_model(treeview);
    GtkTreeSelection* selection = gtk_tree_view_get_selection(treeview);
    GtkTreeIter iter;
    GtkTreeIter last;
    GtkTreeIter child;
    GtkTreePath* path;

    switch (type) {
        case TREEVIEW_SELECT_FIRST:
            if (gtk_tree_model_get_iter_first(model, &iter)) {
                path = gtk_tree_model_get_path(model, &iter);
                gtk_tree_selection_select_path(selection, path);
                gtk_tree_path_free(path);
            }
            break;
        case TREEVIEW_SELECT_LAST:
            if (gtk_tree_model_get_iter_first(model, &iter)) {
                do {
                    last = iter;
                    if (gtk_tree_model_iter_children(model, &child, &last)) {
                        do {
                            last = child;
                        } while (gtk_tree_model_iter_next(model, &child));
                    }
                } while (gtk_tree_model_iter_next(model, &iter));
                path = gtk_tree_model_get_path(model, &last);
                gtk_tree_selection_select_path(selection, path);
                gtk_tree_path_free(path);
            }
            break;
        case TREEVIEW_SELECT_PREVIOUS:
            if (gtk_tree_selection_get_selected(selection, NULL, &iter)) {
                path = gtk_tree_model_get_path(model, &iter);
                if (gtk_tree_path_prev(path)) {
                    gtk_tree_selection_select_path(selection, path);
                } else if (gtk_tree_path_up(path)) {
                    gtk_tree_selection_select_path(selection, path);
                }
                gtk_tree_path_free(path);
            } else {
		nodelist_select(mainview, TREEVIEW_SELECT_FIRST);
	    }
            break;
        case TREEVIEW_SELECT_NEXT:
            if (gtk_tree_selection_get_selected(selection, NULL, &iter)) {
                path = gtk_tree_model_get_path(model, &iter);
                gtk_tree_path_down(path);
                while (!gtk_tree_model_get_iter(model, &iter, path)) {
                    /* Child item does not exist - bubble up to next item */
                    gtk_tree_path_up(path);
                    gtk_tree_path_next(path);

                    if (gtk_tree_path_get_depth(path) == 0) {
                        /* avoid infinite loops */
                        break;
                    }
                }

                if (gtk_tree_model_get_iter(model, &iter, path)) {
                    gtk_tree_selection_select_path(selection, path);
                }

                gtk_tree_path_free(path);
            } else {
		nodelist_select(mainview, TREEVIEW_SELECT_FIRST);
            }
            break;
        default:
            g_assert_not_reached();
            break;
    }

    nodelist_scroll_to_selection(mainview);

    /**
     * This is a workaround: The HildonPannableArea does not seem to
     * scroll to the right position (especially when going to the first
     * or last item in the list), so we simply scroll again after 500ms.
     **/
    g_timeout_add(500, nodelist_scroll_to_selection, mainview);
}


const gchar *
format_overview_name(nodeData *nd, const gchar *name)
{
    static gchar *tmp;
    gchar *font_desc, *font_color;

    if (tmp != NULL) {
        g_free(tmp);
    }

    if (name == NULL) {
        name = nd->name;
    }

    font_desc = he_get_logical_font_desc("SmallSystemFont");
    font_color = he_get_logical_font_color("SecondaryTextColor");

    tmp = g_markup_printf_escaped("%s\n<span font_desc=\"%s\" foreground=\"%s\">%s</span>",
            name,
            font_desc,
            font_color,
            (nd->typ==NODE_CHECKLIST)?(_("Checklist")):
             ((nd->typ==NODE_TEXT)?(_("Rich text")):
              ((nd->typ==NODE_SKETCH)?(_("Sketch")):(""))));

    g_free(font_desc);
    g_free(font_color);

    return tmp;
}


gchar *
he_get_logical_font_desc(const gchar *name)
{
    GtkSettings *settings = gtk_settings_get_default();
    GtkStyle *style = gtk_rc_get_style_by_paths(settings,
            name, NULL, G_TYPE_NONE);

    return pango_font_description_to_string(style->font_desc);
}

gchar *
he_get_logical_font_color(const gchar *name)
{
    GdkColor color;
    GtkSettings *settings = gtk_settings_get_default();
    GtkStyle *style = gtk_rc_get_style_by_paths(settings,
            "GtkButton", "osso-logical-colors", GTK_TYPE_BUTTON);

    if (gtk_style_lookup_color(style, name, &color)) {
        return gdk_color_to_string(&color);
    } else {
        return NULL;
    }
}


