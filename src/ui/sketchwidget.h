
/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef SKETCHWIDGET_H
#define SKETCHWIDGET_H

#include <math.h>
#include <gtk/gtk.h>

/*#define NOTIFY_STUPID */
/*#define NOTIFY_COORDS */
/*#define PRESSURECOLORS_DEBUG */

#define PRESSURE_MIN 0.1
#define PRESSURE_MAX 0.35

#define PRESSURE_COLOR_MAX 0.25
#define PRESSURE_COLORS 5

#define BACKGRAPH_LINE_HEIGHT 32
#define BACKGRAPH_LINE_MARGIN 40
#define BACKGRAPH_LINE_SIZE 1
#define BACKGRAPH_LINE_MARGIN_SIZE 2

#define BACKGRAPH_GRAPH_WIDTH 32
#define BACKGRAPH_GRAPH_SIZE 1

typedef enum
	{
		SKETCHBACK_UNSET = -1,
		SKETCHBACK_NONE,
		SKETCHBACK_LINES,
		SKETCHBACK_GRAPH,
		SKETCHBACK_COUNT
	} sketchBack;

#define TILE_SIZE 32

/*
 * 800/TILE_SIZE
 */
#define TILES_X 25
#define TILES_ROWSTRIDE TILES_X

/*
 * 480/TILE_SIZE
 */
#define TILES_Y 15

#define UNDO_LEVELS 10

typedef enum
	{
		SKETCHSHAPE_FREEHAND = 0,
		SKETCHSHAPE_LINE,
		SKETCHSHAPE_RECT,
		SKETCHSHAPE_ELLIPSE,
		SKETCHSHAPE_COUNT
	} sketchShape;

typedef struct _SketchWidgetTile SketchWidgetTile;
struct _SketchWidgetTile
	{
		guint tileid;
		GdkPixmap *tiledata;
		GdkPixmap *tiledata_new;
	};

typedef struct _SketchWidgetUndo SketchWidgetUndo;
struct _SketchWidgetUndo
	{

		/*
		 * keep a linked list for modified tiles with their data
		 */
		GSList *tileList;						/*list of SketchWidgetTile */
		char saved_tiles[TILES_X * TILES_Y]; /*fast dupe check*/
	};

typedef struct _SketchWidget SketchWidget;
struct _SketchWidget
	{
		GtkWidget *scrolledwindow;
		GtkWidget *drawingarea;
		GdkPixmap *pixmap;
		GdkPixmap *backpixmap;

		GdkPixmap *backpixmaps[SKETCHBACK_COUNT];
		int backpixmapheights[SKETCHBACK_COUNT];

		sketchBack backstyle;
		sketchBack backstyle_currentimage;
		
		sketchShape shape;
		gboolean fillmode;
		gboolean shiftmode;

		guint brush_size;
		guint brush_radius;
		GdkColor brush_color;
		gboolean border;

		GdkColor brush_colors[PRESSURE_COLORS];
		gdouble brush_colors_pressure_index[PRESSURE_COLORS];
		
		gboolean pressuresensitivity;

		GdkGC *gc;
		GdkGC *backgc;
		unsigned long lastevent_x, lastevent_y, start_x, start_y, cur_x, cur_y;
		gboolean pressed;
		gboolean coords_valid; /*if start_x/start_y valid*/

		gboolean is_edited;

		GList *undolist;						/*list of SketchWidgetUndo, kept reverse */
		guint undocurrent;					/*current index on the undolist (=available redo steps) */
		/*undocurrent = 0, no redo available*/

		/*
		 * current undo buffer, active when mouse pointer is held down
		 * when mouse pointer is released the new states of tiles will be saved again,
		 * then this will be prepended to *undolist and set to NULL
		 */
		SketchWidgetUndo *undocurbuffer;

		void (*callback_undotoggle) (SketchWidget * sk, gboolean st, gpointer data);
		void (*callback_redotoggle) (SketchWidget * sk, gboolean st, gpointer data);
		gpointer callback_undotoggle_data;
		gpointer callback_redotoggle_data;

		void (*callback_finger) (SketchWidget * sk, gint x, gint y, gdouble pressure, gpointer data);
		gpointer callback_finger_data;
		
		guint sizex, sizey;
		guint stupidflag; /*1:ignore next motion_event 2:ignore next keypress 0:normal*/
	};

SketchWidget *sketchwidget_new(gint sizex, gint sizey, gboolean border);
void sketchwidget_destroy(SketchWidget * sk);
gboolean sketch_configure(GtkWidget * widget, GdkEventConfigure * event, SketchWidget * sk);

void sketchwidget_set_fingercallback(SketchWidget * sk, void *func, gpointer data);
void sketchwidget_set_undocallback(SketchWidget * sk, void *func, gpointer data);
void sketchwidget_set_redocallback(SketchWidget * sk, void *func, gpointer data);
GtkWidget *sketchwidget_get_drawingarea(SketchWidget * sk);
GtkWidget *sketchwidget_get_mainwidget(SketchWidget * sk);
gboolean sketchwidget_get_edited(SketchWidget * sk);
void sketchwidget_set_edited(SketchWidget * sk, gboolean status);
void sketchwidget_clear_real(SketchWidget * sk);
void sketchwidget_clear(SketchWidget * sk);
void sketchwidget_set_fillmode(SketchWidget * sk, gboolean filled);
void sketchwidget_set_shift(SketchWidget * sk, gboolean shift);
gboolean sketchwidget_get_shift(SketchWidget * sk);
void sketchwidget_set_shape(SketchWidget * sk, sketchShape sp);
void sketchwidget_set_brushsize(SketchWidget * sk, guint bsize);
guint sketchwidget_get_brushsize(SketchWidget * sk);
void sketchwidget_set_brushcolor(SketchWidget * sk, GdkColor col);
GdkColor sketchwidget_get_brushcolor(SketchWidget * sk);
void sketchwidget_set_backstyle(SketchWidget * sk, sketchBack style);

GdkPixbuf *sketchwidget_trim_image(GdkPixbuf * pixbuf, guint totalw, guint totalh, double *w, double *h, gboolean doTopLeft);

gboolean sketchwidget_cut(SketchWidget * sk, GtkClipboard * clippy);
gboolean sketchwidget_copy(SketchWidget * sk, GtkClipboard * clippy);
gboolean sketchwidget_paste(SketchWidget * sk, GtkClipboard * clippy);

/*
 * unref the result afterwards
 */
GdkPixmap *sketchwidget_get_Pixmap(SketchWidget * sk);

/*
 * undo/redo
 */
gboolean sketchwidget_undo(SketchWidget * sk);
gboolean sketchwidget_redo(SketchWidget * sk);
void sketchwidget_wipe_undo(SketchWidget * sk);

/* Widget-specific logging */

#define SKETCH_WIDGET_LOG_DOMAIN "SketchWidget"

#define sketchwidget_debug(...)   g_log(SKETCH_WIDGET_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, __VA_ARGS__)
#define sketchwidget_message(...) g_log(SKETCH_WIDGET_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, __VA_ARGS__)
#define sketchwidget_warning(...) g_log(SKETCH_WIDGET_LOG_DOMAIN, G_LOG_LEVEL_WARNING, __VA_ARGS__)

#endif
