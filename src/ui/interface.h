/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <gtk/gtk.h>
#include <appdata.h>
#include <sqlite3.h>
#include <ui/sketchwidget.h>
#include <config.h>

#include "../he/he-fullscreen-button.h"

/**
 * If you want to have finger-friendly support (i.e. big fonts in
 * checklists, big scrollbars in checklists, texts and sketches),
 * uncomment the following #define line.
 **/
/* #define MAEMOPADPLUS_FINGER_FRIENDLY */
#define MAEMOPADPLUS_FINGER_FONT "sans 20"

#include <hildon/hildon-program.h> 
#include <hildon/hildon-color-button.h>
#include <wptextview.h>
#include <wptextbuffer.h>

/* How long one has to press a checklist item before editing starts */
#define CHECKLIST_LONGPRESS_EDIT_DELAY 1000

#define _(String) gettext(String)
#define N_(a, b, c) ngettext(a, b, c)

#define USE_TEMP_TABLES

#ifdef USE_TEMP_TABLES
	#define TEMPTABLE_KEYWORD " TEMP"
#else
	#define TEMPTABLE_KEYWORD ""
#endif

typedef enum {
    RESPONSE_CREATE_NEW = 1,
    RESPONSE_OPEN_FILE,
    RESPONSE_QUIT,
} SetupResponse;

/*
 * confirmation responses 
 */
#define CONFRESP_YES 1
#define CONFRESP_NO 2
#define CONFRESP_OK 3
#define CONFRESP_CANCEL 4
#define CONFRESP_ERROR 5

#define BRUSHSIZE_COUNT 7

#define SKETCHNODE_X 288
#define SKETCHNODE_Y 96
#define SKETCHNODE_RX 144
#define SKETCHNODE_RY 48

typedef enum
	{
		NODE_UNKNOWN = 0,
		NODE_TEXT,
		NODE_SKETCH,
		NODE_CHECKLIST
	} nodeType;

typedef enum
	{
		NODEFLAG_NONE = 0,
		NODEFLAG_SKETCHLINES = 1 << 0,
		NODEFLAG_SKETCHGRAPH = 1 << 1,
		NODEFLAG_WORDWRAP = 1 << 2
			/*
			 * NODEFLAG_OTHER = 1 << 3
			 * NODEFLAG_OTHER2 = 1 << 4
			 */
	} nodeFlag;

enum
	{
		NODE_NAME,
		NODE_PIXBUF,
		NODE_DATA,
		N_COLUMNS
	};

enum
	{
		CHECKNODE_CHECKED,
		CHECKNODE_TEXT,
		CHECKNODE_COLOR,
		CHECKNODE_BOLD,
		CHECKNODE_STRIKE,
                CHECKNODE_ICON_NAME,
		CHECKN_COLUMNS
	};

typedef enum
	{
		CHECKSTYLE_CHECKED = 1 << 0,
		CHECKSTYLE_BOLD = 1 << 1,
		CHECKSTYLE_STRIKE = 1 << 2
	} checklistStyle;

typedef struct _nodeData nodeData;
struct _nodeData
	{
		unsigned int sql3id;

		nodeType typ;
		gchar *name;
		GdkPixbuf *namepix;

		unsigned long lastMod;
		unsigned int flags;
	};

/*
 * Struct to include view's information 
 */
typedef struct _MainView MainView;
struct _MainView
	{
		/*
		 * Handle to app's data 
		 */
		AppData *data;

		/*
		 * Items for menu 
		 */
		GtkWidget *tools_item;
		GtkWidget *delete_node_item;
		GtkWidget *rename_node_item;
		GtkWidget *export_node_item;
		GtkWidget *move_node_item;
		GtkWidget *move_up_node_item;
		GtkWidget *move_down_node_item;
		GtkWidget *tools_brushsize;
		GtkWidget *tools_pagestyle;
		GtkWidget *tools_shape;
		GtkWidget *tools_font;
		GtkWidget *tools_color;
		GtkWidget *tools_pressure;
		
		GtkWidget *brushsizemenu;
		GtkWidget *sketchlinesmenu;
		GtkWidget *shapemenu;

		/*
		 * Toolbar 
		 */
		GtkWidget *toolbar;
		GtkWidget *iconw;

		/*
		 * GtkToolItem* cut_tb;
		 * GtkToolItem* copy_tb;
		 * GtkToolItem* paste_tb;
		 * GtkToolItem* separator_tb2;
		 */
		GtkToolItem *bold_tb, *italic_tb, *underline_tb, *bullet_tb, *strikethru_tb, *check_tb, *checkadd_tb, *checkedit_tb, *checkdel_tb;

		GtkToolItem *font_tb;
		GtkToolItem *colorbutton_tb;

                GdkColor* current_color;

		GtkToolItem *brushsize_tb;
		GtkWidget *brushsizemenuitems[BRUSHSIZE_COUNT];

		GtkToolItem *eraser_tb;

		GtkToolItem *sketchlines_tb;
		GtkWidget *sketchlinesmenuitems[3];

		GtkToolItem *undo_tb;
		GtkToolItem *redo_tb;

		GtkToolItem *shape_tb;
		GtkWidget *shapemenuitems[4];

                GtkToolItem* sharing_tb;

		GtkToolItem* fullscreen_tb;

		/*
		 * Textview related 
		 */
		GtkWidget *scrolledwindow;	/* textview is under this widget */
		GtkWidget *textview;				/* widget that shows the text */
		WPTextBuffer *buffer;			/* buffer that contains the text */
		GtkClipboard *clipboard;		/* clipboard for copy/paste */

                GtkWidget *checklist_content; /* all checklist contents */
                GtkWidget *checklist_empty_label; /* The "No items" label for empty checklists */
		GtkWidget *listscroll;	/* checklist is under this widget */
		GtkWidget *listview; /*checklist */

		GtkTreeViewColumn* checklist_column_check;
		GtkTreeViewColumn* checklist_column_text;
                guint checklist_longpress_source_id; /* longpress timeout id */

                gboolean checklist_edited; /* set to TRUE when the checklist is "dirty" and needs to be saved */
                gboolean checklist_prepend; /* set to TRUE when paste/insert should prepend instead of append  */

		GtkWidget *treeview;
		GtkWidget *scrolledtree;		/* scrolledwindow for the tree */
                GtkWidget* action_area_button_add; /* "Add new node" button in action area */

                /* Node list item on which a longpress has been carried out */
                GtkTreePath* node_list_longpress_path;

		SketchWidget *sk;

                /* AppMenu stuff */
                GtkWidget* main_view_app_menu;
                GtkWidget* menu_button_new;
                GtkWidget* menu_button_open;
                GtkWidget* menu_button_save;
                GtkWidget* menu_button_about;

                GtkWidget* node_view_app_menu;
                GtkWidget* menu_button_rename;
                GtkWidget* menu_button_delete;
                GtkWidget* menu_button_export;
                GtkWidget* menu_button_clear;

                /* Sketch-specific */
                GtkWidget* menu_button_square;
                GtkWidget* menu_button_filled;

                /* Rich text-specific */
                GtkWidget* menu_button_wordwrap;

                /* Checklist-specific */
                GtkWidget* menu_button_remove_checked;

                /* End AppMenu stuff */

                gboolean can_show_node_view;

                HeFullscreenButton* node_view_fullscreen_button;

		/* The underlying "real" tree store for the node list */
		GtkTreeStore *main_view_tree_store;

		/* The visible (possibly filtered) tree for the node list */
		GtkTreeModelFilter *main_view_tree_filter;

		/* The widget for search-as-you-type in the main view */
		HildonLiveSearch *main_view_live_search;

		guint busyrefcount;

                /* The previous keyval in the node view window */
                guint node_view_prev_keyval;
                /* If TRUE, node_view_prev_keyval will be reset */
                gboolean node_view_prev_keyval_reset;

                /* Same as above, but for the main view */
                guint main_view_prev_keyval;
                gboolean main_view_prev_keyval_reset;

		gboolean file_edited;				/* for node operations and notification of new stuff in tmptable */
		gchar *file_name;
		gboolean loading;

		sqlite3 *db;

		nodeData *cansel_node;
		guint32 cansel_time;

		gboolean newnodedialog_createchild;
	};

#define datatable_name "nodes"
#define datatable_tmpname "tmpnodes"
#define datatable_backupname "nodesbackup"

#define datatable "( \
	nodeid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
	parent INTEGER NOT NULL DEFAULT 0, \
	bodytype INTEGER NOT NULL DEFAULT 0, \
	name TEXT, \
	body TEXT, \
	nameblob BLOB, \
	bodyblob BLOB, \
	lastmodified INTEGER NOT NULL DEFAULT 0, \
	ord INTEGER NOT NULL DEFAULT 0, \
	flags INTEGER NOT NULL DEFAULT 0);"

#define dataindex "(parent, ord);"

#define misctable_name "settings"
#define misctable "(skey TEXT UNIQUE, sval TEXT);"

#define datatableversion 1

#define checklisttable_name "checklists"
#define checklisttable_tmpname "tmpchecklists"
#define checklisttable_backupname "checklistsbackup"

#define checklisttable "( \
	idx INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
	nodeid INTEGER NOT NULL DEFAULT 0, \
	name TEXT, \
	style INTEGER NOT NULL DEFAULT 0, \
	color INTEGER NOT NULL DEFAULT 0, \
	ord INTEGER NOT NULL DEFAULT 0);"
#define checklistindex "(nodeid, ord);"

#define checklisttableversion 1

/*
 * Publics: 
 */
void sk_set_brushsize(MainView * main, guint bsize);
void _toggle_tool_button_set_inconsistent(GtkToggleToolButton *button, gboolean inconsistent);
MainView *interface_main_view_new(AppData * data);
void interface_main_view_destroy(MainView * main);
gchar *interface_file_chooser(MainView * mainview, GtkFileChooserAction action, gchar * suggname, gchar * suggext);

/* Get the window that should be used as parent for dialogs */
GtkWindow*
mainview_get_dialog_parent(MainView* mainview);

SetupResponse interface_initial_setup(MainView* main);

#endif
