/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <ui/interface.h>
#include <gtk/gtk.h>
#include <appdata.h>


/* Helper macro for toggling a GtkToggleToolButton */
#define maepad_toggle_gtk_toggle_tool_button(tool_button) \
  gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(tool_button), \
 !gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(tool_button)))

void prepareUIforNodeChange(MainView * mainview, nodeType typ);

nodeData *getSelectedNode(MainView * mainview);
void saveCurrentData(MainView * mainview);
void saveDataToNode(MainView * mainview, nodeData *selnode);

gboolean callback_node_view_window_state(GtkWidget* window, GdkEventWindowState* event, gpointer user_data);

gboolean callback_treeview_button_press(GtkTreeView* treeview, GdkEventButton* event, gpointer user_data);
void callback_treeview_celldatafunc(GtkTreeViewColumn * tree_column, GtkCellRenderer * cell, GtkTreeModel * tree_model, GtkTreeIter * iter, gpointer data);
void callback_treeview_change(GtkTreeSelection * selection, gpointer data);
gboolean treeview_canselect(GtkTreeSelection * selection, GtkTreeModel * model, GtkTreePath * path, gboolean path_currently_selected, gpointer userdata);

gboolean newnodedlg_key_press_cb(GtkWidget * widget, GdkEventKey * event, GtkWidget * dlg);
void add_new_node(nodeData * node, MainView * mainview, gboolean ischild);

void callback_file_new_text_node(GtkAction * action, gpointer data);
void callback_new_node_real(GtkAction * action, gpointer data);

void callback_file_new_node(GtkAction * action, gpointer data);

void callback_file_export_node(GtkAction * action, gpointer data);

void callback_file_delete_node(GtkAction * action, gpointer data);
void callback_delete_node_real(MainView* mainview);

void callback_file_rename_node (GtkAction *action, gpointer data);
void callback_rename_node_real(MainView* mainview, gchar* new_name);

void callback_about(GtkAction * action, gpointer data);

gboolean callback_live_search_refilter(HildonLiveSearch *, gpointer);

/*
 *  move node
 */
void callback_move_up_node(GtkAction * action, gpointer data);
void callback_move_down_node(GtkAction * action, gpointer data);

/*
 * edit-> cut/copy/paste 
 */
void callback_edit_clear(GtkAction * action, gpointer data);
void callback_edit_cut(GtkAction * action, gpointer data);
void callback_edit_copy(GtkAction * action, gpointer data);
void callback_edit_paste(GtkAction * action, gpointer data);
gint cb_popup(GtkWidget * widget, GdkEvent * event);
void on_node_menu_show(GtkWidget* nodemenu, gpointer user_data);


/*
 * file-> new/open/save 
 */
gboolean closefile(MainView * mainview);
gboolean callback_file_close(GtkAction * action, gpointer data);
void callback_file_new(GtkAction * action, gpointer data);
gboolean reset_ctree(GtkTreeModel * model, GtkTreePath * path, GtkTreeIter * iter, gpointer data);
void new_file(MainView * mainview);
void callback_file_open(GtkAction * action, gpointer data);
gboolean open_file(gchar * filename, MainView * mainview);
void callback_file_save(GtkAction * action, gpointer data);

void
checklist_remove_rowrefs(MainView* mainview, GList* rowrefs, gboolean show_results);

/*
 * font/color 
 */
void callback_shapemenu(GtkAction * action, GtkWidget * wid);
void callback_eraser(GtkAction * action, MainView * mainview);
void callback_menu(GtkAction * action, GtkWidget * menu);
void callback_brushsizetb(GtkAction * action, MainView *mainview);
void callback_brushsize(GtkAction * action, GtkWidget * wid);
void callback_sketchlines(GtkAction * action, GtkWidget * wid);
void callback_color(GtkAction* action, MainView* mainview);
void callback_color_invoke(GtkAction * action, gpointer data);
void callback_pressure(GtkAction * action, MainView *mainview);
void callback_wordwrap(GtkWidget* widget, gpointer user_data);
void callback_font(GtkAction * action, gpointer data);
void callback_fontstyle(GtkAction * action, GtkWidget * wid);
void callback_textbuffer_move(WPTextBuffer *textbuffer, MainView *mainview);

void callback_sharing(GtkWidget* widget, gpointer user_data);
void callback_remove_checked(GtkWidget* widget, gpointer user_data);

gint wp_savecallback(const gchar *buffer, GString * gstr);

void callback_undo(GtkAction * action, MainView * mainview);
void callback_redo(GtkAction * action, MainView * mainview);

void callback_undotoggle(gpointer widget, gboolean st, MainView * mainview);
void callback_redotoggle(gpointer widget, gboolean st, MainView * mainview);

gboolean close_cb(GtkWidget * widget, GdkEventAny * event, MainView * mainview);
gboolean on_main_view_key_press(GtkWidget* widget, GdkEventKey* event, gpointer user_data);
gboolean on_node_view_key_press(GtkWidget* widget, GdkEventKey* event, gpointer user_data);
gboolean on_main_view_key_release(GtkWidget* widget, GdkEventKey* event, gpointer user_data);
gboolean on_node_view_key_release(GtkWidget* widget, GdkEventKey* event, gpointer user_data);

gboolean on_checklist_longpress_timeout(gpointer user_data);
gboolean on_checklist_button_press(GtkWidget* widget, GdkEventButton* event, gpointer user_data);
gboolean on_checklist_button_release(GtkWidget* widget, GdkEventButton* event, gpointer user_data);
gboolean on_checklist_start_panning(HildonPannableArea* area, gpointer user_data);

void callback_fullscreen(GtkToolButton* tool_button, gpointer user_data);

void callback_checklist_change(GtkTreeSelection *selection, MainView *mainview);
void callback_checklist_paste(MainView *mainview);
void callback_checklist_add(GtkAction *action, MainView *mainview);
void callback_checklist_edit(GtkAction *action, MainView *mainview);
void callback_checklist_delete(GtkAction *action, MainView *mainview);
void callback_checklist_delete_real(MainView* mainview);

void callback_sketch_button_toggled(HildonCheckButton* button, gpointer user_data);

/*
 * buffer modified 
 */
void callback_buffer_modified(GtkAction * action, gpointer data);


const gchar *
format_overview_name(nodeData *nd, const gchar *name);

gchar *
he_get_logical_font_desc(const gchar *name);

gchar *
he_get_logical_font_color(const gchar *name);


/* UI helper functions needed in callbacks */
gboolean
show_confirmation(MainView* mainview, gchar* question);

void
show_banner(MainView* mainview, const gchar* text);

typedef enum {
    LINE_EDIT_MODE_DEFAULT,
    LINE_EDIT_MODE_APPEND,
    LINE_EDIT_MODE_PREPEND,
} LineEditMode;

#define show_line_edit_dialog(mainview, title, label_text, action, text) \
   show_line_edit_dialog_full(mainview, title, label_text, action, text, \
           LINE_EDIT_MODE_DEFAULT)

gchar*
show_line_edit_dialog_full(MainView* mainview,
                           const gchar* title,
                           const gchar* label_text,
                           const gchar* action,
                           const gchar* text,
                           LineEditMode mode);

/* TreeView helper functions */

typedef enum {
    TREEVIEW_MAINVIEW,
    TREEVIEW_CHECKLIST,
} TreeViewType;

void
treeview_scroll_to_selection(MainView* mainview, TreeViewType type);

typedef enum {
    TREEVIEW_SELECT_FIRST,
    TREEVIEW_SELECT_LAST,
    TREEVIEW_SELECT_PREVIOUS,
    TREEVIEW_SELECT_NEXT,
} TreeviewSelectType;

void
checklist_select(MainView* mainview, TreeviewSelectType type);

void
nodelist_select(MainView* mainview, TreeviewSelectType type);

void
checklist_edit_selected(MainView* mainview, LineEditMode mode);

#endif
