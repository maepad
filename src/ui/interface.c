/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <gtk/gtk.h>
#include <libintl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <config.h>

#include <hildon/hildon-program.h> 
#include <hildon/hildon-file-chooser-dialog.h>
#include <hildon/hildon-note.h>
#include <hildon/hildon-font-selection-dialog.h>
#include <hildon/hildon-color-button.h>

#include "callbacks.h"
#include "interface.h"
#include "sketchwidget.h"

/* Function prototype for strcasestr(3) - a GNU function */
char *strcasestr(const char *haystack, const char *needle);

/*
 * Privates: 
 */

GtkWidget *create_brushsizemenu(MainView * main);
GtkWidget *create_shapemenu(MainView * main);
GtkWidget *create_sketchlinesmenu(MainView * main);
GtkWidget *create_menu(MainView * main);
static void create_toolbar(MainView * main);
void create_textarea(MainView * main);
void create_treeview(MainView * main);
static void _tool_button_set_icon_name(GtkToolButton *button, const gchar *icon_name);
static void _tool_button_set_icon_file(GtkToolButton* button, const gchar* file_name);
void create_checklist(MainView * main);

void sk_set_brushsize(MainView * main, guint bsize)
{
	int i = (bsize / 2) - 1;

	if (i >= BRUSHSIZE_COUNT)
		i = BRUSHSIZE_COUNT - 1;
	if (i < 0)
		i = 0;

	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(main->brushsizemenuitems[i]), FALSE);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(main->brushsizemenuitems[i]), TRUE);
}

static void _tool_button_set_icon_name(GtkToolButton *button, const gchar *icon_name)
{
	GtkWidget *image;
	image = gtk_image_new_from_icon_name(icon_name, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_tool_button_set_icon_widget(button, image);
}

static void _tool_button_set_icon_file(GtkToolButton* button, const gchar* file_name)
{
	GtkWidget *image;
	image = gtk_image_new_from_file(file_name);
	gtk_tool_button_set_icon_widget(button, image);
}

void _toggle_tool_button_set_inconsistent(GtkToggleToolButton *button, gboolean inconsistent)
{
GtkWidget *toggle_button = gtk_bin_get_child(GTK_BIN(button));
gtk_toggle_button_set_inconsistent(GTK_TOGGLE_BUTTON(toggle_button), inconsistent);
}

MainView *interface_main_view_new(AppData * data)
{
	MainView *result = g_new0(MainView, 1);

        result->checklist_edited = FALSE;
        result->checklist_prepend = FALSE;
        result->current_color = NULL;
        result->can_show_node_view = FALSE;
        result->node_list_longpress_path = NULL;
        result->checklist_longpress_source_id = 0;
        result->node_view_prev_keyval = GDK_VoidSymbol;
        result->node_view_prev_keyval_reset = FALSE;
        result->main_view_prev_keyval = GDK_VoidSymbol;
        result->main_view_prev_keyval_reset = FALSE;

	GtkWidget *main_vbox = gtk_vbox_new(FALSE, 0);

	result->clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
	gtk_clipboard_set_can_store(result->clipboard, NULL, 0);

	result->file_edited = FALSE;

	result->data = data;

        data->main_view = HILDON_STACKABLE_WINDOW(hildon_stackable_window_new());
        data->node_view = HILDON_STACKABLE_WINDOW(hildon_stackable_window_new());

        /* Enable auto-rotation on Maemo 5 (automatic since PR1.2?) */
        hildon_gtk_window_set_portrait_flags(data->main_view,
                HILDON_PORTRAIT_MODE_SUPPORT);
        hildon_gtk_window_set_portrait_flags(data->node_view,
                HILDON_PORTRAIT_MODE_SUPPORT);

        /* Fullscreen and toolbar handling for the node view window */
        g_signal_connect(G_OBJECT(data->node_view),
                         "window-state-event",
                         G_CALLBACK(callback_node_view_window_state),
                         result);
        result->node_view_fullscreen_button = he_fullscreen_button_new(
                GTK_WINDOW(data->node_view));

        /* Make sure node view only gets hidden when closed */
        g_signal_connect(G_OBJECT(data->node_view),
                         "delete-event",
                         G_CALLBACK(gtk_widget_hide_on_delete),
                         NULL);

	/**
	 * We need to connect to these signals very early (before setting
	 * up the HildonLiveSearch hook), so that we can control and
	 * override the key presses in the main view
	 **/
        g_signal_connect(G_OBJECT(result->data->main_view),
			"key-press-event",
			G_CALLBACK(on_main_view_key_press),
			result);
        g_signal_connect(G_OBJECT(result->data->main_view),
			"key-release-event",
			G_CALLBACK(on_main_view_key_release),
			result);

	create_treeview(result);
	create_textarea(result);
	create_checklist(result);
	
	result->brushsizemenu = create_brushsizemenu(result);
	result->sketchlinesmenu = create_sketchlinesmenu(result);
	result->shapemenu = create_shapemenu(result);
	
	create_toolbar(result);
	GtkWidget *nodemenu = create_menu(result);

        /* Handle the event when the longpress menu is shown */
        g_signal_connect(G_OBJECT(nodemenu), "show",
                G_CALLBACK(on_node_menu_show), result);

	g_signal_connect_swapped(result->treeview, "button_press_event", G_CALLBACK(cb_popup), nodemenu);
	gtk_widget_tap_and_hold_setup(GTK_WIDGET(result->treeview), nodemenu, NULL, GTK_TAP_AND_HOLD_NONE);

	/**
	 * Setup the main window layout:
	 *   window -- vbox
	 *             |- scrolledtree -- treeview
	 *             |- livesearch
	 **/
	GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
        gtk_box_pack_start(GTK_BOX(vbox), result->scrolledtree, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(result->main_view_live_search), FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(data->main_view), vbox);

	result->sk = sketchwidget_new(800, 480, FALSE);
	sketchwidget_set_undocallback(result->sk, callback_undotoggle, result);
	sketchwidget_set_redocallback(result->sk, callback_redotoggle, result);

	gtk_box_pack_start(GTK_BOX(main_vbox), result->scrolledwindow, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(main_vbox), sketchwidget_get_mainwidget(result->sk), TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(main_vbox), result->checklist_content, TRUE, TRUE, 0);
	hildon_window_add_toolbar(HILDON_WINDOW(result->data->node_view), GTK_TOOLBAR(result->toolbar));

        gtk_container_add(GTK_CONTAINER(data->node_view), main_vbox);

	gtk_widget_show(main_vbox);

	gtk_widget_show_all(GTK_WIDGET(result->data->main_view));
	gtk_widget_grab_focus(GTK_WIDGET(result->textview));

	result->file_edited = FALSE;
	result->file_name = NULL;

        /* Signals on the main view */
	g_signal_connect(G_OBJECT(result->data->main_view), "delete_event", G_CALLBACK(close_cb), result);

        /* Signals on the node view */
        g_signal_connect(G_OBJECT(result->data->node_view), "key-press-event", G_CALLBACK(on_node_view_key_press), result);
        g_signal_connect(G_OBJECT(result->data->node_view), "key-release-event", G_CALLBACK(on_node_view_key_release), result);

	sk_set_brushsize(result, 2);
	sk_set_brushsize(result, 4);

	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(result->sketchlinesmenuitems[0]), TRUE);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(result->shapemenuitems[0]), TRUE);

	result->newnodedialog_createchild = TRUE;

	result->loading=FALSE;

        /* Initialize the sketch widget and main view */
        gtk_widget_show(GTK_WIDGET(data->main_view));
        gtk_widget_hide(result->scrolledwindow);
        gtk_widget_show(sketchwidget_get_mainwidget(result->sk));
        gtk_widget_realize(sketchwidget_get_drawingarea(result->sk));
        gtk_widget_hide(sketchwidget_get_mainwidget(result->sk));

	return result;
}

void interface_main_view_destroy(MainView * main)
{
    if (main->db != NULL) {
        sqlite3_close(main->db);
    }

    sketchwidget_destroy(main->sk);
    g_free(main);
}

gchar *interface_file_chooser(MainView * mainview, GtkFileChooserAction action, gchar * suggname, gchar * suggext)
{
	GtkWidget *dialog;
	gchar *filename = NULL;

	dialog = hildon_file_chooser_dialog_new(GTK_WINDOW(mainview->data->main_view), action);
	gtk_file_chooser_set_show_hidden(GTK_FILE_CHOOSER(dialog), TRUE);

	/* Use the "Documents" folder as default storage location */
	gchar* folder = g_strdup_printf("%s/MyDocs/.documents/", g_get_home_dir());
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), folder);
	g_free(folder);

	if (action == GTK_FILE_CHOOSER_ACTION_SAVE && suggname != NULL)
		{
			gchar fn[128];

			if (suggext == NULL)
				g_snprintf(fn, sizeof(fn), "%s", suggname);
			else
				g_snprintf(fn, sizeof(fn), "%s.%s", suggname, suggext);

			int i = 0;
			struct stat s;

			while(stat(fn, &s) == 0)
				{
					i++;
					if (suggext == NULL)
						g_snprintf(fn, sizeof(fn), "%s(%d)", suggname, i);
					else
						g_snprintf(fn, sizeof(fn), "%s(%d).%s", suggname, i, suggext);
				}

			maepad_debug("Generated filename: %s", fn);
			gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), fn);
		}

	gtk_widget_show_all(GTK_WIDGET(dialog));
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
		{
			filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		}

	gtk_widget_destroy(dialog);
	return filename;
}

SetupResponse interface_initial_setup(MainView* main)
{
	HildonNote *hn = NULL;
        SetupResponse response = RESPONSE_QUIT;

	g_assert(main != NULL && main->data->app != NULL);

	hn = HILDON_NOTE(hildon_note_new_confirmation_add_buttons(
                    GTK_WINDOW(main->data->main_view),
                    _("Welcome to MaePad!\nWhat do you want to do?"),
                    _("New file"), RESPONSE_CREATE_NEW,
                    _("Open file"), RESPONSE_OPEN_FILE,
                    _("Quit"), RESPONSE_QUIT,
                    NULL, NULL));

	response = gtk_dialog_run(GTK_DIALOG(hn));
	gtk_widget_destroy(GTK_WIDGET(hn));

	return response;
}

/*
 * Privates 
 */

/*
 * Create the menu items needed for the drop down menu 
 */
GtkWidget *create_menu(MainView * main)
{
    /* Create the main window app menu */
    main->main_view_app_menu = GTK_WIDGET(hildon_app_menu_new());

    main->menu_button_new = hildon_button_new_with_text(0, 0, _("New database"), NULL);
    main->menu_button_open = hildon_button_new_with_text(0, 0, _("Open database"), NULL);
    main->menu_button_save = hildon_button_new_with_text(0, 0, _("Save changes"), NULL);
    main->menu_button_about = hildon_button_new_with_text(0, 0, _("About"), NULL);

    g_signal_connect(G_OBJECT(main->menu_button_new), "clicked", G_CALLBACK(callback_file_new), main);
    g_signal_connect(G_OBJECT(main->menu_button_open), "clicked", G_CALLBACK(callback_file_open), main);
    g_signal_connect(G_OBJECT(main->menu_button_save), "clicked", G_CALLBACK(callback_file_save), main);
    g_signal_connect(G_OBJECT(main->menu_button_about), "clicked", G_CALLBACK(callback_about), main);

    hildon_app_menu_append(HILDON_APP_MENU(main->main_view_app_menu), GTK_BUTTON(main->menu_button_new));
    hildon_app_menu_append(HILDON_APP_MENU(main->main_view_app_menu), GTK_BUTTON(main->menu_button_open));
    hildon_app_menu_append(HILDON_APP_MENU(main->main_view_app_menu), GTK_BUTTON(main->menu_button_save));
    hildon_app_menu_append(HILDON_APP_MENU(main->main_view_app_menu), GTK_BUTTON(main->menu_button_about));

    hildon_window_set_app_menu(HILDON_WINDOW(main->data->main_view),
            HILDON_APP_MENU(main->main_view_app_menu));
    gtk_widget_show_all(GTK_WIDGET(main->main_view_app_menu));


    /* Create the node window app menu */
    main->node_view_app_menu = GTK_WIDGET(hildon_app_menu_new());

    main->menu_button_rename = hildon_button_new_with_text(0, 0, _("Rename memo"), NULL);
    main->menu_button_delete = hildon_button_new_with_text(0, 0, _("Delete memo"), NULL);
    main->menu_button_export = hildon_button_new_with_text(0, 0, _("Export to file"), NULL);
    main->menu_button_clear = hildon_button_new_with_text(0, 0, _("Clear contents"), NULL);

    /* Sketch-specific */
    main->menu_button_square = hildon_check_button_new(0);
    gtk_button_set_label(GTK_BUTTON(main->menu_button_square), _("Square shapes"));
    main->menu_button_filled = hildon_check_button_new(0);
    gtk_button_set_label(GTK_BUTTON(main->menu_button_filled), _("Fill shapes"));

    /* Rich text-specific */
    main->menu_button_wordwrap = hildon_check_button_new(0);
    gtk_button_set_label(GTK_BUTTON(main->menu_button_wordwrap), _("Word wrap"));

    /* Checklist-specific */
    main->menu_button_remove_checked = hildon_button_new_with_text(0, 0, _("Remove checked items"), NULL);

    g_signal_connect(G_OBJECT(main->menu_button_rename), "clicked", G_CALLBACK(callback_file_rename_node), main);
    g_signal_connect(G_OBJECT(main->menu_button_delete), "clicked", G_CALLBACK(callback_file_delete_node), main);
    g_signal_connect(G_OBJECT(main->menu_button_export), "clicked", G_CALLBACK(callback_file_export_node), main);
    g_signal_connect(G_OBJECT(main->menu_button_clear), "clicked", G_CALLBACK(callback_edit_clear), main);
    g_signal_connect(G_OBJECT(main->menu_button_square), "toggled", G_CALLBACK(callback_sketch_button_toggled), main);
    g_signal_connect(G_OBJECT(main->menu_button_filled), "toggled", G_CALLBACK(callback_sketch_button_toggled), main);
    g_signal_connect(G_OBJECT(main->menu_button_wordwrap), "toggled", G_CALLBACK(callback_wordwrap), main);
    g_signal_connect(G_OBJECT(main->menu_button_remove_checked), "clicked", G_CALLBACK(callback_remove_checked), main);

    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_rename));
    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_delete));
    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_export));
    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_clear));
    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_square));
    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_filled));
    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_wordwrap));
    hildon_app_menu_append(HILDON_APP_MENU(main->node_view_app_menu), GTK_BUTTON(main->menu_button_remove_checked));

    hildon_window_set_app_menu(HILDON_WINDOW(main->data->node_view),
            HILDON_APP_MENU(main->node_view_app_menu));
    gtk_widget_show_all(GTK_WIDGET(main->node_view_app_menu));




	/*
	 * Create needed handles 
	 */
	GtkMenu *main_menu;
	GtkWidget *file_menu, *about, *edit_menu, *tools_menu, *node_menu, *node_item,*move_node_menu;

	/*
	 * Get the menu of our view 
	 */
	main_menu = GTK_MENU( hildon_gtk_menu_new () );
	/* Add menu to HildonWindow */
	/*hildon_window_set_menu(main->data->main_view, main_menu); XXX */

	/*
	 * Create new menus for submenus in our drop down menu 
	 */
	file_menu = hildon_gtk_menu_new();
	node_menu = hildon_gtk_menu_new();
	edit_menu = hildon_gtk_menu_new();
	tools_menu = hildon_gtk_menu_new();
	move_node_menu = hildon_gtk_menu_new();

	/*
	 * Create the menu items 
	 */
	node_item = gtk_menu_item_new_with_label(_("Memo"));

	main->delete_node_item = gtk_menu_item_new_with_label(_("Delete memo"));
	main->rename_node_item = gtk_menu_item_new_with_label(_("Rename memo"));
	main->export_node_item = gtk_menu_item_new_with_label(_("Export memo"));

	main->move_node_item = gtk_menu_item_new_with_label(_("Move memo"));
	main->move_up_node_item = gtk_menu_item_new_with_label(_("Move up"));
	main->move_down_node_item = gtk_menu_item_new_with_label(_("Move down"));

	main->tools_item = gtk_menu_item_new_with_label(_("Tools"));

	/*
	 * Add menu items to right menus 
	 */
	gtk_menu_append(main_menu, node_item);
	gtk_menu_append(node_menu, main->delete_node_item);
	gtk_menu_append(node_menu, main->rename_node_item);
	gtk_menu_append(node_menu, main->move_node_item);
	gtk_menu_append(node_menu, gtk_separator_menu_item_new());
	gtk_menu_append(node_menu, main->export_node_item);

	/*move node sub-menu*/
	gtk_menu_append(move_node_menu, main->move_up_node_item);
	gtk_menu_append(move_node_menu, main->move_down_node_item);

	gtk_menu_append(main_menu, main->tools_item);
	gtk_menu_append(main_menu, gtk_separator_menu_item_new());
	about = gtk_image_menu_item_new_from_stock(GTK_STOCK_ABOUT, NULL);
	gtk_menu_append(main_menu, about);
	gtk_menu_append(main_menu, gtk_separator_menu_item_new());

	g_signal_connect(G_OBJECT(about), "activate", G_CALLBACK(callback_about), main);

	/*
	 * Add submenus to the right items 
	 */
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(node_item), node_menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_item), tools_menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->move_node_item), move_node_menu);

	main->tools_brushsize = gtk_menu_item_new_with_label(_("Brush size"));
	main->tools_pagestyle = gtk_menu_item_new_with_label(_("Page style"));
	main->tools_shape = gtk_menu_item_new_with_label(_("Shape"));
	main->tools_color = gtk_menu_item_new_with_label(_("Color..."));
	main->tools_font = gtk_menu_item_new_with_label(_("Font..."));
	main->tools_pressure = gtk_check_menu_item_new_with_label(_("Pressure sensitive brush color"));

	gtk_menu_append(tools_menu, main->tools_color);
	gtk_menu_append(tools_menu, main->tools_brushsize);
	gtk_menu_append(tools_menu, main->tools_shape);
	gtk_menu_append(tools_menu, main->tools_pagestyle);
	gtk_menu_append(tools_menu, main->tools_font);
	gtk_menu_append(tools_menu, main->tools_pressure);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_brushsize), main->brushsizemenu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_pagestyle), main->sketchlinesmenu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_shape), main->shapemenu);

	/*
	 * Attach the callback functions to the activate signal 
	 */
	g_signal_connect(G_OBJECT(main->delete_node_item), "activate", G_CALLBACK(callback_file_delete_node), main);
	g_signal_connect(G_OBJECT(main->rename_node_item), "activate", G_CALLBACK(callback_file_rename_node), main);
	g_signal_connect(G_OBJECT(main->export_node_item), "activate", G_CALLBACK(callback_file_export_node), main);
	
	g_signal_connect(G_OBJECT(main->move_up_node_item), "activate", G_CALLBACK(callback_move_up_node), main);
	g_signal_connect(G_OBJECT(main->move_down_node_item), "activate", G_CALLBACK(callback_move_down_node), main);

	g_signal_connect(G_OBJECT(main->tools_font), "activate", G_CALLBACK(callback_font), main);
	g_signal_connect(G_OBJECT(main->tools_pressure), "toggled", G_CALLBACK(callback_pressure), main);
	g_signal_connect(G_OBJECT(main->tools_color), "activate", G_CALLBACK(callback_color_invoke), main);

	/*
	 * We need to show menu items 
	 */
	gtk_widget_show_all(GTK_WIDGET(main_menu));

	return (node_menu);
}

GtkWidget *create_brushsizemenu(MainView * main)
{
	GtkWidget *menu;
	int i;

	GSList *gr = NULL;

	menu = hildon_gtk_menu_new();

	for(i = 0; i < BRUSHSIZE_COUNT; i++)
		{
			int bsize = (i + 1) * 2;

			char fpa[PATH_MAX];

			g_snprintf(fpa, sizeof(fpa), "%s/pencil%d.png", PIXMAPDIR, i + 1);
			GtkWidget *pix = gtk_image_new_from_file(fpa);

			g_object_ref(pix);

			char st[128];

			g_snprintf(st, sizeof(st), _("%dpt"), bsize);
			main->brushsizemenuitems[i] = gtk_radio_menu_item_new_with_label(gr, st);
			gr = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(main->brushsizemenuitems[i]));

			gtk_object_set_user_data(GTK_OBJECT(main->brushsizemenuitems[i]), GINT_TO_POINTER(bsize));
			gtk_object_set_data(GTK_OBJECT(main->brushsizemenuitems[i]), "m", main);
			gtk_object_set_data(GTK_OBJECT(main->brushsizemenuitems[i]), "i", pix);

			gtk_menu_prepend(menu, main->brushsizemenuitems[i]);
			g_signal_connect(G_OBJECT(main->brushsizemenuitems[i]), "activate", G_CALLBACK(callback_brushsize), main->brushsizemenuitems[i]);
		}

	gtk_widget_show_all(GTK_WIDGET(menu));

	return (menu);
}

GtkWidget *create_sketchlinesmenu(MainView * main)
{
	GtkWidget *menu;
	int i;

	GSList *gr = NULL;

	menu = hildon_gtk_menu_new();

	char st[3][128];

	g_snprintf(st[0], sizeof(st[0]), _("No background"));
	g_snprintf(st[1], sizeof(st[1]), _("Notebook lines"));
	g_snprintf(st[2], sizeof(st[2]), _("Graph lines"));

	for(i = 0; i < 3; i++)
		{
			char fpa[PATH_MAX];

			g_snprintf(fpa, sizeof(fpa), "%s/sketchbg%d.png", PIXMAPDIR, i);
			GtkWidget *pix = gtk_image_new_from_file(fpa);

			g_object_ref(pix);

			main->sketchlinesmenuitems[i] = gtk_radio_menu_item_new_with_label(gr, st[i]);
			gr = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(main->sketchlinesmenuitems[i]));

			gtk_object_set_user_data(GTK_OBJECT(main->sketchlinesmenuitems[i]), GINT_TO_POINTER(i));
			gtk_object_set_data(GTK_OBJECT(main->sketchlinesmenuitems[i]), "m", main);
			gtk_object_set_data(GTK_OBJECT(main->sketchlinesmenuitems[i]), "i", pix);

			gtk_menu_prepend(menu, main->sketchlinesmenuitems[i]);
			g_signal_connect(G_OBJECT(main->sketchlinesmenuitems[i]), "activate", G_CALLBACK(callback_sketchlines), main->sketchlinesmenuitems[i]);
		}

	gtk_widget_show_all(GTK_WIDGET(menu));

	return (menu);
}

GtkWidget *create_shapemenu(MainView * main)
{
	GtkWidget* menu = NULL;
	GSList *gr = NULL;
	int i = 0;

        const gchar* labels[] = {
            _("Freehand"),
            _("Line"),
            _("Rectangle"),
            _("Ellipse"),
        };

	menu = hildon_gtk_menu_new();

	for(i=0; i<SKETCHSHAPE_COUNT; i++) {
            gchar* filename = g_strdup_printf("%s/shape%d.png", PIXMAPDIR, i);
            GtkWidget* pix = gtk_image_new_from_file(filename);
            if (pix == NULL) {
                g_error("Cannot load file: %s", filename);
            }
            g_object_ref(pix);
            g_free(filename);

            main->shapemenuitems[i] = gtk_radio_menu_item_new_with_label(gr, labels[i]);
            gr = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(main->shapemenuitems[i]));

            gtk_object_set_user_data(GTK_OBJECT(main->shapemenuitems[i]), GINT_TO_POINTER(i));
            gtk_object_set_data(GTK_OBJECT(main->shapemenuitems[i]), "m", main);
            gtk_object_set_data(GTK_OBJECT(main->shapemenuitems[i]), "i", pix);

            gtk_menu_prepend(menu, main->shapemenuitems[i]);
            g_signal_connect(G_OBJECT(main->shapemenuitems[i]), "activate", G_CALLBACK(callback_shapemenu), main->shapemenuitems[i]);
        }

	gtk_widget_show_all(GTK_WIDGET(menu));

	return (menu);
}
/*
 * Create toolbar to mainview 
 */
static void create_toolbar(MainView * main)
{
	/*
	 * Create new GTK toolbar 
	 */
	main->toolbar = gtk_toolbar_new();

	/*
	 * Set toolbar properties 
	 */
	gtk_toolbar_set_orientation(GTK_TOOLBAR(main->toolbar), GTK_ORIENTATION_HORIZONTAL);
	gtk_toolbar_set_style(GTK_TOOLBAR(main->toolbar), GTK_TOOLBAR_BOTH_HORIZ);

	/*
	 * Make menus and buttons to toolbar: 
	 */

	/*
	 * main->cut_tb = gtk_tool_button_new_from_stock(GTK_STOCK_CUT);
	 * main->copy_tb = gtk_tool_button_new_from_stock(GTK_STOCK_COPY);
	 * main->paste_tb = gtk_tool_button_new_from_stock(GTK_STOCK_PASTE);
	 * main->separator_tb2 = gtk_separator_tool_item_new();
	 */

	main->bold_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->bold_tb), "general_bold");

	main->italic_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->italic_tb), "general_italic");

	main->underline_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->underline_tb), PIXMAPDIR "/underline.png");

	main->bullet_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->bullet_tb), "notes_bullets");

	main->strikethru_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->strikethru_tb), PIXMAPDIR "/strike.png");

	main->check_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->check_tb), "widgets_tickmark_list");

	main->checkadd_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->checkadd_tb), "general_add");

	main->checkedit_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->checkedit_tb), PIXMAPDIR "/sketch.png");

	main->checkdel_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->checkdel_tb), "general_delete");

/*		       	
	main->bold_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_BOLD);
	main->italic_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_ITALIC);
	main->underline_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_UNDERLINE);
*/
	main->font_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->font_tb), PIXMAPDIR "/font.png");

/*	main->fullscreen_tb = gtk_tool_button_new_from_stock(GTK_STOCK_ZOOM_FIT);*/

	/*
	 * Insert items to toolbar 
	 */
	/*
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->cut_tb, -1);
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->copy_tb, -1);
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->paste_tb, -1);
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->separator_tb2, -1);
	 */
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->check_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->font_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->bold_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->italic_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->underline_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->bullet_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->strikethru_tb, -1);

	main->colorbutton_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->colorbutton_tb), PIXMAPDIR "/palette.png");
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->colorbutton_tb, -1);
	g_signal_connect(G_OBJECT(main->colorbutton_tb), "clicked", G_CALLBACK(callback_color), main);

	main->brushsize_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->brushsize_tb), PIXMAPDIR "/pencil1.png");
	g_signal_connect(G_OBJECT(main->brushsize_tb), "clicked", G_CALLBACK(callback_brushsizetb), main);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->brushsize_tb, -1);

	main->eraser_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->eraser_tb), "sketch_erase");
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->eraser_tb, -1);
	g_signal_connect(G_OBJECT(main->eraser_tb), "toggled", G_CALLBACK(callback_eraser), main);

	main->shape_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->shape_tb), PIXMAPDIR "/shape0.png");
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->shape_tb, -1);
	g_signal_connect(G_OBJECT(main->shape_tb), "clicked", G_CALLBACK(callback_menu), main->shapemenu);

	main->sketchlines_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_file(GTK_TOOL_BUTTON(main->sketchlines_tb), PIXMAPDIR "/sketchbg0.png");
	g_signal_connect(G_OBJECT(main->sketchlines_tb), "clicked", G_CALLBACK(callback_menu), main->sketchlinesmenu);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->sketchlines_tb, -1);

	
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), gtk_separator_tool_item_new(), -1);

	main->sharing_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->sharing_tb), "control_sharing");
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->sharing_tb, -1);
	g_signal_connect(G_OBJECT(main->sharing_tb), "clicked", G_CALLBACK(callback_sharing), main);

	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->checkadd_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->checkedit_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->checkdel_tb, -1);

	main->undo_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->undo_tb), "general_undo");
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->undo_tb, -1);
	g_signal_connect(G_OBJECT(main->undo_tb), "clicked", G_CALLBACK(callback_undo), main);

	main->redo_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->redo_tb), "general_redo");
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->redo_tb, -1);
	g_signal_connect(G_OBJECT(main->redo_tb), "clicked", G_CALLBACK(callback_redo), main);

        GtkWidget* fullscreen_separator = GTK_WIDGET(gtk_separator_tool_item_new());
        gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), GTK_TOOL_ITEM(fullscreen_separator), -1);
        gtk_separator_tool_item_set_draw(GTK_SEPARATOR_TOOL_ITEM(fullscreen_separator), FALSE);
        gtk_tool_item_set_expand(GTK_TOOL_ITEM(fullscreen_separator), TRUE);

        main->fullscreen_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->fullscreen_tb), "general_fullsize");
	g_signal_connect(G_OBJECT(main->fullscreen_tb), "clicked", G_CALLBACK(callback_fullscreen), main);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->fullscreen_tb, -1);

	/*
	 * Connect signals to buttons 
	 */

	/*
	 * g_signal_connect(G_OBJECT(main->cut_tb), "clicked",
	 * G_CALLBACK(callback_edit_cut), main);
	 * g_signal_connect(G_OBJECT(main->copy_tb), "clicked",
	 * G_CALLBACK(callback_edit_copy), main);
	 * g_signal_connect(G_OBJECT(main->paste_tb), "clicked",
	 * G_CALLBACK(callback_edit_paste), main);
	 */
	g_signal_connect(G_OBJECT(main->font_tb), "clicked", G_CALLBACK(callback_font), main);

	gtk_object_set_data(GTK_OBJECT(main->bold_tb), "s", GINT_TO_POINTER(WPT_BOLD));
	gtk_object_set_data(GTK_OBJECT(main->italic_tb), "s", GINT_TO_POINTER(WPT_ITALIC));
	gtk_object_set_data(GTK_OBJECT(main->underline_tb), "s", GINT_TO_POINTER(WPT_UNDERLINE));
	gtk_object_set_data(GTK_OBJECT(main->bullet_tb), "s", GINT_TO_POINTER(WPT_BULLET));
	gtk_object_set_data(GTK_OBJECT(main->strikethru_tb), "s", GINT_TO_POINTER(WPT_STRIKE));
	gtk_object_set_data(GTK_OBJECT(main->check_tb), "s", GINT_TO_POINTER(WPT_LEFT));

	gtk_object_set_data(GTK_OBJECT(main->bold_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->italic_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->underline_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->bullet_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->strikethru_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->check_tb), "m", main);

	g_signal_connect(G_OBJECT(main->bold_tb), "toggled", G_CALLBACK(callback_fontstyle), main->bold_tb);
	g_signal_connect(G_OBJECT(main->italic_tb), "toggled", G_CALLBACK(callback_fontstyle), main->italic_tb);
	g_signal_connect(G_OBJECT(main->underline_tb), "toggled", G_CALLBACK(callback_fontstyle), main->underline_tb);
	g_signal_connect(G_OBJECT(main->bullet_tb), "toggled", G_CALLBACK(callback_fontstyle), main->bullet_tb);
	g_signal_connect(G_OBJECT(main->strikethru_tb), "toggled", G_CALLBACK(callback_fontstyle), main->strikethru_tb);
	g_signal_connect(G_OBJECT(main->check_tb), "toggled", G_CALLBACK(callback_fontstyle), main->check_tb);

	g_signal_connect(G_OBJECT(main->checkadd_tb), "clicked", G_CALLBACK(callback_checklist_add), main);
	g_signal_connect(G_OBJECT(main->checkedit_tb), "clicked", G_CALLBACK(callback_checklist_edit), main);
	g_signal_connect(G_OBJECT(main->checkdel_tb), "clicked", G_CALLBACK(callback_checklist_delete), main);

	gtk_widget_show_all(GTK_WIDGET(main->toolbar));
}

/*
 * Create the text area 
 */
void create_textarea(MainView * main)
{
	main->scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
#ifdef MAEMOPADPLUS_FINGER_FRIENDLY
        hildon_helper_set_thumb_scrollbar(main->scrolledwindow, TRUE);
#endif
	gtk_widget_hide(main->scrolledwindow);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(main->scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	main->textview = wp_text_view_new();
/*
	gtk_text_view_set_editable(GTK_TEXT_VIEW(main->textview), TRUE);
	gtk_text_view_set_left_margin(GTK_TEXT_VIEW(main->textview), 10);
	gtk_text_view_set_right_margin(GTK_TEXT_VIEW(main->textview), 10);
	main->buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(main->textview));
	main->buffer = wp_text_buffer_new(NULL);
*/
	main->buffer = WP_TEXT_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(main->textview)));
	
	g_signal_connect(G_OBJECT(main->buffer), "refresh_attributes", G_CALLBACK(callback_textbuffer_move), main);
	g_signal_connect(G_OBJECT(main->buffer), "can_undo", G_CALLBACK(callback_undotoggle), main);
	g_signal_connect(G_OBJECT(main->buffer), "can_redo", G_CALLBACK(callback_redotoggle), main);

	wp_text_buffer_enable_rich_text(main->buffer, TRUE);

	gtk_container_add(GTK_CONTAINER(main->scrolledwindow), main->textview);
	gtk_widget_show(main->textview);

	gtk_widget_modify_font(main->textview, pango_font_description_from_string("Monospace Regular 16"));

	g_signal_connect(G_OBJECT(main->buffer), "modified-changed", G_CALLBACK(callback_buffer_modified), main);
	g_signal_connect(G_OBJECT(main->buffer), "changed", G_CALLBACK(callback_buffer_modified), main);
}

static gboolean
livesearch_filter_func(GtkTreeModel *model, GtkTreeIter *iter,
		gchar *text, gpointer data)
{
    gboolean retvalue;
    gchar *rowtext = NULL;
    gtk_tree_model_get(model, iter, 0, &rowtext, -1);

    if (rowtext == NULL || text == NULL) {
	    return FALSE;
    }

    retvalue = (strcasestr(rowtext, text) != NULL);
    g_free (rowtext);
    return retvalue;
}


/*
 * Create the treeview 
 */
void create_treeview(MainView * main)
{
	main->scrolledtree = hildon_pannable_area_new();
/*#ifdef MAEMOPADPLUS_FINGER_FRIENDLY
        hildon_helper_set_thumb_scrollbar(main->scrolledtree, TRUE);
#endif*/
	gtk_widget_show(main->scrolledtree);

	main->main_view_tree_store = gtk_tree_store_new(N_COLUMNS,
			G_TYPE_STRING, GDK_TYPE_PIXBUF, G_TYPE_POINTER);

	/* Set up Hildon Live Search for the node list */
        main->main_view_live_search = HILDON_LIVE_SEARCH(hildon_live_search_new());
	g_signal_connect(G_OBJECT(main->main_view_live_search),
		       "refilter",
		       G_CALLBACK(callback_live_search_refilter),
		       main);

        main->main_view_tree_filter = GTK_TREE_MODEL_FILTER(gtk_tree_model_filter_new(
			GTK_TREE_MODEL(main->main_view_tree_store),
                        NULL));
	hildon_live_search_set_filter(main->main_view_live_search,
			main->main_view_tree_filter);
	hildon_live_search_set_visible_func(main->main_view_live_search,
			livesearch_filter_func, NULL, NULL);

	main->treeview = hildon_gtk_tree_view_new_with_model(HILDON_UI_MODE_EDIT,
			GTK_TREE_MODEL(main->main_view_tree_filter));

	hildon_live_search_widget_hook(main->main_view_live_search,
			GTK_WIDGET(main->data->main_view), main->treeview);


        /* Add a new button to the treeview action area */
        main->action_area_button_add = hildon_button_new_with_text(
                HILDON_SIZE_FINGER_HEIGHT | HILDON_SIZE_AUTO_WIDTH,
                HILDON_BUTTON_ARRANGEMENT_HORIZONTAL,
                _("Add new memo"),
                NULL);
        hildon_button_set_image(HILDON_BUTTON(main->action_area_button_add),
                gtk_image_new_from_icon_name("general_add", GTK_ICON_SIZE_BUTTON));

        gtk_widget_show(GTK_WIDGET(main->action_area_button_add));
        g_signal_connect(G_OBJECT(main->action_area_button_add),
                         "clicked",
                         G_CALLBACK(callback_file_new_node),
                         main);

        GtkWidget* action_area_box = hildon_tree_view_get_action_area_box(GTK_TREE_VIEW(main->treeview));
        gtk_container_add(GTK_CONTAINER(action_area_box), main->action_area_button_add);
        hildon_tree_view_set_action_area_visible(GTK_TREE_VIEW(main->treeview), TRUE);
        gtk_widget_show_all(GTK_WIDGET(action_area_box));


	GtkCellRenderer *renderer1 = gtk_cell_renderer_text_new();
	GtkCellRenderer *renderer2 = gtk_cell_renderer_pixbuf_new();

	GtkTreeViewColumn *column = gtk_tree_view_column_new();

	gtk_tree_view_column_pack_start(column, renderer2, FALSE);
	gtk_tree_view_column_pack_start(column, renderer1, TRUE);

	gtk_tree_view_column_add_attribute(column, renderer1, "markup", NODE_NAME);
	gtk_tree_view_column_add_attribute(column, renderer2, "pixbuf", NODE_PIXBUF);

        /* Ellipsize to avoid horizontal scroll bars */
        g_object_set(G_OBJECT(renderer1), "ellipsize", PANGO_ELLIPSIZE_END, NULL);

	/*
	 * gtk_tree_view_column_add_attribute(column, renderer2, "width", 0);
	 * gtk_tree_view_column_add_attribute(column, renderer2, "height", 0);
	 */
	gtk_tree_view_column_set_cell_data_func(column, renderer2, callback_treeview_celldatafunc, main, NULL);

        /* Configure for speedier scrolling - fixed height mode */
	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_FIXED);
        gtk_tree_view_set_fixed_height_mode(GTK_TREE_VIEW(main->treeview), TRUE);

	gtk_tree_view_append_column(GTK_TREE_VIEW(main->treeview), column);

	GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(main->treeview));

	gtk_tree_selection_set_mode(select, GTK_SELECTION_BROWSE);
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(main->treeview), FALSE);
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(main->treeview), FALSE);
	gtk_tree_view_set_expander_column(GTK_TREE_VIEW(main->treeview), column);

	gtk_tree_view_expand_all(GTK_TREE_VIEW(main->treeview));
	gtk_tree_view_set_enable_search(GTK_TREE_VIEW(main->treeview), FALSE);
	gtk_tree_view_set_fixed_height_mode(GTK_TREE_VIEW(main->treeview), FALSE);

	g_signal_connect(G_OBJECT(main->treeview), "button-press-event", G_CALLBACK(callback_treeview_button_press), main);

	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(callback_treeview_change), main);
	gtk_tree_selection_set_select_function(select, treeview_canselect, main, NULL);
	gtk_object_set_user_data(GTK_OBJECT(main->treeview), (gpointer) main);

	gtk_container_add(GTK_CONTAINER(main->scrolledtree), main->treeview);

	gtk_widget_show(main->treeview);
	gtk_widget_show(main->scrolledtree);
}


void create_checklist(MainView * main)
{
        main->checklist_content = gtk_vbox_new(FALSE, 0);
	main->listscroll = hildon_pannable_area_new();

        gtk_box_pack_start(GTK_BOX(main->checklist_content),
                main->listscroll, TRUE, TRUE, 0);

        main->checklist_empty_label = gtk_label_new(_("No items"));
        hildon_helper_set_logical_font(main->checklist_empty_label,
                "LargeSystemFont");
        hildon_helper_set_logical_color(main->checklist_empty_label,
                GTK_RC_FG, GTK_STATE_NORMAL, "SecondaryTextColor");
        gtk_misc_set_alignment(GTK_MISC(main->checklist_empty_label),
                .5, .5);

        gtk_box_pack_start(GTK_BOX(main->checklist_content),
                main->checklist_empty_label, TRUE, TRUE, 0);

        /* Detect scrolling to abort longpress action on checklist */
        g_signal_connect(G_OBJECT(main->listscroll), "panning-started", G_CALLBACK(on_checklist_start_panning), main);

	GtkListStore *store = gtk_list_store_new(CHECKN_COLUMNS, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN, G_TYPE_STRING);

	main->listview = hildon_gtk_tree_view_new_with_model(HILDON_UI_MODE_EDIT, GTK_TREE_MODEL(store));
	g_object_unref(G_OBJECT(store));

	g_object_set(main->listview, "allow-checkbox-mode", FALSE, NULL);

	GtkCellRenderer *renderer1 = gtk_cell_renderer_pixbuf_new();

	GtkCellRenderer *renderer2 = gtk_cell_renderer_text_new();

	main->checklist_column_check = gtk_tree_view_column_new_with_attributes("", renderer1,
                                                      "icon-name", CHECKNODE_ICON_NAME,
                                                      NULL);

	main->checklist_column_text = gtk_tree_view_column_new_with_attributes("", renderer2,
                                                      "text", CHECKNODE_TEXT,
                                                      "foreground", CHECKNODE_COLOR,
                                                      "strikethrough", CHECKNODE_STRIKE,
                                                      "weight", CHECKNODE_BOLD,
                                                      NULL);

        /* Ellipsize to avoid horizontal scroll bars */
        g_object_set(G_OBJECT(renderer2), "ellipsize", PANGO_ELLIPSIZE_END, NULL);

	gtk_tree_view_append_column(GTK_TREE_VIEW(main->listview), main->checklist_column_check);
	gtk_tree_view_append_column(GTK_TREE_VIEW(main->listview), main->checklist_column_text);

        /* Configure for speedier scrolling - fixed height mode */
	gtk_tree_view_column_set_sizing(main->checklist_column_check, GTK_TREE_VIEW_COLUMN_FIXED);
        gtk_tree_view_column_set_fixed_width(main->checklist_column_check, 40);
	gtk_tree_view_column_set_sizing(main->checklist_column_text, GTK_TREE_VIEW_COLUMN_FIXED);
        gtk_tree_view_set_fixed_height_mode(GTK_TREE_VIEW(main->listview), TRUE);

	GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(main->listview));

	/*gtk_tree_selection_set_mode(select, GTK_SELECTION_MULTIPLE);*/
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(main->listview), FALSE);
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(main->listview), FALSE);
	gtk_tree_view_set_enable_search(GTK_TREE_VIEW(main->listview), TRUE);

/*
	gtk_tree_selection_set_select_function(select, treeview_canselect, main, NULL);
*/
	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(callback_checklist_change), main);

	gtk_object_set_user_data(GTK_OBJECT(main->listview), (gpointer) main);

        /* Add handlers for checklist interaction */
        g_signal_connect(main->listview, "button-press-event", G_CALLBACK(on_checklist_button_press), main);
        g_signal_connect(main->listview, "button-release-event", G_CALLBACK(on_checklist_button_release), main);

	gtk_container_add(GTK_CONTAINER(main->listscroll), main->listview);
	gtk_widget_show_all(main->checklist_content);
        gtk_widget_hide(main->listscroll);
/*
GtkTreeIter    toplevel;
gtk_list_store_append(store, &toplevel);
gtk_list_store_set(store, &toplevel,
                     CHECKNODE_CHECKED, TRUE, CHECKNODE_TEXT, "sample text", -1);
*/	
}


GtkWindow*
mainview_get_dialog_parent(MainView* mainview)
{
    if (GTK_WIDGET_VISIBLE(mainview->data->node_view)) {
        return GTK_WINDOW(mainview->data->node_view);
    } else {
        return GTK_WINDOW(mainview->data->main_view);
    }
}

