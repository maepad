/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <ui/interface.h>
#include <ui/callbacks.h>
#include <string.h>
#include <stdlib.h>

#include <gtk/gtkmain.h>
#include <hildon/hildon-program.h>
#include <hildon/hildon-note.h>
#include <libintl.h>
#include <locale.h>
#include <libosso.h>

#define _(String) gettext(String)

#include <config.h>
#include <appdata.h>

AppData*
app_data_new()
{
    AppData* app_data = g_new0(AppData, 1);

    app_data->app = HILDON_PROGRAM(hildon_program_get_instance());
    g_assert(app_data->app);
    app_data->osso = osso_initialize("org.maemo.maepad", VERSION, FALSE, NULL);
    g_assert(app_data->osso);
    app_data->gconf = gconf_client_get_default();
    g_assert(app_data->gconf);

    return app_data;
}

void
app_data_destroy(AppData* app_data)
{
    osso_deinitialize(app_data->osso);
    g_object_unref(app_data->gconf);

    g_free(app_data);
}

int main(int argc, char *argv[])
{
    AppData *app_data;
    MainView *main_view;

    /* Initialise the locale stuff */
    setlocale(LC_ALL, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    /* Init the gtk - must be called before any hildon stuff */
    gtk_init(&argc, &argv);

    /* Create the hildon application and setup the title */
    g_set_application_name ("MæPad");

    /* Create the data and views for our application */
    app_data = app_data_new();
    main_view = interface_main_view_new(app_data);

    /* Check if we can directly open the last document */
    gchar* last_document = gconf_client_get_string(app_data->gconf, MAEPAD_GCONF_LAST_DOCUMENT, NULL);

    if (last_document != NULL) {
        open_file(last_document, main_view);
        g_free(last_document);
    }

    gboolean do_quit = FALSE;
    while (main_view->file_name == NULL && !do_quit) {
        switch (interface_initial_setup(main_view)) {
            case RESPONSE_CREATE_NEW:
                    callback_file_new(NULL, main_view);
                    break;
            case RESPONSE_OPEN_FILE:
                    callback_file_open(NULL, main_view);
                    break;
            case RESPONSE_QUIT:
                    do_quit = TRUE;
                    break;
        }
    };

    if (main_view->file_name != NULL) {
        /* Ready to begin the main loop */
        gtk_main();

        /* Remember last filename on close */
        if (main_view->file_name != NULL) {
            gconf_client_set_string(app_data->gconf, MAEPAD_GCONF_LAST_DOCUMENT, main_view->file_name, NULL);
        }
    }

    /* Clean-up the interface and app data objects */
    interface_main_view_destroy(main_view);
    app_data_destroy(app_data);

    return EXIT_SUCCESS;
}

