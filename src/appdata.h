/*
 * This file is part of MaePad
 * Copyright (c) 2010 Thomas Perl <thp.io/about>
 * http://thp.io/2010/maepad/
 *
 * Based on Maemopad+:
 * Copyright (c) 2006-2008 Kemal Hadimli
 * Copyright (c) 2008 Thomas Perl
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef MAEMOPAD_APPDATA_H
#define MAEMOPAD_APPDATA_H

#include <libosso.h>
#include <gconf/gconf-client.h>
#include <config.h>

#include <hildon/hildon.h>

typedef struct _AppData AppData;

struct _AppData
{
    HildonProgram  *app; /* handle to application */
    osso_context_t *osso; /* handle to osso */
    GConfClient* gconf;

    HildonStackableWindow* main_view; /* list of nodes */
    HildonStackableWindow* node_view; /* node view */
};

#define MAEPAD_GCONF_LAST_DOCUMENT "/apps/maepad/general/lastdocument"

/* App-specific logging and debugging output */

#define MAEPAD_LOG_DOMAIN "MaePad"

#define maepad_debug(...)   g_log(MAEPAD_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, __VA_ARGS__)
#define maepad_message(...) g_log(MAEPAD_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, __VA_ARGS__)
#define maepad_warning(...) g_log(MAEPAD_LOG_DOMAIN, G_LOG_LEVEL_WARNING, __VA_ARGS__)

#endif
